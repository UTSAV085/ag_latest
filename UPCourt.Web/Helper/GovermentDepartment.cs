﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using UPCourt.Web.Core;
using UPCourt.Web.Models;

namespace core.koushikiSofatware.Helper
{
    public static class HtmlHelpers
    {
        public static IHtmlString RemoveLink(this HtmlHelper htmlHelper, string linkText, string container, string deleteElement)
        {
            var js = string.Format("javascript:removeNestedForm(this,'{0}','{1}');return false;", container, deleteElement);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        public static IHtmlString ToggleRadioButton(this HtmlHelper htmlHelper, string linkText, string container, string toggleElement)
        {
            var js = string.Format("javascript:toggleNestedForm(this,'{0}','{1}');return false;", container, toggleElement);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }

        public static IHtmlString AddLinkPhone<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string containerElement, string counterElement, string collectionProperty, Type nestedType)
        {
            var ticks = DateTime.UtcNow.Ticks;
            var nestedObject = Activator.CreateInstance(nestedType);
            var partial = htmlHelper.EditorFor(x => nestedObject,"Phone").ToHtmlString().JsEncode();
            
            partial = partial.Replace("id=\\\"nestedObject", "id=\\\"" + collectionProperty + "_" + ticks + "_");
            partial = partial.Replace("name=\\\"nestedObject", "name=\\\"" + collectionProperty + "[" + ticks + "]");
            var js = string.Format("javascript:addNestedForm('{0}','{1}','{2}','{3}');return false;", containerElement, counterElement, ticks, partial);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        public static IHtmlString AddLinkAddress<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string containerElement, string counterElement, string collectionProperty, Type nestedType)
        {
            var ticks = DateTime.UtcNow.Ticks;
            var nestedObject = Activator.CreateInstance(nestedType);
            var partial = htmlHelper.EditorFor(x => nestedObject, "Address").ToHtmlString().JsEncode();
            partial = partial.Replace("id=\\\"nestedObject", "id=\\\"" + collectionProperty + "_" + ticks + "_");            
            partial = partial.Replace("name=\\\"nestedObject", "name=\\\"" + collectionProperty + "[" + ticks + "]");
            var js = string.Format("javascript:addNestedForm('{0}','{1}','{2}','{3}');return false;", containerElement, counterElement, ticks, partial);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        public static IHtmlString AddLinkNoticeParties<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string containerElement, string counterElement, string collectionProperty, Type nestedType)
        {
            var ticks = DateTime.UtcNow.Ticks;
            var nestedObject = Activator.CreateInstance(nestedType);
            var partial = htmlHelper.EditorFor(x => nestedObject, "NoticeParty").ToHtmlString().JsEncode();
            partial = partial.Replace("id=\\\"nestedObject", "id=\\\"" + collectionProperty + "_" + ticks + "_");
            partial = partial.Replace("name=\\\"nestedObject", "name=\\\"" + collectionProperty + "[" + ticks + "]");
            var js = string.Format("javascript:addNestedForm('{0}','{1}','{2}','{3}',this);return false;", containerElement, counterElement, ticks, partial);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        public static IHtmlString AddLinkNoticeRespondents<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string containerElement, string counterElement, string collectionProperty, Type nestedType)
        {
            var ticks = DateTime.UtcNow.Ticks;
            var nestedObject = Activator.CreateInstance(nestedType);
            var partial = htmlHelper.EditorFor(x => nestedObject, "NoticeRespondent").ToHtmlString().JsEncode();
            partial = partial.Replace("id=\\\"nestedObject", "id=\\\"" + collectionProperty + "_" + ticks + "_");
            partial = partial.Replace("name=\\\"nestedObject", "name=\\\"" + collectionProperty + "[" + ticks + "]");
            var js = string.Format("javascript:addNestedForm('{0}','{1}','{2}','{3}',this);return false;", containerElement, counterElement, ticks, partial);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        private static string JsEncode(this string s)
        {
            if (string.IsNullOrEmpty(s)) return "";
            int i;
            int len = s.Length;
            StringBuilder sb = new StringBuilder(len + 4);
            string t;
            for (i = 0; i < len; i += 1)
            {
                char c = s[i];
                switch (c)
                {
                    case '>':
                    case '"':
                    case '\\':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    case '\n':
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\r':
                        break;
                    default:
                        if (c < ' ')
                        {
                            string tmp = new string(c, 1);
                            t = "000" + int.Parse(tmp, System.Globalization.NumberStyles.HexNumber);
                            sb.Append("\\u" + t.Substring(t.Length - 4));
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            return sb.ToString();
        }

        public static IHtmlString AddBasicIndex<TModel>(this HtmlHelper<TModel> htmlHelper, string linkText, string containerElement, string counterElement, string collectionProperty, Type nestedType)
        {
            var ticks = DateTime.UtcNow.Ticks;
            var nestedObject = Activator.CreateInstance(nestedType);
            var partial = htmlHelper.EditorFor(x => nestedObject, "BasicIndex").ToHtmlString().JsEncode();
            partial = partial.Replace("id=\\\"nestedObject", "id=\\\"" + collectionProperty + "_" + ticks + "_");
            partial = partial.Replace("name=\\\"nestedObject", "name=\\\"" + collectionProperty + "[" + ticks + "]");
            var js = string.Format("javascript:addNestedForm('{0}','{1}','{2}','{3}',this);return false;", containerElement, counterElement, ticks, partial);
            TagBuilder tb = new TagBuilder("a");
            tb.Attributes.Add("href", "#");
            tb.Attributes.Add("onclick", js);
            tb.InnerHtml = linkText;
            var tag = tb.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(tag);
        }
        //[AllowAnonymous]
        //[HttpPost]
        //[MultipleButton(Name = "action", Argument = "addbasicindex")]
        //public ActionResult addbasicindex(NoticeMvcModel model)
        //{
        //    var abc = model.NoticeBasicIndex;
        //    //var tempAssesseeITRTypeAssessment = AssesseeITRTypeAssessmentDataService.GetLite(db).FirstOrDefault(i => i.AssesseeId == AppUser.UserId);
        //    //if (tempAssesseeITRTypeAssessment.IsNull())
        //    //{
        //    //    tempAssesseeITRTypeAssessment = new AssesseeITRTypeAssessment();
        //    //    tempAssesseeITRTypeAssessment.AssesseeITRTypeAssessmentId = Guid.NewGuid();
        //    //    tempAssesseeITRTypeAssessment.AssesseeId = AppUser.UserId;

        //    //    db.AssesseeITRTypeAssessments.Add(tempAssesseeITRTypeAssessment);
        //    //}
        //    //tempAssesseeITRTypeAssessment.IsIncomeFromSalaryOrPension = true;
        //    //db.SaveChanges();
        //    return abc;
        //}

        ////[AllowAnonymous]
        //[HttpPost]
        ////[MultipleButton(Name = "action", Argument = "addbasicindex")]
        //public JsonResult addbasicindex(NoticeMvcModel data)
        //{
        //    //var ITR1SubmissionHas = ITR1SubmissionDataService.GetLite(db).FirstOrDefault(i => i.AssesseeId == data.AssesseeId && i.FinancialYearId == data.FinancialYearId);
        //    //if (ITR1SubmissionHas == null)
        //    //{
        //    //    rModel.Data = data;
        //    //    rModel.IsSuccess = false;
        //    //    rModel.Message = "First Submission of this year";
        //    //    return RModelResult();
        //    //}
        //    //var IncomeSourceHouseProperty = new ITR1IncomeSourceHousePropertySameAddressResponseModel();
        //    //var ProfileAddress = ITR1PersonalInfoDataService.GetLite(db).FirstOrDefault(i => i.AssesseeId == data.AssesseeId);
        //    //if (ProfileAddress == null)
        //    //{
        //    //    //rModel.Data = data;
        //    //    rModel.IsSuccess = false;
        //    //    rModel.Message = "Not found any House Property";
        //    //    return RModelResult();
        //    //}
        //    //IncomeSourceHouseProperty.FlatDoorBlockNumber = ProfileAddress.FlatDoorBlockNumber;
        //    //IncomeSourceHouseProperty.PremiseName = ProfileAddress.PremiseName;
        //    //IncomeSourceHouseProperty.RoadStreet = ProfileAddress.RoadStreet;
        //    //IncomeSourceHouseProperty.Pincode = ProfileAddress.Pincode;
        //    //IncomeSourceHouseProperty.AreaLocality = ProfileAddress.AreaLocality;
        //    //IncomeSourceHouseProperty.TownCity = ProfileAddress.TownCity;
        //    //IncomeSourceHouseProperty.StateName = ProfileAddress.StateName;
        //    //IncomeSourceHouseProperty.CountryName = ProfileAddress.CountryName;
        //    //rModel.Data = IncomeSourceHouseProperty;
        //    rModel.IsSuccess = true;
        //    rModel.Message = "Get Address";
        //    return RModelResult();
        //}
    }
}