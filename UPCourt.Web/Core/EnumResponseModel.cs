﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPCourt.Web.Core
{
    public class EnumResponseModel
    {
        public string Value { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}