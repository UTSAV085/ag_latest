﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Core
{
    public static class ModelHelper
    {
        public static List<EnumResponseModel> GetEnumResponseModelMembers<E>()
        {
            var members = Enum.GetValues(typeof(E));
            var List = new List<EnumResponseModel>();
            foreach (Enum policyterm in members)
            {
                var obj = new EnumResponseModel();
                obj.Value = policyterm.ToString();
                obj.Name = Extended.GetDescription(policyterm);
                List.Add(obj);
            }

            return List;
        }
    }
}