﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UPCourt.Web.Core
{
    public class BaseResultModel
    {
        public BaseResultModel()
        {
            Errors = new List<ErrorMessage>();
        }
        public String Message { get; set; }
        public bool IsSuccess { get; set; }
        public Object Data { get; set; }
        public List<ErrorMessage> Errors { get; internal set; }

        public void AddError(String ErrorMessage)
        {
            AddError(ErrorMessage, ErrorType.Error);
        }
        public void AddError(String ErrorMessage, ErrorType ErrorType)
        {
            AddError(ErrorMessage, ErrorType, null);
        }
        public void AddError(String ErrorMessage, ErrorType ErrorType, String FieldName)
        {
            AddError(ErrorMessage, ErrorType, FieldName, null);
        }

        public void AddError(String ErrorMessage, ErrorType ErrorType, String FieldName, string AdditionalInfo)
        {
            ErrorMessage message = new ErrorMessage { AdditionInfo = AdditionalInfo, FieldName = FieldName, Message = ErrorMessage, ErrorType = ErrorType };
            Errors.Add(message);
        }


        public void AddErrors(IEnumerable<String> ErrorMessages)
        {
            foreach (var item in ErrorMessages)
            {
                AddError(item);
            }
        }
    }

    public class ErrorMessage
    {
        public string FieldName { get; set; }
        public string Message { get; set; }
        public String AdditionInfo { get; set; }
        public ErrorType ErrorType { get; set; }
    }

    public enum ErrorType
    {
        Error,
        Warning,
        Info
    }
}