﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Core
{
    public class BaseModel<E, M> where E : class where M : class, new()
    {
        private const int DefaultMaxRecursiveLevel = 5;

        [IgnoreDataMember]
        public int? RecursiveLevel { get; set; }
        [IgnoreDataMember]
        public int? MaxRecursiveLevel { get; set; }
        public virtual M EntityToResponse(E entity, M model)
        {
            return model;
        }
        public static M CopyFromEntity(E dbData, int? recusrsiveLevel, int? MaxRecursiveLevel = DefaultMaxRecursiveLevel)
        {
            if (dbData == null)
            {
                return null;
            }
            var data = Extended.CopyX<E, M>(dbData);

            if (recusrsiveLevel.HasValue && recusrsiveLevel >= MaxRecursiveLevel)
            {
                return null;
            }
            recusrsiveLevel++;
            var dataM = data as BaseModel<E, M>;
            dataM.RecursiveLevel = recusrsiveLevel;
            dataM.MaxRecursiveLevel = MaxRecursiveLevel;

            data = dataM.EntityToResponse(dbData, dataM as M);

            return data;
        }
        public static List<M> CopyFromEntityList(IEnumerable<E> dbData, int? recusrsiveLevel, int? MaxRecursiveLevel = DefaultMaxRecursiveLevel)
        {
            if (dbData == null)
            {
                return null;
            }
            var list = new List<M>();
            foreach (var data in dbData)
            {
                list.Add(CopyFromEntity(data, recusrsiveLevel, MaxRecursiveLevel));
            }

            return list;
        }
    }

    public class BaseEntityModel<E, M> : BaseModel<E, M> where E : class where M : class, new()
    {
        //[Display(Name = "Created By")]
        //public String CreateBy { get; set; }
        //[Display(Name = "Created Date")]
        //public DateTime CreateDate { get; set; }
        //[Display(Name = "Modifiy By")]
        //public String ModifyBy { get; set; }
        //[Display(Name = "Modify Date")]
        //public DateTime ModifyDate { get; set; }

    }
}