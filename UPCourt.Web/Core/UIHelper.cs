﻿using log4net;
using Microsoft.AspNet.Identity;
//using Microsoft.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Drawing;
using QRCoder;
using UPCourt.Entity;
using UPCourt.Web.Core;
using System.Web.Hosting;
using System.IO;

namespace UPCourt.Web.Core
{
    public static class UIHelper
    {
        public static String GetUserName(this BaseMVCController controller)
        {
            return controller.User.Identity.GetUserName();
        }

        private static readonly ILog Log =
              LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static ILog getLogger()
        {
            return Log;
        }

        //public static MvcHtmlString MenuLink<TController>(this HtmlHelper helper, Expression<Action<TController>> action, string linkText) where TController : Controller
        //{
        //    var linkRoute = Microsoft.Web.Mvc.Internal.ExpressionHelper.GetRouteValuesFromExpression(action);

        //    var currentRoute = helper.ViewContext.RouteData.Values;

        //    var actionLink = helper.ActionLink(action, linkText);

        //    var isCurrentRoute = linkRoute["controller"].ToString() == currentRoute["controller"].ToString() &&
        //        linkRoute["action"].ToString() == currentRoute["action"].ToString();

        //    return MvcHtmlString.Create(string.Format("<li{0}>", isCurrentRoute ? " class=\"active\"" : string.Empty) + actionLink + "</li>");
        //}

        public static MvcHtmlString ActivePage(this HtmlHelper helper, string linkText, string action, string controller, LinkType linkType = LinkType.NavItem)
        {
            return ActivePage2(helper, linkText, action, controller, linkType);

            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();

            //if (currentController == controller && currentAction == action)
            //{
            //    classValue = "selected";
            //}
            var isCurrentRoute = currentController == controller;
            var actionclassName = linkType == LinkType.NavItem ? "nav-link " : "dropdown-item ";
            actionclassName += (isCurrentRoute ? " active" : string.Empty);
            var actionLink = helper.ActionLink(linkText, action, controller).ToString();
            actionLink = actionLink.ToString().Replace("<a ", "<a class=\"" + actionclassName + "\" ");

            if (linkType == LinkType.DropdownItem || linkType == LinkType.DropdownSubmenu)
            {
                return MvcHtmlString.Create(actionLink);
            }

            var liClassName = "nav-item ";
            var liLink = MvcHtmlString.Create("<li class=\"" + liClassName + "\">" + actionLink + "</li>");
            //var anchorLink = MvcHtmlString.Create(liLink.ToString().Replace("<a ", "<a class=\"" + className + "\" "));
            //var r = MvcHtmlString.Create(string.Format("<li class=\"nav-item\"><a class=\"{1}\" href=\"/"+controller+"/"+action+"\">{0}</a></li>", linkText, className));
            return liLink;


        }

        public static MvcHtmlString ActivePage2(this HtmlHelper helper, string linkText, string action, string controller, LinkType linkType = LinkType.NavItem)
        {


            string currentController = helper.ViewContext.Controller.ValueProvider.GetValue("controller").RawValue.ToString();
            string currentAction = helper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();

            //if (currentController == controller && currentAction == action)
            //{
            //    classValue = "selected";
            //}
            var isCurrentRoute = currentController == controller && currentAction == action;
            string actionclassName = string.Empty;
            string actionLink = string.Empty;
            switch (linkType)
            {
                case LinkType.NavItem:
                case LinkType.DropdownItem:
                    actionclassName = linkType == LinkType.NavItem ? "nav-link " : "dropdown-item ";
                    actionclassName += (isCurrentRoute ? " active" : string.Empty);
                    actionLink = helper.ActionLink(linkText, action, controller).ToString();
                    actionLink = actionLink.ToString().Replace("<a ", "<a class=\"" + actionclassName + "\" ");
                    break;

                //actionclassName = linkType == LinkType.NavItem ? "nav-link " : "dropdown-item ";
                //actionclassName += (isCurrentRoute ? " active" : string.Empty);
                //actionLink = helper.ActionLink(linkText, action, controller).ToString();
                //actionLink = actionLink.ToString().Replace("<a ", "<a class=\"" + actionclassName + "\" ");
                //break;
                case LinkType.DropdownSubmenu:
                    actionclassName = "dropdown-item dropdown-toggle ";
                    actionLink = helper.ActionLink(linkText, action, controller).ToString();
                    actionLink = actionLink.ToString().Replace("<a ", "<button type=\"button\" data-toggle=\"dropdown\" class=\"" + actionclassName + "\" ").Replace("</a>", "</button>");
                    break;
                default:
                    break;
            }

            //if (linkType == LinkType.DropdownItem || linkType == LinkType.DropdownSubmenu)
            {
                return MvcHtmlString.Create(actionLink);
            }

            var liClassName = "nav-item ";
            var liLink = MvcHtmlString.Create("<li class=\"" + liClassName + "\">" + actionLink + "</li>");
            //var anchorLink = MvcHtmlString.Create(liLink.ToString().Replace("<a ", "<a class=\"" + className + "\" "));
            //var r = MvcHtmlString.Create(string.Format("<li class=\"nav-item\"><a class=\"{1}\" href=\"/"+controller+"/"+action+"\">{0}</a></li>", linkText, className));
            return liLink;


        }




        public static Bitmap GetQrCodeBitmap(string code)
        {
            if (code == null)
            {
                code = "KSSoftware";
            }
            QRCodeGenerator qr = new QRCodeGenerator();
            QRCodeData data = qr.CreateQrCode(code, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(data);
            return qrCode.GetGraphic(20);
        }

        public static byte[] GetQrCodeBitmap2(string code)
        {
            var bitmap = GetQrCodeBitmap(code);
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bitmap, typeof(byte[]));

        }

        public static List<EnumResponseModel> GetEnumResponseModelMembers<E>()
        {
            var members = Enum.GetValues(typeof(E));
            var List = new List<EnumResponseModel>();
            foreach (Enum policyterm in members)
            {
                var obj = new EnumResponseModel();
                obj.Value = policyterm.ToString();
                obj.Name = Extended.GetDescription(policyterm);
                List.Add(obj);
            }

            return List;
        }

        public static List<SelectListItem> GetSelectListFromEnum<T>(T? preSelected) where T : struct
        {
            var dbTypeList = GetEnumResponseModelMembers<T>();

            List<SelectListItem> TypeList = dbTypeList.Select(F => new SelectListItem() { Text = F.Name, Value = F.Value, Selected = F.Value == preSelected.ToIntN().ToStringX() }).ToList();
            return TypeList;
        }

        public static MvcHtmlString LoadFile(this HtmlHelper helper, string Path)
        {
            string Filepath = HostingEnvironment.MapPath(Path);
            string content = File.ReadAllText(Filepath);
            return MvcHtmlString.Create(content);
        }

        public static string GetEditIdValue()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("id"))
                return (string)routeValues["id"];
            else if (HttpContext.Current.Request.QueryString.AllKeys.Contains("id"))
                return HttpContext.Current.Request.QueryString["id"];

            return string.Empty;
        }

        public static string GetControllerName()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("controller"))
                return (string)routeValues["controller"];

            return string.Empty;
        }

        public static string GetActionName()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("action"))
                return (string)routeValues["action"];

            return string.Empty;
        }
    }

    public enum LinkType
    {
        NavItem,
        DropdownItem,
        DropdownSubmenu
    }
}