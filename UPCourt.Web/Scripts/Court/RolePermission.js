﻿$(function () {
    $('input[type=checkbox]').change(function () {
        var identifier = $(this).attr('id')
        var domHidden = $("input[type='hidden'][identifier='" + identifier + "']")
        if (this.checked) {
            $(domHidden).val('True')
        }
        else {
            $(domHidden).val('False')
        }
    });
})