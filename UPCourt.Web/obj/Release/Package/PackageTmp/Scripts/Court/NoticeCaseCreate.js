﻿$(function () {
    $('#fromdate').datetimepicker({
        format: 'DD-MMM-YYYY'
    });
    $('#todate').datetimepicker({
        format: 'DD-MMM-YYYY'
    });

    $("[data-card-widget='collapse']").click()
    $("[role='button']").click(function () {
        $(this).find('i').toggleClass('fas fa-plus fas fa-minus');
        $(this).nextAll('.card-body').first().toggle("slow", function () {
            if ($(this).is(":visible")) {
                $(this).show()
            }
            else {
                $(this).hide()
            }
        })
    });

    $("[type='htmleditor']").summernote();

    $("button[action='remove']").attr('disabled', 'disabled')

    $("#addNew").click(function () {
        var lastDiv = $("div[replicate='true']").last()
        var newDiv = $(lastDiv).clone()
        $(newDiv).find("div[role='textbox']").html('');
        $(newDiv).find("input[type='text']").val('');
        $(newDiv).find("input[type='file']").val('');

        $($(newDiv).html()).insertAfter(lastDiv).hide().show('slow');
        //$('body, html').animate({ scrollTop: $(newDiv).offset().top });

        $("button[action='remove']").removeAttr('disabled')
    });

    $(document).on("click", "button[action='remove']", function () {
        var del = $(this).closest(".form-group")
        $(del).hide(1000, function () {
            $(del).remove();
            if ($("button[action='remove']").length == 1) {
                $("button[action='remove']").attr('disabled', 'disabled')
            }
        });
    })
})