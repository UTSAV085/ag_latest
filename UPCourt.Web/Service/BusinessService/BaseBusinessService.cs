﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.BusinessService
{
    public class BaseBusinessService
    {
        public static string GetInvoiceNo(string Prefix, int SerialNo)
        {
            string Number = "000000" + SerialNo;
            var InvoiceNo = Prefix
                                        + "-"
                                        + Extended.CurrentIndianTime.ToString("ddMMyyyy")
                                        + "-"
                                        + Number.Substring(Number.Length - 6);
            return InvoiceNo;
        }
    }
}