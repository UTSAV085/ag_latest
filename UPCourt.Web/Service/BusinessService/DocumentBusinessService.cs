﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UPCourt.Entity;

namespace UPCourt.Web.Service.BusinessService
{
    public class DocumentBusinessService
    {
        public static void AssignEntityIdAndType(ICollection<Document> entityDocs, IEnumerable<Document> newDocuments, Expression<Func<Document, Guid?>> memberLamda, Guid EntityId, DocumentEntityType entityType)
        {
            foreach (var item in newDocuments)
            {
                Extended.SetPropertyValue(item, memberLamda, EntityId);
                Extended.SetPropertyValue(item, i => i.EntityType, entityType);
                entityDocs.Add(item);
            }

        }
    }
}