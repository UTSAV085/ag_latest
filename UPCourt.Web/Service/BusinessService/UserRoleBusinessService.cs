﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Service.BusinessService
{
    public class UserRoleBusinessService : BaseMVCController
    {
        public void InitilizeUserRoles(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRoleMvcModel.Name), selectedValue);
        }
    }
}