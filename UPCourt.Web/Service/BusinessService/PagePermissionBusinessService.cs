﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Service.BusinessService
{
    public class PagePermissionBusinessService
    {
        public static List<EnumResponseModel> ToEnumResponseModel(IEnumerable<PagePermissionType> pagePermissions)
        {
            List<EnumResponseModel> destructred = new List<EnumResponseModel>();
            foreach (var item in pagePermissions)
            {
                var display = item.GetDescription() ?? item.ToString();
                destructred.Add(new EnumResponseModel { Name = display, Value = item.ToString() });
            }

            return destructred;
        }
    }
}