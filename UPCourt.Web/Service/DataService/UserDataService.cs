﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class UserDataService
    {
        public static IQueryable<User> GetLite(DatabaseContext db)
        {
            return db.Users;
        }
        public static IQueryable<User> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.UserRole)
                .ThenInclude(i => i.UserRolePagePermissions)
                .ThenInclude(i => i.PagePermission)
                .Include(i => i.AGDepartment)
                .Include(i => i.GovernmentDepartment);
        }
    }
}