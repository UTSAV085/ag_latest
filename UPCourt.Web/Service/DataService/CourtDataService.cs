﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class CourtDataService
    {
        public static IQueryable<Court> GetLite(DatabaseContext db)
        {
            return db.Courts;
        }
      /*  public static IQueryable<Court> GetLiteWithFilter(DatabaseContext db,List<AdvocateCourt> advocateCourts)
        {
            if(advocateCourts != null)
            {
                return db.Courts.SelectMany(advocateCourts.)
            }
            return db.Courts;
        }*/
        public static IQueryable<Court> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.CourtOpenCloseLogs);
        }
    }
}