﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovernmentDepartmentAddressDataService
    {
        public static IQueryable<GovernmentDepartmentAddress> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentAddresses;
        }

    }
}