﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovermentDepartmentDataService
    {

        public static IQueryable<GovernmentDepartment> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartments;
        }
        public static IQueryable<GovernmentDepartment> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.GovernmentDepartmentAddresses)
                .Include(i => i.GovernmentDepartmentContactNos)
                .Include(i => i.PartyAndRespondents);
            
        }
    }
}