﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class AdvocateDataService
    {
        public static IQueryable<Advocate> GetLite(DatabaseContext db)
        {
            return db.Advocates;
        }
        public static IQueryable<Advocate> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.Designation)
                .Include(i => i.AdvocateCourts);

        }
    }
}