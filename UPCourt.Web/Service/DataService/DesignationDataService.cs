﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class DesignationDataService
    {
        public static IQueryable<Designation> GetLite(DatabaseContext db)
        {
            return db.Designations;
        }
        public static IQueryable<Designation> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.Advocates);
        }
    }
}