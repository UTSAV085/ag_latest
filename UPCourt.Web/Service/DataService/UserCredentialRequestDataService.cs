﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class UserCredentialRequestDataService
    {
        public static IQueryable<UserCredentialRequest> GetLite(DatabaseContext db)
        {
            return db.UserCredentialRequests;
        }
        public static IQueryable<UserCredentialRequest> GetDetail(DatabaseContext db)
        {
            return GetLite(db)
                .Include(i => i.Region);
        }
    }
}