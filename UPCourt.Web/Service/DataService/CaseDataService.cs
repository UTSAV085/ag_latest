﻿using Microsoft.EntityFrameworkCore;
using UPCourt.Entity;
using System.Linq;

namespace UPCourt.Web.Service.DataService
{
    public class CaseDataService
    {
        public static IQueryable<Case> GetLite(DatabaseContext db)
        {
            return db.Cases;
        }
        public static IQueryable<Case> GetDetail(DatabaseContext db)
        {
            return db.Cases
                .Include(F => F.Advocate)
                .Include(F => F.Court)
                .Include(F => F.Region)
                .Include(F => F.Notice).ThenInclude(F => F.NoticeCounselor)
                .Include(F => F.Notice).ThenInclude(F => F.Parties)
                .Include(F => F.Notice).ThenInclude(F => F.BasicIndexs)
                .Include(F => F.Notice).ThenInclude(F => F.Petitioner)
                .Include(F => F.Petitioner)
                .Include(F => F.NoticeCounselor)
                .Include(F => F.UserCases)
                .Include(F => F.Parties).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.Respondents).ThenInclude(F => F.GovernmentDepartment)
                .Include(F => F.BasicIndexs).ThenInclude(F => F.Documents)
                .Include(F => F.CaseHearings).ThenInclude(F => F.Documents)
                .Include(F => F.CaseStatus)
                .Include(F => F.CaseTransfers);
        }
    }
}