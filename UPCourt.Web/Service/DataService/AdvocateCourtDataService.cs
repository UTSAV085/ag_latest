﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class AdvocateCourtDataService
    {
        public static IQueryable<AdvocateCourt> GetLite(DatabaseContext db)
        {
            return db.AdvocateCourts;
        }
    }
}