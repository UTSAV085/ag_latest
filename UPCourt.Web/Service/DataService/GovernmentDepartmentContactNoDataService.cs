﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class GovernmentDepartmentContactNoDataService
    {
        public static IQueryable<GovernmentDepartmentContactNo> GetLite(DatabaseContext db)
        {
            return db.GovernmentDepartmentContactNos;
        }
    }
}