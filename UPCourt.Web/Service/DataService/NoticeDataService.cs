﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;

namespace UPCourt.Web.Service.DataService
{
    public class NoticeDataService
    {
        public static IQueryable<Notice> GetLite(DatabaseContext db)
        {
            return db.Notices;
        }
        public static IQueryable<Notice> GetDetail(DatabaseContext db)
        {
            return db.Notices
                .Include(i => i.Petitioner)
                .Include(i => i.NoticeCounselor)
                .Include(i => i.Parties)
                .Include(i => i.Respondents)
                .Include(i => i.BasicIndexs);
        }
    }
}