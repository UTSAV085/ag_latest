﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class NoticeMvcModel : BaseEntityModel<Notice, NoticeMvcModel>
    {
        public Guid NoticeId { get; set; }
        public Guid PetitionerId { get; set; }
        public Guid? NoticeCounselorId { get; set; }
        [Required]
        [Display(Name = "Notice Type")]
        public NoticeType NoticeType { get; set; }
        public int SerialNo { get; set; }
        [Display(Name = "Notice No.")]
        public string NoticeNo { get; set; }
        [Required]
        public DateTime? NoticeDate { get; set; }
        //case details
        public string Subject { get; set; }
        //only for Dashboard
        [Display(Name = "Is Context Order")]
        public bool IsContextOrder { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        [Required]
        public NoticeStatus Status { get; set; }
        public PetitionerMvcModel Petitioner { get; set; }
        public NoticeCounselorMvcModel NoticeCounselor { get; set; }
        public List<NoticePartyMvcModel> NoticeParties { get; set; }
        public List<NoticeRespondentMvcModel> NoticeRespondent { get; set; }
        public List<NoticeBasicIndexMvcModel> NoticeBasicIndex { get; set; }
        public override NoticeMvcModel EntityToResponse(Notice entity, NoticeMvcModel model)
        {
            model.Petitioner = PetitionerMvcModel.CopyFromEntity(entity.Petitioner, model.RecursiveLevel);
            model.NoticeCounselor = NoticeCounselorMvcModel.CopyFromEntity(entity.NoticeCounselor, model.RecursiveLevel);
            model.NoticeParties = NoticePartyMvcModel.CopyFromEntityList(entity.Parties, model.RecursiveLevel);
            model.NoticeRespondent = NoticeRespondentMvcModel.CopyFromEntityList(entity.Respondents, model.RecursiveLevel);
            model.NoticeBasicIndex = NoticeBasicIndexMvcModel.CopyFromEntityList(entity.BasicIndexs, model.RecursiveLevel);
            return model;
        }
    }
    public class PetitionerMvcModel : BaseEntityModel<Petitioner, PetitionerMvcModel>
    {
        public Guid PetitionerId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        [Display(Name = "Contact No")]
        public string ContactNo { get; set; }
        [Required]
        public string Email { get; set; }
        public string District { get; set; }
        [Required]
        [Display(Name = "ID Proof")]
        public IDProofType IDProof { get; set; }
        [Required]
        [Display(Name = "ID Proof Number")]
        public string IDProofNumber { get; set; }
    }
    public class NoticeCounselorMvcModel : BaseEntityModel<NoticeCounselor, NoticeCounselorMvcModel>
    {
        public Guid NoticeCounselorId { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Roll No.")]
        public string RollNo { get; set; }
        [Required]
        [Display(Name = "Enrollment No.")]
        public string EnrollmentNo { get; set; }
        [Display(Name = "Contact No.")]
        public string ContactNo { get; set; }
        [Required]
        public string Email { get; set; }
    }
    public class NoticePartyMvcModel : BaseEntityModel<PartyAndRespondent, NoticePartyMvcModel>
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticePartyId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public Guid? CasePartyId { get; set; }
        public Guid? CaseRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
        //CaseId needed
        public PartyType PartyType { get; set; }
        [Display(Name = "Party Name")]
        public string PartyName { get; set; }
        [Display(Name = "Contact No.")]
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        //department details
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        [Display(Name = "If Party Department")]
        public bool IsPartyDepartment { get; set; }
        public bool DeleteNoticeParty { get; set; }
        [FilterUIHint("GovermentDepartmentForNoticeMvcModel")]
        public GovermentDepartmentForNoticeMvcModel GovermentDepartment { get; set; }
        public override NoticePartyMvcModel EntityToResponse(PartyAndRespondent entity, NoticePartyMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentForNoticeMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            return model;
        }
    }
    public class NoticeRespondentMvcModel : BaseEntityModel<PartyAndRespondent, NoticeRespondentMvcModel>
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticePartyId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public Guid? CasePartyId { get; set; }
        public Guid? CaseRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
        //CaseId needed
        public PartyType PartyType { get; set; }
        [Display(Name = "Party Name")]
        public string PartyName { get; set; }
        [Display(Name = "Contact No.")]
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        //department details
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        [Display(Name = "If Party Department")]
        public bool IsPartyDepartment { get; set; }
        public bool DeleteRespondentParty { get; set; }
        [FilterUIHint("GovermentDepartmentForNoticeMvcModel")]
        public GovermentDepartmentForNoticeMvcModel GovermentDepartment { get; set; }
        public override NoticeRespondentMvcModel EntityToResponse(PartyAndRespondent entity, NoticeRespondentMvcModel model)
        {
            model.GovermentDepartment = GovermentDepartmentForNoticeMvcModel.CopyFromEntity(entity.GovernmentDepartment, model.RecursiveLevel);
            return model;
        }
    }
    public class GovermentDepartmentForNoticeMvcModel : BaseEntityModel<GovernmentDepartment, GovermentDepartmentForNoticeMvcModel>
    {
        public Guid GovernmentDepartmentId { get; set; }
        public string Name { get; set; }
    }

    public class NoticeCaseIndexMvcModel
    {
        public Guid NoticeId { get; set; }
        [Display(Name = "Notice No.")]
        public string NoticeNo { get; set; }
        [Display(Name = "Notice From")]
        public DateTime? NoticeFromDate { get; set; }
        [Display(Name = "Notice To")]
        public DateTime? NoticeToDate { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter case no.")]
        [Display(Name = "Case No")]
        public string CaseNo { get; set; }
        public List<SelectListItem> CaseStatuses { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select case status")]
        [Display(Name = "Case Status")]
        public Guid CaseStatusId { get; set; }
        public List<NoticeMvcModel> Notices { get; set; }
    }

    public class NoticeBasicIndexMvcModel : BaseEntityModel<NoticeBasicIndex, NoticeBasicIndexMvcModel>
    {
        public Guid NoticeBasicIndexId { get; set; }
        public Guid? NoticeId { get; set; }
        public Guid? CaseId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public string Particulars { get; set; }
        public string Annexure { get; set; }
        public int SerialNo { get; set; }
        [Display(Name = "Index Date")]
        public DateTime? IndexDate { get; set; }
        [Display(Name = "No. Of Pages")]
        public int? NoOfPages { get; set; }
        public List<DocumentMvcModel> Documents { get; set; }
        public override NoticeBasicIndexMvcModel EntityToResponse(NoticeBasicIndex entity, NoticeBasicIndexMvcModel model)
        {
            model.Documents = DocumentMvcModel.CopyFromEntityList(entity.Documents, model.RecursiveLevel);
            return model;
        }
    }
    public class DocumentMvcModel : BaseEntityModel<Document, DocumentMvcModel>
    {
        public Guid DocumentId { get; set; }
        [Display(Name = "Upload Document Name")]
        [Required]
        public string Name { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        public Guid? CaseHearingId { get; set; }
        public DocumentType Type { get; set; }
        public DocumentEntityType EntityType { get; set; }
        public string DocumentUrl { get; set; }
        public int Ordinal { get; set; }
        public DocumentStatus Status { get; set; }
        public int Size { get; set; }
        //Extra props for business requiremnts
        public int PageCount { get; set; }
        public string PageRange { get; set; }
        [Display(Name = "Comments")]
        [AllowHtml]
        public string Description { get; set; }
    }
}