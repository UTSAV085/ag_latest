﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class DesignationCreateRequestModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        public DesignationStatus Status { get; set; }
        public bool IsDefault { get; set; }
    }

    public class DesignationResponseModel : BaseEntityModel<Designation, DesignationResponseModel>
    {
        public Guid DesignationId { get; set; }
        [Required]
        [Display(Name ="Designation")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }
        public DesignationStatus Status { get; set; }
        public bool IsDefault { get; set; }
    }
}