﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class AdvocateMvcModel: BaseEntityModel<Advocate, AdvocateMvcModel>
    {
        [Required]
        public Guid AdvocateId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public AdvocateStatus Status { get; set; }
        public Guid? DesignationId { get; set; }
        public Guid? CourtId { get; set; }
        public DesignationResponseModel Designation { get; set; }
       // public CourtMvcModel Court { get; set; }        
        public List<Guid> CourtTest { get; set; }
        public List<AdvocateCourtMvcModel> AdvocateCourts { get; set; }
        public override AdvocateMvcModel EntityToResponse(Advocate entity, AdvocateMvcModel model)
        {
            model.Designation = DesignationResponseModel.CopyFromEntity(entity.Designation, 0);
            model.AdvocateCourts = AdvocateCourtMvcModel.CopyFromEntityList(entity.AdvocateCourts, 0);
            return model;


        }
    }
}