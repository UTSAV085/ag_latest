﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class UserLoginRequestModel
    {
        [Required]
        public string EmailId { get; set; }
        [Required]
        public string Password { get; set; }
    }
    public class UserMvcModel : BaseEntityModel<User, UserMvcModel>
    {
        public Guid UserId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        [Required]
        public string Email { get; set; }
        public string LoginId { get; set; }
        //[Required]
        public string Password { get; set; }
        [Required]
        public UserStatus Status { get; set; }
        [Required]
        [Display(Name = "User Type")]
        public UserType UserType { get; set; }
        public List<SelectListItem> UserTypeList { get; set; }
        public bool IsDefault { get; set; }
        [Required]
        [Display(Name = "Role")]
        public Guid UserRoleId { get; set; }
        [Display(Name = "AG-Department")]
        public Guid? AGDepartmentId { get; set; }
        [Display(Name = "Government Department")]
        public Guid? GovernmentDepartmentId { get; set; }
        public UserRoleMvcModel UserRole { get; set; }
        public AGDepartmentMvcModel AGDepartment { get; set; }
        public GovermentDepartmentMvcModel GovermentDepartment { get; set; }
        public override UserMvcModel EntityToResponse(User entity, UserMvcModel model)
        {
            model.UserRole = UserRoleMvcModel.CopyFromEntity(entity.UserRole, 0);
            model.AGDepartment = AGDepartmentMvcModel.CopyFromEntity(entity.AGDepartment, 0);
            model.GovermentDepartment = GovermentDepartmentMvcModel.CopyFromEntity(entity.GovernmentDepartment, 0);
            //model.UserType = UserBusinessService.UserTypeEnumResponseModel(entity.UserType);
            return model;
        }
    }
}