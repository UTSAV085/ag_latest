﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CaseMvcModel : BaseEntityModel<Case, CaseMvcModel>
    {
        public Guid CaseId { get; set; }
        public Guid NoticeId { get; set; }
        public int OrdinalNo { get; set; }
        [Required]
        [Display(Name = "Notice Type")]
        public string PetitionNo { get; set; }
        [Display(Name = "Case Status")]
        public Guid? CaseStatusId { get; set; }
        [Display(Name = "Advocate")]
        public Guid? AdvocateId { get; set; }
        [Display(Name = "Court")]
        public Guid? CourtId { get; set; }
        [Display(Name = "Region")]
        public Guid? RegionId { get; set; }
        [Display(Name = "Status2")]
        public CaseMovementStatus? Status2 { get; set; }
        public NoticeType Type { get; set; }
        [Required]
        [Display(Name = "Petitioner")]
        public Guid PetitionerId { get; set; }
        [Display(Name = "Notice Counselor")]
        public Guid? NoticeCounselorId { get; set; }
        [Display(Name = "Next Hearing Date")]
        public DateTime? NextHearingDate { get; set; }
        public CaseStatusMvcModel CaseStatus { get; set; }
        public AdvocateMvcModel Advocate { get; set; }
        public CourtMvcModel Court { get; set; }
        public RegionMvcModel Region { get; set; }
        public NoticeMvcModel Notice { get; set; }
        public PetitionerMvcModel Petitioner { get; set; }
        public NoticeCounselorMvcModel NoticeCounselor { get; set; }
        public ICollection<Case> Cases { get; set; }
        public ICollection<UserCaseMvcModel> UserCases { get; set; }
        public ICollection<NoticePartyMvcModel> Parties { get; set; }
        public ICollection<NoticePartyMvcModel> Respondents { get; set; }
        public ICollection<NoticeBasicIndexMvcModel> BasicIndexs { get; set; }
        public ICollection<CaseHearingMvcModel> CaseHearings { get; set; }
        public ICollection<CaseTransferMvcModel> CaseTransfers { get; set; }
        public override CaseMvcModel EntityToResponse(Case entity, CaseMvcModel model)
        {
            model.Advocate = AdvocateMvcModel.CopyFromEntity(entity.Advocate, model.RecursiveLevel);
            model.Court = CourtMvcModel.CopyFromEntity(entity.Court, model.RecursiveLevel);
            model.Region = RegionMvcModel.CopyFromEntity(entity.Region, model.RecursiveLevel);
            model.Notice = NoticeMvcModel.CopyFromEntity(entity.Notice, model.RecursiveLevel);
            model.Petitioner = PetitionerMvcModel.CopyFromEntity(entity.Petitioner, model.RecursiveLevel);
            model.NoticeCounselor = NoticeCounselorMvcModel.CopyFromEntity(entity.NoticeCounselor, model.RecursiveLevel);
            model.UserCases = UserCaseMvcModel.CopyFromEntityList(entity.UserCases, model.RecursiveLevel);
            model.Parties = NoticePartyMvcModel.CopyFromEntityList(entity.Parties, model.RecursiveLevel);
            model.Respondents = NoticePartyMvcModel.CopyFromEntityList(entity.Respondents, model.RecursiveLevel);
            model.BasicIndexs = NoticeBasicIndexMvcModel.CopyFromEntityList(entity.BasicIndexs, model.RecursiveLevel);
            model.CaseHearings = CaseHearingMvcModel.CopyFromEntityList(entity.CaseHearings, model.RecursiveLevel);
            model.CaseTransfers = CaseTransferMvcModel.CopyFromEntityList(entity.CaseTransfers, model.RecursiveLevel);
            model.CaseStatus = CaseStatusMvcModel.CopyFromEntity(entity.CaseStatus, model.RecursiveLevel);
            return model;
        }
    }
    public class CaseHearingMvcModel : BaseEntityModel<CaseHearing, CaseHearingMvcModel>
    {
        public Guid CaseHearingId { get; set; }
        public Guid CaseId { get; set; }
        [Display(Name = "Hearing Date")]
        public DateTime? HearingDate { get; set; }
        [Display(Name = "Next Hearing Date")]
        public DateTime? NextHearingDate { get; set; }
        public CaseMvcModel Case { get; set; }
        public ICollection<DocumentMvcModel> Documents { get; set; }
        public override CaseHearingMvcModel EntityToResponse(CaseHearing entity, CaseHearingMvcModel model)
        {
            model.Case = CaseMvcModel.CopyFromEntity(entity.Case, model.RecursiveLevel);
            model.Documents = DocumentMvcModel.CopyFromEntityList(entity.Documents, model.RecursiveLevel);
            return model;
        }
    }

    public class CaseTransferMvcModel : BaseEntityModel<CaseTransfer, CaseTransferMvcModel>
    {
        public Guid CaseTransferId { get; set; }
        public Guid CaseId { get; set; }
        public Guid CourtId { get; set; }
        public DateTime? TransferDate { get; set; }
        public string Description { get; set; }
        public CaseMvcModel Case { get; set; }
        public CourtMvcModel Court { get; set; }
        public override CaseTransferMvcModel EntityToResponse(CaseTransfer entity, CaseTransferMvcModel model)
        {
            model.Case = CaseMvcModel.CopyFromEntity(entity.Case, model.RecursiveLevel);
            model.Court = CourtMvcModel.CopyFromEntity(entity.Court, model.RecursiveLevel);
            return model;
        }
    }

    public class NewCaseHearingMvcModel
    {
        public CaseHearingMvcModel Hearing { get; set; }
        public DocumentMvcModel Document { get; set; }
        [Required]
        [Display(Name = "Hearing case documents")]
        public System.Web.HttpPostedFileBase UploadFile { get; set; }
    }

    public class CaseFilterMvcModel : BaseEntityModel<Case, CaseFilterMvcModel>
    {
        [Display(Name = "Case Status")]
        public Guid? CaseStatusActive { get; set; }

        [Display(Name = "Case Status")]
        public Guid? CaseStatusTransfer { get; set; }

        [Display(Name = "Case Type")]
        public string CaseTypeActive { get; set; }

        [Display(Name = "Case Type")]
        public string CaseTypeDispose { get; set; }
        [Display(Name = "Case Type")]
        public string CaseTypeTransfer { get; set; }

        [Display(Name = "Date")]
        public DateTime? CaseDateActive { get; set; }
        [Display(Name = "Date")]
        public DateTime? CaseDateDispose { get; set; }
        [Display(Name = "Date")]
        public DateTime? CaseDateTransfer { get; set; }

        public AdvocateMvcModel Advocate { get; set; }
        public CourtMvcModel OldCourt { get; set; }
        public CourtMvcModel NewCourt { get; set; }
        public string Description { get; set; }

        public ICollection<CaseMvcModel> AcctiveCases { get; set; }
        public ICollection<CaseMvcModel> DisposedCases { get; set; }
        public ICollection<CaseMvcModel> CaseTransfer { get; set; }

        public ICollection<CaseMvcModel> Cases { get; set; }

        public override CaseFilterMvcModel EntityToResponse(Case entity, CaseFilterMvcModel model)
        {
            return model;
        }
    }
}