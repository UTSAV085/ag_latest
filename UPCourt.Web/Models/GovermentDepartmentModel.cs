﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class GovermentDepartmentMvcModel: BaseEntityModel<GovernmentDepartment, GovermentDepartmentMvcModel>
    {
      
        public Guid GovernmentDepartmentId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string HeadOffice { get; set; }
        [Required]
        public string POCName { get; set; }
        [Required]
        public string POCContact { get; set; }
        public bool IsDefault { get; set; }
        public bool IsAssignUser { get; set; }
        public GovtDeparmentStatus Status { get; set; }
        [FilterUIHint("GovernmentDepartmentContactNoMvcModel")]
        public List<GovernmentDepartmentContactNoMvcModel> GovernmentDepartmentContactNos { get; set; }
        [FilterUIHint("GovernmentDepartmentAddressMvcModel")]
        public List<GovernmentDepartmentAddressMvcModel> GovernmentDepartmentAddresses { get; set; }
        public override GovermentDepartmentMvcModel EntityToResponse(GovernmentDepartment entity, GovermentDepartmentMvcModel model)
        {
            model.GovernmentDepartmentContactNos = GovernmentDepartmentContactNoMvcModel.CopyFromEntityList(entity.GovernmentDepartmentContactNos, 0);
            model.GovernmentDepartmentAddresses = GovernmentDepartmentAddressMvcModel.CopyFromEntityList(entity.GovernmentDepartmentAddresses, model.RecursiveLevel);
            return model;
        }
    }

    public class GovernmentDepartmentContactNoMvcModel : BaseEntityModel<GovernmentDepartmentContactNo, GovernmentDepartmentContactNoMvcModel>
    {
        public Guid GovernmentDepartmentContactNoId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string ContactNo { get; set; }
        public bool DeletePhone { get; set; }

    }
    public class GovernmentDepartmentContactNoRequestModel
    {
      
        public string ContactNo { get; set; }
        public bool DeletePhone { get; set; }
    }

    public class GovernmentDepartmentAddressMvcModel : BaseEntityModel<GovernmentDepartmentAddress, GovernmentDepartmentAddressMvcModel>
    { 
        public Guid GovernmentDepartmentAddressId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string Address { get; set; }
        public bool IsHeadOfficeAddress { get; set; }
        public bool DeleteAddress { get; set; }
    }
}