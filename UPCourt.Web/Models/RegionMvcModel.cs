﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class RegionMvcModel : BaseEntityModel<Region, RegionMvcModel>
    {
        public Guid RegionId { get; set; }
        [Required]
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string Description { get; set; }
        [Required]
        public RegionStatus Status { get; set; }
    }
}