﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Service.BusinessService;

namespace UPCourt.Web.Models
{
    public class UserRoleMvcModel : BaseEntityModel<UserRole, UserRoleMvcModel>
    {
        public Guid UserRoleId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public UserRoleStatus Status { get; set; }
        public bool IsSytemCreated { get; set; }
        public ICollection<UserRolePagePermissionMvcModel> UserRolePagePermissions { get; set; }
        public override UserRoleMvcModel EntityToResponse(UserRole entity, UserRoleMvcModel model)
        {
            model.UserRolePagePermissions = UserRolePagePermissionMvcModel.CopyFromEntityList(entity.UserRolePagePermissions, model.RecursiveLevel);
            return model;
        }
    }

    public class UserRolePermissionMvcModel : UserRoleMvcModel
    {
        public List<PagePermissionMvcModel> PagePermissions { get; set; }
        
    }
    public class UserRolePagePermissionMvcModel : BaseEntityModel<UserRolePagePermission, UserRolePagePermissionMvcModel>
    {
        public Guid UserRolePagePermissionId { get; set; }
        public Guid UserRoleId { get; set; }
        public Guid PagePermissionId { get; set; }
        public IEnumerable<PagePermissionType> PermittedPermissionTypes { get; set; }
        public PagePermissionMvcModel PagePermission { get; set; }
        public override UserRolePagePermissionMvcModel EntityToResponse(UserRolePagePermission entity, UserRolePagePermissionMvcModel model)
        {
            model.PagePermission = PagePermissionMvcModel.CopyFromEntity(entity.PagePermission, model.RecursiveLevel);
            return model;
        }
    }

    public class PagePermissionMvcModel : BaseModel<PagePermission, PagePermissionMvcModel>
    {
        public Guid PagePermissionId { get; set; }
        public IEnumerable<PagePermissionType> PermissionTypes { get; set; }
        public PageName PageName { get; set; }
        public string PageNameDisplay { get; set; }
        public List<EnumResponseModel> PermissionTypesDestructed { get; set; }
        public override PagePermissionMvcModel EntityToResponse(PagePermission entity, PagePermissionMvcModel model)
        {
            model.PageNameDisplay = entity.PageName.GetCustomAttribute<PagePermissionAttribute>().DisplayText;
            model.PermissionTypesDestructed = PagePermissionBusinessService.ToEnumResponseModel(entity.PermissionTypes);
            return model;
        }
    }
}