﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CaseStatusMvcModel : BaseEntityModel<CaseStatus, CaseStatusMvcModel>
    {
        public Guid CaseStatusId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Hex Color Code")]
        public string HexColorCode { get; set; }
        public CaseStatusType Type { get; set; }
    }
}