﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class AdvocateCourtMvcModel : BaseEntityModel<AdvocateCourt, AdvocateCourtMvcModel>
    {
        public Guid AdvocateCourtId { get; set; }
        public Guid? AdvocateId { get; set; }
        public Guid? CourtId { get; set; }
        public AdvocateMvcModel Advocate { get; set; }
        public CourtMvcModel Court { get; set; }
    }
}