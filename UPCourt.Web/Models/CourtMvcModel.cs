﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class CourtMvcModel : BaseEntityModel<Court, CourtMvcModel>
    {
        public Guid CourtId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Number { get; set; }
        [Display(Name = "Court Bench")]
        public CourtBenchType CourtBench { get; set; }
        [Required]
        public CourtStatus Status { get; set; }
        [Display(Name = "Is Close")]
        public bool IsClose { get; set; }
        [Display(Name = "Close From Date")]
        public DateTime? CloseFromDate { get; set; }
        [Display(Name = "Close To Date")]
        public DateTime? CloseToDate { get; set; }
        public override CourtMvcModel EntityToResponse(Court entity, CourtMvcModel model)
        {
            model.CloseFromDate = entity?.CourtOpenCloseLogs?.OrderBy(i => i.CloseToDate).FirstOrDefault()?.CloseFromDate;
            model.CloseToDate = entity?.CourtOpenCloseLogs?.OrderBy(i => i.CloseToDate).FirstOrDefault()?.CloseToDate;
            return model;
        }
    }
    public class CourtOpenCloseLogMvcModel : BaseEntityModel<CourtOpenCloseLog, CourtOpenCloseLogMvcModel>
    {
        public Guid CourtOpenCloseLogId { get; set; }
        public Guid CourtId { get; set; }
        public DateTime? CloseFromDate { get; set; }
        public DateTime? CloseToDate { get; set; }
    }
}