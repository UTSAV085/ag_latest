﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using UPCourt.Entity;
using UPCourt.Web.Core;

namespace UPCourt.Web.Models
{
    public class UserCaseMvcModel : BaseEntityModel<UserCase, UserCaseMvcModel>
    {
        public Guid UserCaseId { get; set; }
        public Guid UserId { get; set; }
        public Guid CaseId { get; set; }
        public UserMvcModel User { get; set; }
        public CaseMvcModel Case { get; set; }
        public override UserCaseMvcModel EntityToResponse(UserCase entity, UserCaseMvcModel model)
        {
            model.Case = CaseMvcModel.CopyFromEntity(entity.Case, model.RecursiveLevel);
            model.User = UserMvcModel.CopyFromEntity(entity.User, model.RecursiveLevel);
            return model;
        }
    }
}