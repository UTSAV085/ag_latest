﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class UserCredentialRequestController : BaseMVCController
    {
        // GET: UserCredentialRequest
        public ActionResult Index()
        {
            var dbData = UserCredentialRequestDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            return View(UserCredentialRequestModel.CopyFromEntityList(dbData, 0));
        }

        // GET: UserCredentialRequest/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            InitilizeRegions(null);
            var model = new UserCredentialRequestModel();
            return View(model);
        }

        // POST: UserCredentialRequest/Create
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Create(UserCredentialRequestModel model)
        {
            InitilizeRegions(model.RegionId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}
            UserCredentialRequest tempUserCredentialRequest = new UserCredentialRequest();
            tempUserCredentialRequest.UserCredentialRequestId = Guid.NewGuid();
            tempUserCredentialRequest.CreateDate = Extended.CurrentIndianTime.ToString();
            tempUserCredentialRequest.Status = UserCredentialRequestStatus.Pending;
            InitializeData(tempUserCredentialRequest, model);

            db.UserCredentialRequests.Add(tempUserCredentialRequest);
            db.SaveChanges();
            SucessMessage = $"User credential request {tempUserCredentialRequest.Name} created sucessfully";

            return RedirectToAction("Create");
        }

        // GET: UserCredentialRequest/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeRegions(dbData.RegionId);
            return View(UserCredentialRequestModel.CopyFromEntity(dbData, 0));
        }

        // POST: UserCredentialRequest/Edit/5
        [HttpPost]
        public ActionResult Edit(UserCredentialRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = UserCredentialRequestDataService.GetDetail(db).Where(i => i.UserCredentialRequestId == model.UserCredentialRequestId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            dbData.Status = model.Status;
            dbData.ModifyBy = AppUser.UserName;
            //InitializeData(dbData, model);

            db.SaveChanges();
            SucessMessage = $"User credential request {dbData.Name} update sucessfully";

            return RedirectToAction("Index");
        }
        private void InitializeData(UserCredentialRequest tempUserCredentialRequest, UserCredentialRequestModel model)
        {
            tempUserCredentialRequest.Name = model.Name;
            tempUserCredentialRequest.Address = model.Address;
            tempUserCredentialRequest.Email = model.Email;
            tempUserCredentialRequest.MobileNo = model.MobileNo;
            tempUserCredentialRequest.PetitionNo = model.PetitionNo;
            tempUserCredentialRequest.RegionId = model.RegionId;
            tempUserCredentialRequest.ModifyBy = model.Name;
        }
        public void InitilizeRegions(object selectedValue)
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = RegionMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Regions = new SelectList(mvc, nameof(RegionMvcModel.RegionId)
                , nameof(RegionMvcModel.Name), selectedValue);
        }
    }
}
