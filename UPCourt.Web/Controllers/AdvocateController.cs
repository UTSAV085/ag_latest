﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class AdvocateController : BaseMVCController
    {
        // GET: Advocate
        public ActionResult Index()
        {
            var dbAdvocate = AdvocateDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            return View(AdvocateMvcModel.CopyFromEntityList(dbAdvocate,0));
        }

        // GET: Advocate/Create
        public ActionResult Create()
        {
            InitilizeDesignation(null);
            InitilizeCourt(null);
            var model = new AdvocateMvcModel();
            model.Status = AdvocateStatus.Active;
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(AdvocateMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);

            }
            Advocate advocate = new Advocate();
            advocate.AdvocateId = Guid.NewGuid();
            advocate.Name = model.Name;
            advocate.Address = model.Address;
            advocate.ContactNo = model.ContactNo;
            advocate.Email = model.Email;
            advocate.ModifyBy = AppUser.UserName;
            advocate.Status = model.Status;
            advocate.DesignationId = model.DesignationId;
            advocate.AdvocateCourts = new List<AdvocateCourt>();
            foreach(var item in model.CourtTest)
            {
                AdvocateCourt advocateCourt = new AdvocateCourt();
                advocateCourt.AdvocateCourtId = Guid.NewGuid();
                advocateCourt.AdvocateId = advocate.AdvocateId;
                advocateCourt.CourtId = item;
                advocateCourt.ModifyBy = AppUser.UserName;
                db.AdvocateCourts.Add(advocateCourt);
            }
            db.Advocates.Add(advocate);
            db.SaveChanges();
            SucessMessage = $"Advocate {advocate.Name} Created sucessfully";

            return RedirectToAction("Index");
        }
            public void InitilizeDesignation(object selectedValue)
        {
            var dbData = DesignationDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = DesignationResponseModel.CopyFromEntityList(dbData, 0);
            ViewBag.Designations = new SelectList(mvc, nameof(DesignationResponseModel.DesignationId)
                , nameof(DesignationResponseModel.Name), selectedValue);
        }

        public ActionResult Edit(Guid id)
        {
            var dbData = AdvocateDataService.GetDetail(db).Where(i => i.AdvocateId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeDesignation(dbData.DesignationId);
            InitilizeCourt(null);
            return View(AdvocateMvcModel.CopyFromEntity(dbData, 0));
        }
        [HttpPost]
        public ActionResult Edit(AdvocateMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = AdvocateDataService.GetDetail(db).Where(i => i.AdvocateId == model.AdvocateId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }

            dbData.Name = model.Name;
            dbData.ContactNo = model.ContactNo;
            dbData.Email = model.Email;
            dbData.Status = model.Status;
            dbData.Address = model.Address;
            dbData.DesignationId = model.DesignationId;
            dbData.ModifyBy = AppUser.UserName;
            foreach (var item in model.CourtTest)
            {             
                    AdvocateCourt advocateCourt = new AdvocateCourt();
                    advocateCourt.AdvocateCourtId = Guid.NewGuid();
                    advocateCourt.AdvocateId = dbData.AdvocateId;
                    advocateCourt.CourtId = item;
                    advocateCourt.ModifyBy = AppUser.UserName;
                    db.AdvocateCourts.Add(advocateCourt);               
               
            }
            db.SaveChanges();
            SucessMessage = $"Advocate {dbData.Name} update sucessfully";
            return RedirectToAction("Index");
        }
        public void InitilizeCourt(List<AdvocateCourt> selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = CourtMvcModel.CopyFromEntityList(dbData, 0);
            if (!selectedValue.IsEmptyCollection())
            {
                var dbData2 = new List<Court>();
                foreach (var item in selectedValue.ToList())
                {
                   
                    var data = dbData.Find(i => i.CourtId == item.CourtId);
                    dbData2.Add(data);
                     
                }
                mvc = CourtMvcModel.CopyFromEntityList(dbData2, 0);
            }
            
           
            ViewBag.Courts = new MultiSelectList(mvc, nameof(CourtMvcModel.CourtId)
                , nameof(CourtMvcModel.Name));
        }
        public ActionResult Details(Guid id)
        {
            var dbData = AdvocateDataService.GetDetail(db).Where(i => i.AdvocateId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeDesignation(dbData.DesignationId);
            InitilizeCourt(dbData.AdvocateCourts.ToList());


            return View(AdvocateMvcModel.CopyFromEntity(dbData, 0));
        }
        private bool CommonForInsertUpdate(AdvocateMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = AdvocateDataService.GetLite(db).FirstOrDefault(i => i.AdvocateId != model.AdvocateId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "AG-Department name already exists");
                return false;
            }
            return true;
        }
    }
}
