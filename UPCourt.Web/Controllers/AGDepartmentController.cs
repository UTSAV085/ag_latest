﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class AGDepartmentController : BaseMVCController
    {
        // GET: AGDepartment
        public ActionResult Index()
        {
            var dbData = AGDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            return View(AGDepartmentMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: AGDepartment/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(AGDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: AGDepartment/Create
        public ActionResult Create()
        {
            var model = new AGDepartmentMvcModel();
            return View(model);
        }

        // POST: AGDepartment/Create
        [HttpPost]
        public ActionResult Create(AGDepartmentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}
            AGDepartment tempAGDepartment = new AGDepartment();
            tempAGDepartment.AGDepartmentId = Guid.NewGuid();
            InitializeData(tempAGDepartment, model);

            db.AGDepartments.Add(tempAGDepartment);
            db.SaveChanges();
            SucessMessage = $"AG-Department {tempAGDepartment.Name} created sucessfully";

            return RedirectToAction("Index");
        }

        // GET: AGDepartment/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(AGDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: AGDepartment/Edit/5
        [HttpPost]
        public ActionResult Edit(AGDepartmentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = AGDepartmentDataService.GetLite(db).Where(i => i.AGDepartmentId == model.AGDepartmentId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            InitializeData(dbData, model);

            db.SaveChanges();
            SucessMessage = $"AG-Department {dbData.Name} update sucessfully";

            return RedirectToAction("Index");
        }
        private void InitializeData(AGDepartment tempAGDepartment, AGDepartmentMvcModel model)
        {
            tempAGDepartment.Name = model.Name;
            tempAGDepartment.DepartmentHeadName = model.DepartmentHeadName;
            tempAGDepartment.Status = model.Status;
            tempAGDepartment.IsDefault = model.IsDefault;
            tempAGDepartment.ModifyBy = AppUser.UserName;
        }
        private bool CommonForInsertUpdate(AGDepartmentMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = AGDepartmentDataService.GetLite(db).FirstOrDefault(i => i.AGDepartmentId != model.AGDepartmentId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "AG-Department name already exists");
                return false;
            }
            return true;
        }
    }
}
