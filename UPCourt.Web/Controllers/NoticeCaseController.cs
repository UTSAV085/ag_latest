﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeCaseController : BaseMVCController
    {
        // GET: NoticeCase
        public ActionResult Index()
        {
            if (!IsValidPermission(rModel, PageName.NoticeToCase, PagePermissionType.CanView))
            {
                return RedirectToAction("Index", "Login");
            }
            NoticeCaseIndexMvcModel model = new NoticeCaseIndexMvcModel()
            {
                NoticeNo = string.Empty,
                NoticeFromDate = DateTime.Now.AddMonths(-1),
                NoticeToDate = DateTime.Now
            };

            var dbData = NoticeDataService.GetDetail(db)
                .Where(F => F.Status == Entity.NoticeStatus.Pending
                && F.NoticeDate >= model.NoticeFromDate
                && F.NoticeDate <= model.NoticeToDate)
                .OrderByDescending(i => i.NoticeDate)
                .ThenByDescending(F => F.NoticeNo).ToList();

            model.CaseStatuses = GetAllStatus();

            //model.CaseStatuses = AllStatuses
            //    .Select(i => new SelectListItem { Text = i.Name, Value = i.CaseStatusId.ToString(), Selected = i == AllStatuses.First() }).ToList();

            model.Notices = NoticeMvcModel.CopyFromEntityList(dbData, 0);
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(NoticeCaseIndexMvcModel model)
        {
            var dbData = NoticeDataService.GetDetail(db)
                .Where(F => F.Status == Entity.NoticeStatus.Pending
                && (model.NoticeNo == null || F.NoticeNo.Contains(model.NoticeNo))
                && (!model.NoticeFromDate.HasValue || F.NoticeDate.Value.Date >= model.NoticeFromDate.Value.Date)
                && (!model.NoticeToDate.HasValue || F.NoticeDate.Value.Date <= model.NoticeToDate.Value.Date)
                )
                .OrderByDescending(i => i.NoticeDate)
                .ThenByDescending(F => F.NoticeNo).ToList();
            model.CaseStatuses = GetAllStatus();
            model.Notices = NoticeMvcModel.CopyFromEntityList(dbData, 0);
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateCase(NoticeCaseIndexMvcModel model)
        {
            var Notice = NoticeDataService.GetDetail(db).FirstOrDefault(F => F.NoticeId == model.NoticeId);
            if (Notice.IsNull()) return RedirectToAction("Index");
            var TotalCases = CaseDataService.GetLite(db).ToList().Count;
            Case NewCase = new Case()
            {
                CaseId = Guid.NewGuid(),
                NoticeId = model.NoticeId,
                OrdinalNo = TotalCases + 1,
                PetitionNo = model.CaseNo.Trim(),
                CaseStatusId = model.CaseStatusId,
                Type = Notice.NoticeType,
                PetitionerId = Notice.PetitionerId,
                NoticeCounselorId = Notice.NoticeCounselorId,
                ModifyBy = AppUser.UserName
            };
            //Party and Respondents
            Notice.Parties.ToList().ForEach(F => F.CasePartyId = NewCase.CaseId);
            Notice.Respondents.ToList().ForEach(F => F.CaseRespondentId = NewCase.CaseId);
            Notice.BasicIndexs.ToList().ForEach(F => F.CaseId = NewCase.CaseId);

            Notice.Status = NoticeStatus.Accept;

            db.Cases.Add(NewCase);
            db.SaveChanges();
            return RedirectToAction("Create", new { id = NewCase.CaseId });
        }

        // GET: NoticeCase/Create
        public ActionResult Create(Guid id)
        {
            var model = new NewCaseHearingMvcModel();
            var modelCaseHearing = new CaseHearingMvcModel();
            var Case = CaseDataService.GetDetail(db).FirstOrDefault(F => F.CaseId == id);
            modelCaseHearing.Case = CaseMvcModel.CopyFromEntity(Case, 0);
            modelCaseHearing.CaseId = id;
            modelCaseHearing.Documents = new List<DocumentMvcModel>();
            model.Hearing = modelCaseHearing;
            return View(model);
        }

        // POST: NoticeCase/Create
        [HttpPost]
        public ActionResult Create(NewCaseHearingMvcModel model)
        {
            try
            {
                // TODO: Add insert logic here
                var uploadDocs = FileUploadCommon(rModel, Enumerable.Empty<Document>(), Enumerable.Empty<DocumentMvcModel>(), 1, 10, DocumentType.Any);
                if (!rModel.IsSuccess)
                {
                    DisplayMessage(MessageType.Error, rModel.Message, "File upload issue", "Failure");
                    return RedirectToAction("Create", new { id = model.Hearing.Case.CaseId });
                }
                Case dbCase = CaseDataService.GetDetail(db).FirstOrDefault(F => F.CaseId == model.Hearing.Case.CaseId);
                CaseHearing Hearing = dbCase.CaseHearings.FirstOrDefault();
                if (Hearing.IsNull())
                {
                    Hearing = new CaseHearing()
                    {
                        CaseHearingId = Guid.NewGuid(),
                        HearingDate = model.Hearing.HearingDate
                    };
                }
                Hearing.CaseId = model.Hearing.Case.CaseId;
                Hearing.NextHearingDate = model.Hearing.NextHearingDate;
                Hearing.ModifyBy = AppUser.UserName;
                Hearing.Documents = Hearing.Documents ?? new List<Document>();
                //Name = postedFile.FileName,
                //DocumentUrl = newFileInfo.Item3,
                //Size = postedFile.ContentLength,
                //DocumentId = Guid.NewGuid()
                foreach (var doc in uploadDocs)
                {
                    doc.CaseHearingId = Hearing.CaseHearingId;
                    doc.Name = model.Document.Name;
                    doc.Type = DocumentType.Any;
                    doc.EntityType = DocumentEntityType.CaseHearing;
                    //doc.Ordinal = 
                }

                return RedirectToAction("Create", new { id = model.Hearing.Case.CaseId });
            }
            catch
            {
                return View();
            }
        }

        List<SelectListItem> GetAllStatus()
        {
            var AllStatuses = CaseStatusDataService.GetLite(db).ToList();
            var CaseStatuses = new List<SelectListItem>();
            for (int i = 0; i < AllStatuses.Count; i++)
            {
                CaseStatuses.Add(new SelectListItem()
                {
                    Text = AllStatuses[i].Name,
                    Value = AllStatuses[i].CaseStatusId.ToString(),
                    Selected = AllStatuses[0].CaseStatusId == AllStatuses[i].CaseStatusId
                });
            }
            return CaseStatuses;
        }
    }
}
