﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CourtController : BaseMVCController
    {
        // GET: Court
        public ActionResult Index()
        {
            //var dbData = from Court in db.Courts
            //             join CourtOpenCloseLog in db.CourtOpenCloseLogs
            //             on Court.CourtId equals CourtOpenCloseLog.CourtId
            //             select new { Court = Court };
            //return View(CourtMvcModel.CopyFromEntityList((IEnumerable<Court>)dbData, 0));
            var dbData = CourtDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            return View(CourtMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CourtMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Court/Create
        public ActionResult Create()
        {
            var model = new CourtMvcModel();
            return View(model);
        }

        // POST: Court/Create
        [HttpPost]
        public ActionResult Create(CourtMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}
            Court tempCourt = new Court();
            tempCourt.CourtId = Guid.NewGuid();
            InitializeData(tempCourt, model, true);

            db.Courts.Add(tempCourt);
            db.SaveChanges();
            SucessMessage = $"Court {tempCourt.Name} created sucessfully";

            return RedirectToAction("Index");
        }
        // GET: AGDepartment/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CourtMvcModel.CopyFromEntity(dbData, 0));
        }
        // POST: CaseStatus/Edit/5
        [HttpPost]
        public ActionResult Edit(CourtMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = CourtDataService.GetDetail(db).Where(i => i.CourtId == model.CourtId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            InitializeData(dbData, model, false);

            db.SaveChanges();
            SucessMessage = $"Court {dbData.Name} update sucessfully";

            return RedirectToAction("Index");
        }
        private void InitializeData(Court tempCourt, CourtMvcModel model, bool IsNew)
        {
            if (IsNew && model.IsClose)
            {
                CourtOpenCloseLog tempCourtOpenCloseLog = new CourtOpenCloseLog();
                tempCourtOpenCloseLog.CourtOpenCloseLogId = Guid.NewGuid();
                tempCourtOpenCloseLog.CourtId = tempCourt.CourtId;
                tempCourtOpenCloseLog.CloseFromDate = model.CloseFromDate;
                tempCourtOpenCloseLog.CloseToDate = model.CloseToDate;
                tempCourtOpenCloseLog.ModifyBy = AppUser.UserName;
                tempCourt.CourtOpenCloseLogs.Add(tempCourtOpenCloseLog);
            }
            if (!IsNew)
            {
                //Update
                if (tempCourt.IsClose == true && model.IsClose == true)
                {
                    var tempLastClosingDate = tempCourt.CourtOpenCloseLogs.OrderBy(i => i.CloseToDate).FirstOrDefault();
                    tempLastClosingDate.CloseFromDate = model.CloseFromDate;
                    tempLastClosingDate.CloseToDate = model.CloseToDate;
                }
                //Create
                if (tempCourt.IsClose == false && model.IsClose == true)
                {
                    CourtOpenCloseLog tempCourtOpenCloseLog = new CourtOpenCloseLog();
                    tempCourtOpenCloseLog.CourtOpenCloseLogId = Guid.NewGuid();
                    tempCourtOpenCloseLog.CourtId = tempCourt.CourtId;
                    tempCourtOpenCloseLog.CloseFromDate = model.CloseFromDate;
                    tempCourtOpenCloseLog.CloseToDate = model.CloseToDate;
                    tempCourtOpenCloseLog.ModifyBy = AppUser.UserName;
                    tempCourt.CourtOpenCloseLogs.Add(tempCourtOpenCloseLog);
                }
            }
            tempCourt.Name = model.Name;
            tempCourt.Number = model.Number;
            tempCourt.Status = model.Status;
            tempCourt.CourtBench = model.CourtBench;
            tempCourt.IsClose = model.IsClose;
            tempCourt.ModifyBy = AppUser.UserName;
        }
        private bool CommonForInsertUpdate(CourtMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = CourtDataService.GetLite(db).FirstOrDefault(i => i.CourtId != model.CourtId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Court name already exists");
                return false;
            }
            return true;
        }
    }
}
