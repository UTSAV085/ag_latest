﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class RegionController : BaseMVCController
    {
        // GET: Region
        public ActionResult Index()
        {
            var dbData = RegionDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            return View(RegionMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: Region/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(RegionMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: Region/Create
        public ActionResult Create()
        {
            var model = new RegionMvcModel();
            return View(model);
        }

        // POST: Region/Create
        [HttpPost]
        public ActionResult Create(RegionMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}
            Region tempRegion = new Region();
            tempRegion.RegionId = Guid.NewGuid();
            InitializeData(tempRegion, model);

            db.Regions.Add(tempRegion);
            db.SaveChanges();
            SucessMessage = $"Region {tempRegion.Name} created sucessfully";

            return RedirectToAction("Index");
        }

        // GET: Region/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(RegionMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: Region/Edit/5
        [HttpPost]
        public ActionResult Edit(RegionMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = RegionDataService.GetLite(db).Where(i => i.RegionId == model.RegionId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            InitializeData(dbData, model);

            db.SaveChanges();
            SucessMessage = $"Region {dbData.Name} update sucessfully";

            return RedirectToAction("Index");
        }
        private void InitializeData(Region tempRegion, RegionMvcModel model)
        {
            tempRegion.Name = model.Name;
            tempRegion.Description = model.Description;
            tempRegion.Status = model.Status;
            tempRegion.IsDefault = model.IsDefault;
            tempRegion.ModifyBy = AppUser.UserName;
        }
        private bool CommonForInsertUpdate(RegionMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = RegionDataService.GetLite(db).FirstOrDefault(i => i.RegionId != model.RegionId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Region name already exists");
                return false;
            }
            return true;
        }
    }
}
