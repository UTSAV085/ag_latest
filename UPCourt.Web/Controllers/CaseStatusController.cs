﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CaseStatusController : BaseMVCController
    {
        // GET: CaseStatus
        public ActionResult Index()
        {
            var dbData = CaseStatusDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            return View(CaseStatusMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: CaseStatus/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CaseStatusMvcModel.CopyFromEntity(dbData, 0));
        }

        // GET: CaseStatus/Create
        public ActionResult Create()
        {
            var model = new CaseStatusMvcModel();
            return View(model);
        }

        // POST: CaseStatus/Create
        [HttpPost]
        public ActionResult Create(CaseStatusMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}
            CaseStatus tempCaseStatus = new CaseStatus();
            tempCaseStatus.CaseStatusId = Guid.NewGuid();
            InitializeData(tempCaseStatus, model);

            db.CaseStatuses.Add(tempCaseStatus);
            db.SaveChanges();
            SucessMessage = $"Case Status {tempCaseStatus.Name} created sucessfully";

            return RedirectToAction("Index");
        }

        // GET: CaseStatus/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(CaseStatusMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: CaseStatus/Edit/5
        [HttpPost]
        public ActionResult Edit(CaseStatusMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = CaseStatusDataService.GetLite(db).Where(i => i.CaseStatusId == model.CaseStatusId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            InitializeData(dbData, model);

            db.SaveChanges();
            SucessMessage = $"Case Status {dbData.Name} update sucessfully";

            return RedirectToAction("Index");
        }

        private void InitializeData(CaseStatus tempCaseStatus, CaseStatusMvcModel model)
        {
            tempCaseStatus.Name = model.Name;
            tempCaseStatus.HexColorCode = model.HexColorCode;
            tempCaseStatus.Type = CaseStatusType.Normal;
            tempCaseStatus.ModifyBy = AppUser.UserName;
        }
        private bool CommonForInsertUpdate(CaseStatusMvcModel model)
        {
            var nameForCheck = model.Name.TrimX();
            var duplicateCheck = CaseStatusDataService.GetLite(db).FirstOrDefault(i => i.CaseStatusId != model.CaseStatusId && i.Name == nameForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), "Case Status name already exists");
                return false;
            }
            return true;
        }
    }
}
