﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class UserController : BaseMVCController
    {
        // GET: User
        public ActionResult Index()
        {
            var dbData = UserDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            return View(UserMvcModel.CopyFromEntityList(dbData, 0));
        }
        // GET: User/Create
        public ActionResult Create()
        {
            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(null);
            var model = new UserMvcModel();
            //InitilizeUserType(model, null);
            return View(model);
        }
        // POST: Country1/Create
        [HttpPost]
        public ActionResult Create(UserMvcModel model)
        {
            InitilizeUserRoles(model.UserRoleId);
            InitilizeAGDepartments(model.AGDepartmentId);
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            if (model.UserType == UserType.AGDepartment)
            {
                if (model.AGDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is required");
                    return View(model);
                }
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            if (model.UserType == UserType.GovernmentDepartment)
            {
                if (model.GovernmentDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is required");
                    return View(model);
                }
                if (model.AGDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            if (model.UserType != UserType.GovernmentDepartment || model.UserType != UserType.AGDepartment)
            {
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
                if (model.AGDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            User tempData = new User()
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Address = model.Address,
                Phone = model.Phone,
                LoginId = model.Email,
                Status = model.Status,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = model.IsDefault,
                UserType = model.UserType,
                AGDepartmentId = model.AGDepartmentId,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                UserRoleId = model.UserRoleId,
                ModifyBy = AppUser.UserName
            };

            db.Users.Add(tempData);
            db.SaveChanges();
            SucessMessage = $"User {tempData.Name} created sucessfully";

            return RedirectToAction("Index");
        }
        public void InitilizeUserRoles(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRoleMvcModel.Name), selectedValue);
        }
        public void InitilizeAGDepartments(object selectedValue)
        {
            var dbData = AGDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = AGDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AGDepartments = new SelectList(mvc, nameof(AGDepartmentMvcModel.AGDepartmentId)
                , nameof(AGDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeUserType(UserMvcModel model, int? Id)
        {
            model.UserTypeList = Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(F => new SelectListItem()
            {
                Text = F.GetDescription(),
                Value = F.ToInt().ToString(),
                Selected = Id.HasValue ? Id.Value == F.ToInt() : false
            }).ToList();
        }
        // GET: Country1/Edit/5
        public ActionResult Edit(Guid id)
        {
            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeUserRoles(dbData.UserRoleId);
            InitilizeAGDepartments(dbData.AGDepartmentId);
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            return View(UserMvcModel.CopyFromEntity(dbData, 0));
        }

        // POST: Country1/Edit/5
        [HttpPost]
        public ActionResult Edit(UserMvcModel model)
        {
            InitilizeUserRoles(model.UserRoleId);
            InitilizeAGDepartments(model.AGDepartmentId);
            InitilizeGovermentDepartments(model.GovernmentDepartmentId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == model.UserId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }
            if (model.UserType == UserType.AGDepartment)
            {
                if (model.AGDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is required");
                    return View(model);
                }
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            if (model.UserType == UserType.GovernmentDepartment)
            {
                if (model.GovernmentDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is required");
                    return View(model);
                }
                if (model.AGDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            if (model.UserType != UserType.GovernmentDepartment || model.UserType != UserType.AGDepartment)
            {
                if (model.GovernmentDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
                if (model.AGDepartmentId != null)
                {
                    ModelState.AddModelError(nameof(model.AGDepartmentId), "AG-Department is not needed because User Type '" + model.UserType + "' has been chosen.");
                    return View(model);
                }
            }
            dbData.Name = model.Name;
            dbData.Email = model.Email;
            dbData.Address = model.Address;
            dbData.Phone = model.Phone;
            dbData.LoginId = model.Email;
            dbData.Status = model.Status;
            //If password changed
            if (!model.Password.IsNullOrEmpty())
            {
                var passwordSalt = PasswordHelper.GetPasswordSalt();
                var passwordHash = PasswordHelper.EncodePassword(model.Password, passwordSalt);

                dbData.PasswordSalt = passwordSalt;
                dbData.PasswordHash = passwordHash;
            }
            dbData.IsDefault = model.IsDefault;
            dbData.UserType = model.UserType;
            dbData.UserRoleId = model.UserRoleId;
            dbData.AGDepartmentId = model.AGDepartmentId;
            dbData.GovernmentDepartmentId = model.GovernmentDepartmentId;
            dbData.ModifyBy = AppUser.UserName;
            db.SaveChanges();
            SucessMessage = "User Role update sucessfully";
            return RedirectToAction("Index");
        }
        private bool CommonForInsertUpdate(UserMvcModel model)
        {
            var emailForCheck = model.Email.TrimX();
            var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.UserId != model.UserId && i.Email == emailForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), "Email already exists");
                return false;
            }
            return true;
        }
        // GET: User/Details/5
        public ActionResult Details(Guid id)
        {
            var dbData = UserDataService.GetDetail(db).Where(i => i.UserId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeUserRoles(dbData.UserRoleId);
            InitilizeAGDepartments(dbData.AGDepartmentId);
            InitilizeGovermentDepartments(dbData.GovernmentDepartmentId);
            return View(UserMvcModel.CopyFromEntity(dbData, 0));
        }
    }
}
