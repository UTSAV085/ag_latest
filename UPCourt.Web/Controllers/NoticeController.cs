﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class NoticeController : BaseMVCController
    {
        [AllowAnonymous]
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "addbasicindex")]
        public ActionResult addbasicindex(NoticeMvcModel model)
        {
            //var tempAssesseeITRTypeAssessment = AssesseeITRTypeAssessmentDataService.GetLite(db).FirstOrDefault(i => i.AssesseeId == AppUser.UserId);
            //if (tempAssesseeITRTypeAssessment.IsNull())
            //{
            //    tempAssesseeITRTypeAssessment = new AssesseeITRTypeAssessment();
            //    tempAssesseeITRTypeAssessment.AssesseeITRTypeAssessmentId = Guid.NewGuid();
            //    tempAssesseeITRTypeAssessment.AssesseeId = AppUser.UserId;

            //    db.AssesseeITRTypeAssessments.Add(tempAssesseeITRTypeAssessment);
            //}
            //tempAssesseeITRTypeAssessment.IsIncomeFromSalaryOrPension = true;
            //db.SaveChanges();
            return View(model);
        }

        // GET: Notice
        public ActionResult Index()
        {
            var dbData = NoticeDataService.GetLite(db).OrderBy(i => i.NoticeDate).ToList();
            return View(NoticeMvcModel.CopyFromEntityList(dbData, 0));
        }

        // GET: Notice/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Notice/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            InitilizeGovermentDepartments(null);
            var model = new NoticeMvcModel();
            model.NoticeParties = new List<NoticePartyMvcModel>();
            model.NoticeRespondent = new List<NoticeRespondentMvcModel>();
            model.NoticeBasicIndex = new List<NoticeBasicIndexMvcModel>();
            model.NoticeRespondent.Add(new NoticeRespondentMvcModel());
            model.NoticeParties.Add(new NoticePartyMvcModel());
            model.NoticeBasicIndex.Add(new NoticeBasicIndexMvcModel());
            return View(model);
        }

        // POST: Notice/Create
        [AllowAnonymous]
        [HttpPost]
        [MultipleButton(Name = "action", Argument = "Create")]
        public ActionResult Create(NoticeMvcModel model)
        {
            InitilizeGovermentDepartments(model.NoticeParties.FirstOrDefault(i => i.GovernmentDepartmentId != null)?.GovernmentDepartmentId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //if (!IsValidPermission(rModel, PageName.UserRole, PagePermissionType.CanCreate))
            //{
            //    return rModel;
            //}

            var LastserialNo = NoticeDataService.GetLite(db).OrderByDescending(i => i.SerialNo).FirstOrDefault();

            //Notice
            Notice tempNotice = new Notice();
            tempNotice.NoticeId = Guid.NewGuid();
            tempNotice.Status = NoticeStatus.Pending;

            tempNotice.ModifyBy = model.Petitioner.Name;

            //Petitioner
            Petitioner tempPetitioner = new Petitioner();
            tempPetitioner.PetitionerId = Guid.NewGuid();
            tempPetitioner.Name = model.Petitioner.Name;
            tempPetitioner.Address = model.Petitioner.Address;
            tempPetitioner.ContactNo = model.Petitioner.ContactNo;
            tempPetitioner.Email = model.Petitioner.Email;
            tempPetitioner.District = model.Petitioner.District;
            tempPetitioner.IDProof = model.Petitioner.IDProof;
            tempPetitioner.IDProofNumber = model.Petitioner.IDProofNumber;
            tempPetitioner.ModifyBy = model.Petitioner.Name;

            //Counselor
            NoticeCounselor tempNoticeCounselor = new NoticeCounselor();
            tempNoticeCounselor.NoticeCounselorId = Guid.NewGuid();
            tempNoticeCounselor.Name = model.NoticeCounselor.Name;
            tempNoticeCounselor.Email = model.NoticeCounselor.Email;
            tempNoticeCounselor.ContactNo = model.NoticeCounselor.ContactNo;
            tempNoticeCounselor.RollNo = model.NoticeCounselor.RollNo;
            tempNoticeCounselor.EnrollmentNo = model.NoticeCounselor.EnrollmentNo;
            tempNoticeCounselor.ModifyBy = model.Petitioner.Name;
            //InitializeData(tempUserCredentialRequest, model);

            //Notice
            tempNotice.PetitionerId = tempPetitioner.PetitionerId;
            tempNotice.NoticeCounselorId = tempNoticeCounselor.NoticeCounselorId;
            tempNotice.NoticeType = model.NoticeType;
            tempNotice.SerialNo = NoticeBusinessService.GetSerailNo(db);
            tempNotice.NoticeNo = NoticeBusinessService.GetInvoiceNo("NOC", tempNotice.SerialNo);
            tempNotice.NoticeDate = Extended.CurrentIndianTime;
            tempNotice.Subject = model.Subject;
            tempNotice.IsContextOrder = model.IsContextOrder;

            if (!model.NoticeParties.IsEmptyCollection())
            {
                foreach (var item in model.NoticeParties)
                {
                    InitilizeGovermentDepartments(item.GovernmentDepartmentId);
                    PartyAndRespondent tempPartyAndRespondent = new PartyAndRespondent();
                    if (!item.IsPartyDepartment)
                    {
                        tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                        tempPartyAndRespondent.NoticePartyId = tempNotice.NoticeId;
                        tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                        tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Party;
                        tempPartyAndRespondent.PartyType = PartyType.Individual;
                        tempPartyAndRespondent.PartyName = item.PartyName;
                        tempPartyAndRespondent.ContactNo = item.ContactNo;
                        tempPartyAndRespondent.Email = item.Email;
                        tempPartyAndRespondent.ModifyBy = model.Petitioner.Name;
                    }
                    if (item.IsPartyDepartment)
                    {
                        tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                        tempPartyAndRespondent.NoticePartyId = tempNotice.NoticeId;
                        tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                        tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Party;
                        tempPartyAndRespondent.PartyType = PartyType.Department;
                        tempPartyAndRespondent.GovernmentDepartmentId = item.GovernmentDepartmentId;
                        tempPartyAndRespondent.ModifyBy = model.Petitioner.Name;
                    }
                    tempNotice.Parties.Add(tempPartyAndRespondent);
                }
            }
            if (!model.NoticeRespondent.IsEmptyCollection())
            {
                foreach (var item in model.NoticeRespondent)
                {
                    InitilizeGovermentDepartments(item.GovernmentDepartmentId);
                    PartyAndRespondent tempPartyAndRespondent = new PartyAndRespondent();
                    if (!item.IsPartyDepartment)
                    {
                        tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                        tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                        tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                        tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                        tempPartyAndRespondent.PartyType = PartyType.Individual;
                        tempPartyAndRespondent.PartyName = item.PartyName;
                        tempPartyAndRespondent.ContactNo = item.ContactNo;
                        tempPartyAndRespondent.Email = item.Email;
                        tempPartyAndRespondent.ModifyBy = model.Petitioner.Name;
                    }
                    if (item.IsPartyDepartment)
                    {
                        tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                        tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                        tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                        tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                        tempPartyAndRespondent.PartyType = PartyType.Department;
                        tempPartyAndRespondent.GovernmentDepartmentId = item.GovernmentDepartmentId;
                        tempPartyAndRespondent.ModifyBy = model.Petitioner.Name;
                    }
                    tempNotice.Respondents.Add(tempPartyAndRespondent);
                }
            }

            db.Notices.Add(tempNotice);
            db.Petitioners.Add(tempPetitioner);
            db.NoticeCounselors.Add(tempNoticeCounselor);
            db.SaveChanges();
            SucessMessage = $"Notice '{tempNotice.SerialNo}' generated sucessfully";

            return View(model);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        // GET: Notice/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Notice/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Notice/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Notice/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Add()
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, "Name", "Name");
            ViewBag.GovermentDepartments2 = mvc;
            return View();
        }

        public JsonResult GovtDept()
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);

            return Json(mvc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddNotice(string petitioner, string counselor, string notice, string respondent)
        {
            string petitionerName = "";
            Guid petitionerId = new Guid();
            string noticeNo = "";

            Notice tempNotice = new Notice();

            var serializeData = JsonConvert.DeserializeObject<List<Petitioner>>(petitioner);
            var serializeCounselorData = JsonConvert.DeserializeObject<List<NoticeCounselor>>(counselor);
            var serializeNoticeData = JsonConvert.DeserializeObject<List<Notice>>(notice);
            var serializeRespondentData = JsonConvert.DeserializeObject<List<NoticeRespondentMvcModel>>(respondent);

            foreach (var data in serializeData)
            {
                Petitioner tempPetitioner = new Petitioner();
                tempPetitioner.PetitionerId = Guid.NewGuid();
                tempPetitioner.Name = data.Name;
                tempPetitioner.Address = data.Address;
                tempPetitioner.ContactNo = data.ContactNo;
                tempPetitioner.Email = data.Email;
                tempPetitioner.District = data.District;
                tempPetitioner.IDProof = data.IDProof;
                tempPetitioner.IDProofNumber = data.IDProofNumber;
                tempPetitioner.ModifyBy = data.Name;

                petitionerName = data.Name;
                petitionerId = tempPetitioner.PetitionerId;
                //db.Petitioners.Add(tempPetitioner);
                //db.SaveChanges();
            }

            foreach (var data in serializeCounselorData)
            {
                NoticeCounselor tempCounselor = new NoticeCounselor();
                tempCounselor.NoticeCounselorId = Guid.NewGuid();
                tempCounselor.Name = data.Name;
                tempCounselor.Email = data.Email;
                tempCounselor.ContactNo = data.ContactNo;
                tempCounselor.EnrollmentNo = data.EnrollmentNo;
                tempCounselor.RollNo = data.RollNo;
                tempCounselor.ModifyBy = petitionerName;

                //db.NoticeCounselors.Add(tempCounselor);
                //db.SaveChanges();
            }

            foreach(var data in serializeNoticeData)
            {
                
                tempNotice.PetitionerId = petitionerId;
                tempNotice.NoticeCounselorId = data.NoticeCounselorId;
                tempNotice.NoticeType = data.NoticeType;
                tempNotice.SerialNo = NoticeBusinessService.GetSerailNo(db);
                tempNotice.NoticeNo = NoticeBusinessService.GetInvoiceNo("NOC", tempNotice.SerialNo);
                tempNotice.NoticeDate = Extended.CurrentIndianTime;
                tempNotice.Subject = data.Subject;
                tempNotice.IsContextOrder = data.IsContextOrder;

                noticeNo = tempNotice.NoticeNo;
            }

            tempNotice.NoticeId = Guid.NewGuid();

            foreach (var data in serializeRespondentData)
            {
                PartyAndRespondent tempPartyAndRespondent = new PartyAndRespondent();
                if (!data.IsPartyDepartment)
                {
                    tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                    tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    tempPartyAndRespondent.PartyType = PartyType.Individual;
                    tempPartyAndRespondent.PartyName = data.PartyName;
                    tempPartyAndRespondent.ContactNo = data.ContactNo;
                    tempPartyAndRespondent.Email = data.Email;
                    tempPartyAndRespondent.ModifyBy = petitionerName;
                }
                if (data.IsPartyDepartment)
                {
                    tempPartyAndRespondent.PartyAndRespondentId = Guid.NewGuid();
                    tempPartyAndRespondent.NoticeRespondentId = tempNotice.NoticeId;
                    tempPartyAndRespondent.NoticeOrCaseType = NoticeOrCaseType.Notice;
                    tempPartyAndRespondent.PartyAndRespondentType = PartyAndRespondentType.Respondent;
                    tempPartyAndRespondent.PartyType = PartyType.Department;
                    tempPartyAndRespondent.GovernmentDepartmentId = data.GovernmentDepartmentId;
                    tempPartyAndRespondent.ModifyBy = petitionerName;
                }
                //tempNotice.Respondents.Add(tempPartyAndRespondent);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }


    }
}
