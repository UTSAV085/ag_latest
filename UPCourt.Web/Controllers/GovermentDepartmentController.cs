﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class GovermentDepartmentController : BaseMVCController
    {
        // GET: Department
        public ActionResult Index()
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            /* GovernmentDepartmentAddressMvcModel GovernmentDepartmentAddressList = new GovernmentDepartmentAddressMvcModel();
             GovermentDepartmentMvcModel govermentDepartmentMvcModel = new GovermentDepartmentMvcModel();

              GovernmentDepartmentContactNoMvcModel GovernmentDepartmentContactNoList = new GovernmentDepartmentContactNoMvcModel();*/
            return View(GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0));
        }
        // GET: Department/Create
        public ActionResult Create()
        {

            InitilizeGovermentDepartment(null);

            var model = new GovermentDepartmentMvcModel();
            model.GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddressMvcModel>();
            model.GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNoMvcModel>();
            model.GovernmentDepartmentContactNos.Add(new GovernmentDepartmentContactNoMvcModel());
            model.GovernmentDepartmentAddresses.Add(new GovernmentDepartmentAddressMvcModel());
            // model.GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNoMvcModel>();
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(GovermentDepartmentMvcModel model)
        {
            // model.GovernmentDepartmentAddressList.Add(new GovernmentDepartmentAddressMvcModel());
            InitilizeGovermentDepartment(model.GovernmentDepartmentId);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }

            GovernmentDepartment tempData = new GovernmentDepartment();
            tempData.GovernmentDepartmentId = Guid.NewGuid();
            tempData.Name = model.Name;
            tempData.Email = model.Email;
            tempData.Status = model.Status;
            tempData.IsDefault = model.IsDefault;
            tempData.POCName = model.POCName;
            tempData.POCContact = model.POCContact;
            tempData.ModifyBy = AppUser.UserName;
            tempData.HeadOffice = model.HeadOffice;
            tempData.GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddress>();
            tempData.GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNo>();
            foreach (var item in model.GovernmentDepartmentContactNos)
            {
                if (!item.DeletePhone)
                {
                    GovernmentDepartmentContactNo governmentDepartmentContactNo = new GovernmentDepartmentContactNo();
                    governmentDepartmentContactNo.GovernmentDepartmentContactNoId = Guid.NewGuid();
                    governmentDepartmentContactNo.GovernmentDepartmentId = tempData.GovernmentDepartmentId;
                    governmentDepartmentContactNo.ContactNo = item.ContactNo;
                    governmentDepartmentContactNo.ModifyBy = AppUser.UserName;
                    db.GovernmentDepartmentContactNos.Add(governmentDepartmentContactNo);
                }
            }
            foreach (var item in model.GovernmentDepartmentAddresses)
            {
                if (!item.DeleteAddress)
                {
                    GovernmentDepartmentAddress tempGovernmentDepartmentAddress = new GovernmentDepartmentAddress();
                    tempGovernmentDepartmentAddress.GovernmentDepartmentAddressId = Guid.NewGuid();
                    tempGovernmentDepartmentAddress.GovernmentDepartmentId = tempData.GovernmentDepartmentId;
                    tempGovernmentDepartmentAddress.Address = item.Address;
                    tempGovernmentDepartmentAddress.IsHeadOfficeAddress = item.IsHeadOfficeAddress;
                    tempGovernmentDepartmentAddress.ModifyBy = AppUser.UserName;
                    db.GovernmentDepartmentAddresses.Add(tempGovernmentDepartmentAddress);
                }
            }
            db.GovernmentDepartments.Add(tempData);
            db.SaveChanges();
            SucessMessage = $"Govt. Department {tempData.Name} Created sucessfully";

            return RedirectToAction("Index");
        }
        public ActionResult Edit(Guid id)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            InitilizeGovermentDepartment(dbData.GovernmentDepartmentId);
            return View(GovermentDepartmentMvcModel.CopyFromEntity(dbData, 0));

        }

        [HttpPost]
        public ActionResult Edit(GovermentDepartmentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var dbAdresses = GovernmentDepartmentAddressDataService.GetLite(db).ToList();
            var dbContacts = GovernmentDepartmentContactNoDataService.GetLite(db).ToList();
            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            if (!CommonForInsertUpdate(model))
            {
                return View(model);
            }


            dbData.Name = model.Name;
            dbData.Email = model.Email;
            dbData.Status = model.Status;
            dbData.IsDefault = model.IsDefault;
            dbData.POCName = model.POCName;
            dbData.POCContact = model.POCContact;
            dbData.ModifyBy = AppUser.UserName;
            dbData.HeadOffice = model.HeadOffice;
            // dbData.GovernmentDepartmentAddresses = new List<GovernmentDepartmentAddress>();
            // dbData.GovernmentDepartmentContactNos = new List<GovernmentDepartmentContactNo>();
            foreach (var item in model.GovernmentDepartmentContactNos)
            {
                var tempContact = dbContacts.FirstOrDefault(i => i.GovernmentDepartmentContactNoId == item.GovernmentDepartmentContactNoId);
                if (!item.DeletePhone)
                {
                    if (tempContact == null)
                    {
                        GovernmentDepartmentContactNo governmentDepartmentContactNo = new GovernmentDepartmentContactNo();
                        governmentDepartmentContactNo.GovernmentDepartmentContactNoId = Guid.NewGuid();
                        governmentDepartmentContactNo.GovernmentDepartmentId = dbData.GovernmentDepartmentId;
                        governmentDepartmentContactNo.ContactNo = item.ContactNo;
                        governmentDepartmentContactNo.ModifyBy = AppUser.UserName;
                        db.GovernmentDepartmentContactNos.Add(governmentDepartmentContactNo);
                    }
                    else
                    {
                        tempContact.ContactNo = item.ContactNo;
                        tempContact.ModifyBy = AppUser.UserName;
                    }

                }
                else
                {

                    if (tempContact != null)
                    {
                        db.GovernmentDepartmentContactNos.Remove(tempContact);
                    }


                }
            }
            foreach (var item in model.GovernmentDepartmentAddresses)
            {
                var tempAddress = dbAdresses.FirstOrDefault(i => i.GovernmentDepartmentAddressId == item.GovernmentDepartmentAddressId);
                if (!item.DeleteAddress)
                {
                    if (tempAddress == null)
                    {
                        GovernmentDepartmentAddress tempGovernmentDepartmentAddress = new GovernmentDepartmentAddress();
                        tempGovernmentDepartmentAddress.GovernmentDepartmentAddressId = Guid.NewGuid();
                        tempGovernmentDepartmentAddress.GovernmentDepartmentId = dbData.GovernmentDepartmentId;
                        tempGovernmentDepartmentAddress.Address = item.Address;
                        tempGovernmentDepartmentAddress.IsHeadOfficeAddress = item.IsHeadOfficeAddress;
                        tempGovernmentDepartmentAddress.ModifyBy = AppUser.UserName;
                        db.GovernmentDepartmentAddresses.Add(tempGovernmentDepartmentAddress);
                    }
                    else
                    {
                        tempAddress.Address = item.Address;
                        tempAddress.IsHeadOfficeAddress = item.IsHeadOfficeAddress;
                        tempAddress.ModifyBy = AppUser.UserName;
                    }
                }
                else
                {

                    if (tempAddress != null)
                    {
                        db.GovernmentDepartmentAddresses.Remove(tempAddress);
                    }
                }
            }

            db.SaveChanges();
            SucessMessage = $"Govt. Department {dbData.Name} Update sucessfully";

            return RedirectToAction("Index");
        }
        public ActionResult Details(Guid id)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == id).FirstOrDefault();
            if (dbData == null)
            {
                return RedirectToAction("Index");
            }
            return View(GovermentDepartmentMvcModel.CopyFromEntity(dbData, 0));
        }
        public void InitilizeGovermentDepartment(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetDetail(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovernmentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private bool CommonForInsertUpdate(GovermentDepartmentMvcModel model)
        {

            var duplicateCheck = GovermentDepartmentDataService.GetLite(db).FirstOrDefault(i => i.GovernmentDepartmentId != model.GovernmentDepartmentId && i.Name == model.Name && i.Email == model.Email);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Name), $"{model.Name} already exists");
                return false;
            }
            return true;
        }

        public void UpdateAssignUser(GovermentDepartmentMvcModel model)
        {
            if (!ModelState.IsValid)
            {
                var dbData = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();

                dbData.Name = model.Name;
                dbData.Email = model.Email;
                dbData.Status = model.Status;
                dbData.IsDefault = model.IsDefault;
                dbData.POCName = model.POCName;
                //dbData.IsAssignUser = model.IsAssignUser;
                dbData.POCContact = model.POCContact;
                dbData.ModifyBy = AppUser.UserName;
                dbData.HeadOffice = model.HeadOffice;
                db.SaveChanges();
            }

        }

        [HttpGet]
        public ActionResult CreateUser(Guid? GDI)
        {
            InitilizeUserRoles(null);
            InitilizeAGDepartments(null);
            InitilizeGovermentDepartments(GDI);
            var model = new UserMvcModel();
            model.UserType = UserType.GovernmentDepartment;
            InitilizeUserType(model, 4);
            return PartialView("CreateUser", model);

        }
        [HttpPost]
        public ActionResult CreateUser(UserMvcModel model)
        {

            if (!ModelState.IsValid)
            {
                return PartialView("CreateUser", model);
            }

            if (!CommonForInsertUpdate(model))
            {
                return PartialView("CreateUser", model);
            }
            if (model.UserType == UserType.GovernmentDepartment)
            {
                if (model.GovernmentDepartmentId == null)
                {
                    ModelState.AddModelError(nameof(model.GovernmentDepartmentId), "Goverment Department is required");
                    return ReBindOnError(model);
                }

            }
            var passWordSalt = PasswordHelper.GetPasswordSalt();
            var passwordHash = PasswordHelper.EncodePassword(model.Password, passWordSalt);
            User tempData = new User()
            {
                UserId = Guid.NewGuid(),
                Name = model.Name,
                Email = model.Email,
                Address = model.Address,
                Phone = model.Phone,
                LoginId = model.Email,
                Status = model.Status,
                PasswordSalt = passWordSalt,
                PasswordHash = passwordHash,
                IsDefault = model.IsDefault,
                UserType = model.UserType,
                AGDepartmentId = model.AGDepartmentId,
                GovernmentDepartmentId = model.GovernmentDepartmentId,
                UserRoleId = model.UserRoleId,
                ModifyBy = AppUser.UserName
            };

            db.Users.Add(tempData);
            db.SaveChanges();
            var GDMModel = GovermentDepartmentDataService.GetDetail(db).Where(i => i.GovernmentDepartmentId == model.GovernmentDepartmentId).FirstOrDefault();
            //GDMModel.IsAssignUser = true;
            db.SaveChanges();
            SucessMessage = $"User {tempData.Name} created sucessfully";
            return RedirectToAction("Index");

            ActionResult ReBindOnError(UserMvcModel model1)
            {
                InitilizeUserRoles(model1.UserRoleId);
                InitilizeAGDepartments(model1.AGDepartmentId);
                InitilizeGovermentDepartments(model1.GovernmentDepartmentId);
                model1.UserType = UserType.GovernmentDepartment;
                InitilizeUserType(model1, 4);
                return PartialView("CreateUser", model1);
            }
        }

        public void InitilizeUserRoles(object selectedValue)
        {
            var dbData = UserRoleDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = UserRoleMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.UserRoles = new SelectList(mvc, nameof(UserRoleMvcModel.UserRoleId)
                , nameof(UserRoleMvcModel.Name), selectedValue);
        }
        public void InitilizeAGDepartments(object selectedValue)
        {
            var dbData = AGDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = AGDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AGDepartments = new SelectList(mvc, nameof(AGDepartmentMvcModel.AGDepartmentId)
                , nameof(AGDepartmentMvcModel.Name), selectedValue);
        }
        public void InitilizeGovermentDepartments(object selectedValue)
        {
            var dbData = GovermentDepartmentDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = GovermentDepartmentMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.GovermentDepartments = new SelectList(mvc, nameof(GovermentDepartmentMvcModel.GovernmentDepartmentId)
                , nameof(GovermentDepartmentMvcModel.Name), selectedValue);
        }
        private void InitilizeUserType(UserMvcModel model, int? Id)
        {
            model.UserTypeList = Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(F => new SelectListItem()
            {
                Text = F.GetDescription(),
                Value = F.ToInt().ToString(),
                Selected = Id.HasValue ? Id.Value == F.ToInt() : false
            }).ToList();
        }
        private bool CommonForInsertUpdate(UserMvcModel model)
        {
            var emailForCheck = model.Email.TrimX();
            var duplicateCheck = UserDataService.GetLite(db).FirstOrDefault(i => i.UserId != model.UserId && i.Email == emailForCheck);
            if (duplicateCheck != null)
            {
                ModelState.AddModelError(nameof(model.Email), "Email already exists");
                return false;
            }
            return true;
        }
    }
}
