﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class CaseController : BaseMVCController
    {
        // GET: Case
        public ActionResult Index(CaseFilterMvcModel caseFilterMvcModel, string CurrentTab)
        {
            InitilizeCouncil(null);
            InitilizeCourt(null);

            if (CurrentTab == null)
                CurrentTab = "AcctiveCases";

            ViewBag.CurrentTab = CurrentTab;
            List<Case> dbData = new List<Case>();
            List<Case> acctiveCases = new List<Case>();
            List<Case> disposedCases = new List<Case>();
            List<Case> caseTransfer = new List<Case>();
            dbData = CaseDataService.GetDetail(db).OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AppUser.UserType == UserType.CSC)
                dbData = dbData.Where(x => x.Notice.NoticeType.ToString() == "1").OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AppUser.UserType == UserType.Advocate)
                dbData = dbData.Where(x => x.Notice.NoticeType.ToString() == "0" && x.AdvocateId == AppUser.UserId).OrderBy(i => i.Notice.NoticeDate).ToList();

            acctiveCases = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Normal).OrderBy(i => i.Notice.NoticeDate).ToList();

            caseTransfer = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Normal).OrderBy(i => i.Notice.NoticeDate).ToList();

            disposedCases = dbData.Where(x => x.CaseStatus.Type == CaseStatusType.Dispose).OrderBy(i => i.Notice.NoticeDate).ToList();

            if (CurrentTab == "AcctiveCases")
            {
                if (caseFilterMvcModel.CaseStatusActive != null)
                    InitilizeCaseStatus(caseFilterMvcModel.CaseStatusActive);
                else
                    InitilizeCaseStatus(null);

                if (caseFilterMvcModel.CaseTypeActive != null)
                    InitilizeCaseType(caseFilterMvcModel.CaseTypeActive);
                else
                    InitilizeCaseType(null);

                if (caseFilterMvcModel.CaseStatusActive != null && caseFilterMvcModel.CaseStatusActive.ToString() != "")
                    acctiveCases = acctiveCases.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusActive).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseTypeActive != null && caseFilterMvcModel.CaseTypeActive != "99" && caseFilterMvcModel.CaseTypeActive != "")
                    acctiveCases = acctiveCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.CaseTypeActive).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseDateActive != null)
                    acctiveCases = acctiveCases.Where(x => x.Notice.NoticeDate == caseFilterMvcModel.CaseDateActive).ToList();

            }
            else if (CurrentTab == "CaseTransfer")
            {

                if (caseFilterMvcModel.CaseStatusTransfer != null)
                    InitilizeCaseStatus(caseFilterMvcModel.CaseStatusTransfer);
                else
                    InitilizeCaseStatus(null);

                if (caseFilterMvcModel.CaseTypeTransfer != null)
                    InitilizeCaseType(caseFilterMvcModel.CaseTypeTransfer);
                else
                    InitilizeCaseType(null);

                if (caseFilterMvcModel.CaseStatusTransfer != null && caseFilterMvcModel.CaseStatusTransfer.ToString() != "")
                    caseTransfer = caseTransfer.Where(x => x.CaseStatusId == caseFilterMvcModel.CaseStatusTransfer).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseTypeTransfer != null && caseFilterMvcModel.CaseTypeTransfer.ToLower() != "all" && caseFilterMvcModel.CaseTypeTransfer != "")
                    caseTransfer = caseTransfer.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.CaseTypeTransfer).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseDateTransfer != null)
                    caseTransfer = caseTransfer.Where(x => x.Notice.NoticeDate == caseFilterMvcModel.CaseDateTransfer).ToList();

            }
            else if (CurrentTab == "DisposedCases")
            {
                var dispst = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Dispose).FirstOrDefault();

                InitilizeCaseStatus(dispst.CaseStatusId);

                if (caseFilterMvcModel.CaseTypeDispose != null)
                    InitilizeCaseType(caseFilterMvcModel.CaseTypeDispose);
                else
                    InitilizeCaseType(null);

                dbData = dbData.Where(x => x.CaseStatusId == dispst.CaseStatusId).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseTypeDispose != null && caseFilterMvcModel.CaseTypeDispose.ToLower() != "all" && caseFilterMvcModel.CaseTypeDispose != "")
                    disposedCases = disposedCases.Where(x => x.Notice.NoticeType.ToString() == caseFilterMvcModel.CaseTypeDispose).OrderBy(i => i.Notice.NoticeDate).ToList();

                if (caseFilterMvcModel.CaseDateDispose != null)
                    disposedCases = disposedCases.Where(x => x.Notice.NoticeDate == caseFilterMvcModel.CaseDateDispose).ToList();
            }
            else
            {
                InitilizeCaseStatus(null);
                InitilizeCaseType(null);
            }







            if (AppUser.UserType == UserType.CSC)
                dbData = dbData.Where(x => x.Notice.NoticeType.ToString() == "1").OrderBy(i => i.Notice.NoticeDate).ToList();

            if (AppUser.UserType == UserType.Advocate)
                dbData = dbData.Where(x => x.Notice.NoticeType.ToString() == "0" && x.AdvocateId == AppUser.UserId).OrderBy(i => i.Notice.NoticeDate).ToList();


            ViewBag.LoginUserType = AppUser.UserType;

            caseFilterMvcModel.AcctiveCases = CaseMvcModel.CopyFromEntityList(acctiveCases, 2);
            caseFilterMvcModel.DisposedCases = CaseMvcModel.CopyFromEntityList(disposedCases, 2);
            caseFilterMvcModel.CaseTransfer = CaseMvcModel.CopyFromEntityList(caseTransfer, 2);


            return View(caseFilterMvcModel);
        }
        [HttpPost]
        public ActionResult AssignDuty(Guid CaseGuid, Guid Advocate)
        {
            var dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == CaseGuid).FirstOrDefault();

            dbcase.AdvocateId = Advocate;
            db.SaveChanges();
            SucessMessage = $" Advocate assigned to case :- {dbcase.PetitionNo} sucessfully";
            return RedirectToAction("Index", new { CurrentTab = "AcctiveCases" });
        }
        [HttpPost]
        public ActionResult ReOpenCase(Guid ReCaseGuid)
        {
            var dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == ReCaseGuid).FirstOrDefault();
            var dbstatus = CaseStatusDataService.GetLite(db).Where(x => x.Name == "Re-Opened").FirstOrDefault();
            dbcase.CaseStatusId = dbstatus.CaseStatusId;
            db.SaveChanges();
            SucessMessage = $"Case :- {dbcase.PetitionNo} Re-Opened sucessfully";
            return RedirectToAction("Index", new { CurrentTab = "DisposedCases" });
        }

        [HttpPost]
        public ActionResult MoveToOtherCourt(string CaseTransferIds, Guid NewCourt, string Description)
        {
            string[] caseids = CaseTransferIds.Split(',');
            foreach (string cas in caseids)
            {
                if (cas != "" && cas != null)
                {
                    var dbcase = CaseDataService.GetDetail(db).Where(x => x.CaseId == Guid.Parse(cas)).FirstOrDefault();

                    CaseTransfer caseTransfer = new CaseTransfer()
                    {
                        CaseTransferId = Guid.NewGuid(),
                        CaseId = Guid.Parse(cas),
                        CourtId = NewCourt,
                        OldCourtId = dbcase.CourtId,
                        TransferDate = DateTime.Now,
                        Description = Description,
                        ModifyBy = AppUser.UserName
                    };

                    dbcase.CaseTransfers.Add(caseTransfer);
                    dbcase.CourtId = NewCourt;
                    db.SaveChanges();
                }
            }
            SucessMessage = $"Case transfered sucessfully";
            return RedirectToAction("Index", new { CurrentTab = "CaseTransfer" });
        }
        public void InitilizeCouncil(object selectedValue)
        {
            var dbData = AdvocateDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var mvc = AdvocateMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.Council = new SelectList(mvc, "AdvocateId", "Name", selectedValue);
        }

        public void InitilizeCourt(object selectedValue)
        {
            var dbData = CourtDataService.GetLite(db).OrderBy(i => i.Name).ToList();
            var AllCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.AllCourt = new SelectList(AllCourt, "CourtId", "Name", selectedValue);


            dbData = dbData.Where(x => x.IsClose == false && x.Status == CourtStatus.Active).ToList();
            var OpenedCourt = CourtMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.OpenedCourt = new SelectList(OpenedCourt, "CourtId", "Name", selectedValue);
        }
        public void InitilizeCaseStatus(object selectedValue)
        {
            var dbData = CaseStatusDataService.GetLite(db).Where(x => x.Type == CaseStatusType.Normal).OrderBy(i => i.Name).ToList();
            var mvc = CaseStatusMvcModel.CopyFromEntityList(dbData, 0);
            ViewBag.CaseStatus = new SelectList(mvc, "CaseStatusId", "Name", selectedValue);
        }

        public void InitilizeCaseType(object selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (AppUser.UserType == UserType.AG || AppUser.UserType == UserType.AAG)
                items.Add(new SelectListItem { Text = "All", Value = "All" });
            if (AppUser.UserType != UserType.CSC)
                items.Add(new SelectListItem { Text = "Criminal", Value = "Criminal" });
            if (AppUser.UserType != UserType.Advocate)
                items.Add(new SelectListItem { Text = "Civil", Value = "Civil" });

            ViewBag.CaseType = new SelectList(items, "Value", "Text", selectedValue);
        }

    }
}
