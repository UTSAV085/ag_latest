﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.DataService;

namespace UPCourt.Web.Controllers
{
    public class LoginController : BaseMVCController
    {
        private CustomUserManager CustomUserManager { get; set; }

        public LoginController()
            : this(new CustomUserManager())
        {
        }

        public LoginController(CustomUserManager customUserManager)
        {
            CustomUserManager = customUserManager;
        }


        //
        // GET: /Login
        [AllowAnonymous]
        public ActionResult Index(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var dbUser = UserDataService.GetLite(db).FirstOrDefault(i => i.Email == model.LoginName);
            if (dbUser == null)
            {
                ModelState.AddModelError(nameof(LoginViewModel.LoginName), "Invalid login attempt.");
                return View(model);
            }
            if (dbUser != null)
            {
                var tempGivenHash = PasswordHelper.EncodePassword(model.Password, dbUser.PasswordSalt);

                if (dbUser.PasswordHash != tempGivenHash)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.Password), "Invalid user name or password.");
                    return View(model);
                }
                if (dbUser.Status != UserStatus.Active)
                {
                    ModelState.AddModelError(nameof(LoginViewModel.LoginName), "User is not active, please talk to admins");
                    return View(model);
                }
            }
            //checking for admin user login log
            UserLogInLog lastLog = UserLogInLogDataService.GetLite(db).FirstOrDefault(i => i.UserId == dbUser.UserId && !i.IsExpired);
            if (lastLog != null)
            {
                lastLog.SignOut = Extended.CurrentIndianTime;
                lastLog.IsExpired = true;
                lastLog.IsForcedExpired = true;
                db.SaveChanges();
            }
            UserLogInLog newlog = new UserLogInLog();
            newlog.UserLogInLogId = Guid.NewGuid();
            newlog.UserId = dbUser.UserId;
            newlog.ApiSessionToken = Guid.NewGuid();
            newlog.SignIn = Extended.CurrentIndianTime;

            db.UserLogInLogs.Add(newlog);
            db.SaveChanges();
            var dbData = UserRoleDataService.GetDetail(db).Where(i => i.UserRoleId == dbUser.UserRoleId).FirstOrDefault();

            var apUser = new ApplicationUser();
            apUser.Id = dbUser.UserId.ToString();
            apUser.UserName = dbUser.Name;
            apUser.UserType = dbUser.UserType;

            await SignInAsync(apUser, model.RememberMe);
            if (dbData.Name == "Admin")
            {
                return RedirectToAction("Dashboard", "Home");
            }
            else if (dbData.Name == "CSC")
            {
                return RedirectToAction("CSCDashboard", "Home");
            }
            return RedirectToLocal(returnUrl);
        }
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {

            //TODO: clear catch of stored
            AuthenticationManager.SignOut(Startup.CokkieName);

            var identity = await CustomUserManager.CreateIdentityAsync(user, Startup.CokkieName);

            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (CustomUserManager != null)
                {
                    CustomUserManager.Dispose();
                    CustomUserManager = null;
                }


            }

            base.Dispose(disposing);
        }
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(Startup.CokkieName);
            return RedirectToAction("Index");
        }
    }
}
