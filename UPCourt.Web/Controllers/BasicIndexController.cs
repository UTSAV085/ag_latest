﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UPCourt.Entity;
using UPCourt.Web.Core;
using UPCourt.Web.Models;
using UPCourt.Web.Service.BusinessService;

namespace UPCourt.Web.Controllers
{
    public class BasicIndexController : BaseMVCController
    {
        // GET: BasicIndex
        [AllowAnonymous]
        [HttpPost]
        public ActionResult UploadFiles()
        {
            //// Checking no of files injected in Request object  
            //if (Request.Files.Count > 0)
            //{
            //    try
            //    {
            //        //  Get all files from Request object  
            //        HttpFileCollectionBase files = Request.Files;
            //        for (int i = 0; i < files.Count; i++)
            //        {
            //            //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
            //            //string filename = Path.GetFileName(Request.Files[i].FileName);  

            //            HttpPostedFileBase file = files[i];
            //            string fname;

            //            // Checking for Internet Explorer  
            //            if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
            //            {
            //                string[] testfiles = file.FileName.Split(new char[] { '\\' });
            //                fname = testfiles[testfiles.Length - 1];
            //            }
            //            else
            //            {
            //                fname = file.FileName;
            //            }

            //            // Get the complete folder path and store the file inside it.  
            //            fname = Path.Combine(Server.MapPath("~/Uploads/"), fname);
            //            //file.SaveAs(fname);
            //            NoticeBasicIndex noticeBasicIndex = new NoticeBasicIndex();
            //            noticeBasicIndex.NoticeBasicIndexId = Guid.NewGuid();
            //            //noticeBasicIndex.Annexure = model.Annexure;
            //            //noticeBasicIndex.Particulars = model.Particulars;
            //            //noticeBasicIndex.IndexDate = model.IndexDate;
            //            Document document = new Document();
            //            document.DocumentId = Guid.NewGuid();
            //            document.Name = file.FileName;
            //            document.NoticeBasicIndexId = noticeBasicIndex.NoticeBasicIndexId;
            //            document.Type = DocumentType.Image;
            //            document.EntityType = DocumentEntityType.Notice;
            //            document.DocumentUrl = fname;
            //            document.Ordinal = 1;
            //            document.ModifyBy = "System";
            //            document.Status = DocumentStatus.Added;
            //            document.Size = file.ContentLength;
            //            noticeBasicIndex.Documents.Add(document);
            //            int pagenumber = NoticeBusinessService.GetPages_PDF(fname);
            //            document.Size = file.ContentLength;
            //            //model.Documents.Add(document);
            //            //CommonSaveChanges(rModel, noticeBasicIndex, model, true);
            //        }
            //        // Returns message that successfully uploaded  
            //        return Json("File Uploaded Successfully!");
            //    }
            //    catch (Exception ex)
            //    {
            //        return Json("Error occurred. Error details: " + ex.Message);
            //    }
            //}
            //else
            //{
            //    return Json("No files selected.");
            //}

            var uploadDocs = FileUploadCommon(rModel, Enumerable.Empty<Document>(), Enumerable.Empty<DocumentMvcModel>(), 1, 10, DocumentType.Any);

            

            if (!rModel.IsSuccess)
            {
                return Json(rModel, JsonRequestBehavior.AllowGet);
            }
            //DocumentBusinessService.AssignEntityIdAndType(tempEntity.Documents, uploadDocs, i => i.NoticeBasicIndexId, tempEntity.NoticeBasicIndexId, DocumentEntityType.Notice);

            var finalDocs = DocumentMvcModel.CopyFromEntityList(uploadDocs, 0);

            foreach (var doc in finalDocs)
            {
                doc.PageCount= NoticeBusinessService.GetPages_PDF(FileUploadWebApiHelper.GetServerFilePathFromUrl(doc.DocumentUrl));
            }

            rModel.Data = finalDocs;

            return Json(rModel,JsonRequestBehavior.AllowGet);
        }

        
    }
}