﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class Court
    {
        public Court()
        {
            AdvocateCourts = new HashSet<AdvocateCourt>();
            CourtOpenCloseLogs = new HashSet<CourtOpenCloseLog>();
            Cases = new HashSet<Case>();
            this.CaseTransfers = new HashSet<CaseTransfer>();
        }
        public Guid CourtId { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public CourtBenchType CourtBench { get; set; }
        public CourtStatus Status { get; set; }
        public bool IsClose { get; set; }
        public string ModifyBy { get; set; } 
        public ICollection<AdvocateCourt> AdvocateCourts { get; set; }
        public ICollection<CourtOpenCloseLog> CourtOpenCloseLogs { get; set; }
       
        public ICollection<Case> Cases { get; set; }
        public ICollection<CaseTransfer> CaseTransfers { get; set; }
    }
    public enum CourtBenchType
    {
        Single = 0,
        Double = 1,
        Full = 2
    }
    public enum CourtStatus
    {
        Inactive = 0,
        Active = 1
    }
}
