﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PartyName), UserNameProp: nameof(ModifyBy))]
    [EntityChangeTrackerParentKey(nameof(NoticePartyId), nameof(NoticeRespondentId),
        nameof(CasePartyId), nameof(CaseRespondentId))]
    public class PartyAndRespondent
    {
        public Guid PartyAndRespondentId { get; set; }
        public Guid? NoticePartyId { get; set; }
        public Guid? NoticeRespondentId { get; set; }
        public Guid? CasePartyId { get; set; }
        public Guid? CaseRespondentId { get; set; }
        public NoticeOrCaseType NoticeOrCaseType { get; set; }
        public PartyAndRespondentType PartyAndRespondentType { get; set; }
        //CaseId needed
        public PartyType PartyType { get; set; }
        public string PartyName { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        //department details
        public Guid? GovernmentDepartmentId { get; set; }       
        public string ModifyBy { get; set; }       
        public Notice NoticeParty { get; set; }
        public Notice NoticeRespondent { get; set; }
        public Case CaseParty { get; set; }
        public Case CaseRespondent { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }
    }

    public enum NoticeOrCaseType
    {
        Notice=0,
        Case=1
    }
    public enum PartyAndRespondentType
    {
        Party = 0,
        Respondent = 1
    }
    public enum PartyType
    {
        Individual = 0,
        Department = 1
    }
}
