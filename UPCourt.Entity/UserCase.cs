﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public class UserCase
    {
        public Guid UserCaseId { get; set; }
        public Guid UserId { get; set; }
        public Guid CaseId { get; set; }
        public string ModifyBy { get; set; }
        public User User { get; set; }
        public Case Case { get; set; }
    }
}
