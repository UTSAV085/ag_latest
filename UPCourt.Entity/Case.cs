﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PetitionNo), UserNameProp: nameof(ModifyBy))]
    public class Case
    {
        public Case()
        {
            this.CaseHearings = new HashSet<CaseHearing>();
            this.UserCases = new HashSet<UserCase>();
            this.CaseTransfers = new HashSet<CaseTransfer>();
        }
        public Guid CaseId { get; set; }
        public Guid NoticeId { get; set; }
        public int OrdinalNo { get; set; }
        public string PetitionNo { get; set; }
        public Guid? CaseStatusId { get; set; }
        public Guid? AdvocateId { get; set; }
        public Guid? CourtId { get; set; }
        public Guid? RegionId { get; set; }
        public CaseMovementStatus? Status2 { get; set; }
        public NoticeType Type { get; set; }
        public Guid PetitionerId { get; set; }
        public Guid? NoticeCounselorId { get; set; }
        public DateTime? NextHearingDate { get; set; }
        public string ModifyBy { get; set; }        
        public CaseStatus CaseStatus { get; set; }
        public Advocate Advocate { get; set; }
        public Court Court { get; set; }
        public Region Region { get; set; } 
        public Notice Notice { get; set; }
        public Petitioner Petitioner { get; set; }
        public NoticeCounselor NoticeCounselor { get; set; }
        public ICollection<UserCase> UserCases { get; set; }
        public ICollection<PartyAndRespondent> Parties { get; set; }
        public ICollection<PartyAndRespondent> Respondents { get; set; }
        public ICollection<NoticeBasicIndex> BasicIndexs { get; set; }
        public ICollection<CaseHearing> CaseHearings { get; set; }          
        public ICollection<CaseTransfer> CaseTransfers { get; set; }
    }
    public enum CaseMovementStatus
    {
        [Description("Review Petiton")]
        ReviewPetiton = 0,
        [Description("Moved To Supreme Court")]
        MovedToSupremeCourt = 1,
        [Description("Send Back To High Court")]
        SendBackToHighCourt = 2
    }
}
