﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    public class NoticeCounselor
    {        
        public Guid NoticeCounselorId { get; set; }
        public string Name { get; set; }
        public string RollNo { get; set; }
        public string EnrollmentNo { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; } 
        public string ModifyBy { get; set; }        

        public Notice Notice { get; set; }
        public Case Case { get; set; }
    }
}
