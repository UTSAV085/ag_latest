﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: false, DisplayProp: null, UserNameProp: null)]
    public class UserLogInLog
    {
        public Guid UserLogInLogId { get; set; }
        public Guid UserId { get; set; }
        public DateTime SignIn { get; set; }
        public DateTime SessionExpireTime { get; set; }
        public DateTime? SignOut { get; set; }
        public Guid ApiSessionToken { get; set; }
        public bool IsExpired { get; set; }
        public bool IsForcedExpired { get; set; }

        public virtual User User { get; set; }
    }
}
