﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(NoticeNo), UserNameProp: nameof(ModifyBy))]
    public class Notice
    {
        public Notice()
        {
            Parties = new HashSet<PartyAndRespondent>();
            Respondents = new HashSet<PartyAndRespondent>();
            BasicIndexs = new HashSet<NoticeBasicIndex>(); 
        }
        public Guid NoticeId { get; set; }
        public Guid PetitionerId { get; set; }
        public Guid? NoticeCounselorId { get; set; }       
        public NoticeType NoticeType { get; set; }
        public int SerialNo { get; set; }
        public string NoticeNo { get; set; }
        public DateTime? NoticeDate { get; set; }
        //case details
        public string Subject { get; set; }
        //only for Dashboard
        public bool IsContextOrder { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        public NoticeStatus Status { get; set; }
       
        public string ModifyBy { get; set; } 
        public Petitioner Petitioner { get; set; }
        public NoticeCounselor NoticeCounselor { get; set; }
        
        public ICollection<PartyAndRespondent> Parties { get; set; }
        public ICollection<PartyAndRespondent> Respondents { get; set; }
        public ICollection<NoticeBasicIndex> BasicIndexs { get; set; }
        public Case Case { get; set; }
    }
    public enum NoticeType
    {
        Criminal = 0,
        Civil = 1
    }
    public enum NoticeStatus
    {
        Pending = 0,
        Accept = 1,
        Reject = 2
    }
}
