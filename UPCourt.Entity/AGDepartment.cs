﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    public class AGDepartment
    {
        public Guid AGDepartmentId { get; set; }
        public string Name { get; set; }
        public string DepartmentHeadName { get; set; }
        public DepartmentStatus Status { get; set; }
        public bool IsDefault { get; set; } 
        public string ModifyBy { get; set; }

        public User user { get; set; }

    }
    public enum DepartmentStatus
    {
        Active = 1,
        Inactive = 0
    }
}
