﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(PageName),UserNameProp:nameof(ModifyBy))]
    public class PagePermission
    {
        public PagePermission()
        {
            UserRolePagePermissions = new HashSet<UserRolePagePermission>();
        }
        public Guid PagePermissionId { get; set; }
        public IEnumerable<PagePermissionType> PermissionTypes { get; set; }
        public PageName PageName { get; set; }
        public string ModifyBy { get; set; }
        public ICollection<UserRolePagePermission> UserRolePagePermissions { get; set; }
    }

    public enum PageName
    {
        [Description("User")]
        [PagePermission("User",  "User", ParentMenue.Master, "far fa-circle nav-icon", 2,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, 
            PagePermissionType.CanView, PagePermissionType.CanViewHistory, 
            PagePermissionType.User_CanApprove)]
        User = 0,

        [Description("AG Department")]
        [PagePermission("AG Department", "AGDepartment", ParentMenue.Master, "far fa-circle nav-icon", 3,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, 
           PagePermissionType.CanView, PagePermissionType.CanViewHistory)]
        AGDepartment = 1,

        [Description("User Role")]
        [PagePermission("User Role", "UserRole", ParentMenue.Master, "far fa-circle nav-icon", 4,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit, 
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        UserRole = 2,

        [Description("Advocate")]
        [PagePermission("Advocate", "Advocate", ParentMenue.Master, "far fa-circle nav-icon", 5,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        Advocate = 3,

        [Description("Case Status")]
        [PagePermission("Case Status", "CaseStatus", ParentMenue.Master, "far fa-circle nav-icon", 6,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        CaseStatus = 4,

        [Description("Court")]
        [PagePermission("Court", "Court", ParentMenue.Master, "far fa-circle nav-icon", 7,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        Court = 5,

        [Description("Designation")]
        [PagePermission("Designation", "Designation", ParentMenue.Master, "far fa-circle nav-icon", 8,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        Designation = 6,

        [Description("Goverment Department")]
        [PagePermission("Goverment Department", "GovermentDepartment", ParentMenue.Master, "far fa-circle nav-icon", 9,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        GovermentDepartment = 7,

        [Description("Region")]
        [PagePermission("Region", "Region", ParentMenue.Master, "far fa-circle nav-icon", 10,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        Region = 8,
        
        [Description("User Credential Request")]
        [PagePermission("User Credential Request", "UserCredentialRequest", ParentMenue.Transaction, "far fa-circle nav-icon", 2,
            PagePermissionType.CanCreate, PagePermissionType.CanEdit,
            PagePermissionType.CanView, PagePermissionType.CanViewHistory,
            PagePermissionType.CanDelete)]
        UserCredentialRequest = 9,

        [Description("Dashboard")]
        [PagePermission("Dashboard", "Home", ParentMenue.StandAlone, "nav-icon fas fa-tachometer-alt", 1,
            PagePermissionType.CanView)]
        Dashboard = 10,

        [Description("Pending Notice")]
        [PagePermission("Pending Notice", "NoticeCase", ParentMenue.Transaction, "far fa-circle nav-icon", 1,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanViewHistory)]
        NoticeToCase = 11,

        [Description("Case")]
        [PagePermission("Case", "Case", ParentMenue.Transaction, "far fa-circle nav-icon", 1,
            PagePermissionType.CanEdit, PagePermissionType.CanView, PagePermissionType.CanViewHistory)]
        Case = 12,
    }
    public enum PagePermissionType
    {
        [Description("Create")]
        [PagePermissionTypeAttribute("Create", "Create")]
        CanCreate = 0,
        [Description("Edit")]
        [PagePermissionTypeAttribute("Edit", "Edit")]
        CanEdit = 1,
        [Description("Delete")]
        [PagePermissionTypeAttribute("Delete", "Delete")]
        CanDelete = 2,
        [Description("View")]
        [PagePermissionTypeAttribute("View", "Index")]
        CanView = 3,
        [Description("Audit Log")]
        [PagePermissionTypeAttribute("Audit Log", "Log")]
        CanViewHistory = 4,
        /// <summary>
        /// Example of custom permision
        /// </summary>
        [Description("Can approve")]
        [PagePermissionTypeAttribute("Can approve", "Approval")]
        User_CanApprove = 5,
    }
}
