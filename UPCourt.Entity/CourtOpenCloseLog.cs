﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(CourtId), UserNameProp: nameof(ModifyBy))]
    public class CourtOpenCloseLog
    {
        public Guid CourtOpenCloseLogId { get; set; }
        public Guid CourtId { get; set; }
        public DateTime? CloseFromDate { get; set; }
        public DateTime? CloseToDate { get; set; }
        public string ModifyBy { get; set; }
        public Court Court { get; set; }
    }
}
