﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Message), UserNameProp: nameof(ModifyBy))]
    public class Notification
    {
        public System.Guid NotificationId { get; set; }
        public System.Guid SourceEntityId { get; set; }
        public NotificationSourceEntityType SourceEntityType { get; set; }
        public Guid? TargetEntityId { get; set; }
        public NotificationTargetEntityType TargetEntityType { get; set; }
        public NotificationStatus Status { get; set; }
        public string Message { get; set; }  
        public string ModifyBy { get; set; }
    }
    public enum NotificationSourceEntityType
    {
        Notice = 0,
        Case=1
    }
    public enum NotificationTargetEntityType
    {
        AG = 0,
        AIG = 1,    
        AGDepartment=2
    }
    public enum NotificationStatus
    {
        Created = 0,
        Delivered = 1,
        Read = 2
    }
}
