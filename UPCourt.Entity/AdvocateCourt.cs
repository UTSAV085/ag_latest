﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    //TODO: display property for mapping table not applicable
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(AdvocateCourtId), UserNameProp: nameof(ModifyBy))]
    public class AdvocateCourt
    {
        public Guid AdvocateCourtId { get; set; }
        public Guid AdvocateId { get; set; }
        public Guid CourtId { get; set; }
        public Advocate Advocate { get; set; }
        public Court Court { get; set; }
        public string ModifyBy { get; set; }
    }
}
