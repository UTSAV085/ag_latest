﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    public class CaseStatus
    {
        public CaseStatus()
        {
            Cases = new HashSet<Case>();
        }
        public Guid CaseStatusId { get; set; }
        public string Name { get; set; }
        public string HexColorCode { get; set; }
        public string ModifyBy { get; set; }
        /// <summary>
        /// This field will be used by systme internally to identity specfic status to re-opon a case
        /// </summary>
        public CaseStatusType Type { get; set; }
        public ICollection<Case> Cases { get; set; }
    }

    public enum CaseStatusType
    {
        Normal=0,
        Dispose=1,
    }
}
