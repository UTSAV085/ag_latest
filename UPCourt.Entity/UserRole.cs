﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class UserRole
    {
        public UserRole()
        {
            this.UserRolePagePermissions = new HashSet<UserRolePagePermission>();
            this.Users = new HashSet<User>();
        }
        public Guid UserRoleId { get; set; }
        public string Name { get; set; }
        public UserRoleStatus Status { get; set; }
        public bool IsSytemCreated { get; set; }
        public string ModifyBy { get; set; }
        public ICollection<UserRolePagePermission> UserRolePagePermissions { get; set; }
        public ICollection<User> Users { get; set; }

    }

    public enum UserRoleStatus
    {
        Inactive = 0,
        Active = 1,
    }
}