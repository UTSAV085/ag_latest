﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(ContactNo), UserNameProp: nameof(ModifyBy))]
    public class GovernmentDepartmentContactNo
    {
        public Guid GovernmentDepartmentContactNoId { get; set; }
        public Guid GovernmentDepartmentId { get; set; }
        public string ContactNo { get; set; } 
        public string ModifyBy { get; set; } 
        public GovernmentDepartment GovernmentDepartment { get; set; }
        
    }
}
