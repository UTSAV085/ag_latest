﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class EntityChangeTrackerTableAttribute : Attribute
    {
        readonly string _DisplayProp;
        readonly bool _EnableChangeLog;
        readonly string _UserNameProp;
        // This is a positional argument
        public EntityChangeTrackerTableAttribute(bool EnableChangeLog, string DisplayProp, string UserNameProp)
        {
            this._EnableChangeLog = EnableChangeLog;
            this._DisplayProp = this.DisplayProp;
            this._UserNameProp = UserNameProp;
        }
        public bool EnableChangeLog
        {
            get { return _EnableChangeLog; }
        }
        public string DisplayProp
        {
            get { return _DisplayProp; }
        }
        public string UserNameProp
        {
            get { return _UserNameProp; }
        }
    }

    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class EntityChangeTrackerIgnoreAttribute : Attribute
    {
        /// <summary>
        /// Keep prop in change log so that alteast audit log can capture someting has changed
        /// </summary>
        readonly bool _KeepProp;
        readonly object _KeepPropDefaultValue;
        public EntityChangeTrackerIgnoreAttribute(bool KeepProp = false, object KeepPropDefaultValue = null)
        {
            this._KeepProp = KeepProp;
            this._KeepPropDefaultValue = KeepPropDefaultValue;
        }
        public bool KeepProp
        {
            get { return _KeepProp; }
        }
        public object KeepPropDefaultValue
        {
            get { return _KeepPropDefaultValue; }
        }
    }

    [System.AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class EntityChangeTrackerParentKeyAttribute : Attribute
    {
        readonly IEnumerable<string> _parentPrimaryKeyList;

        public EntityChangeTrackerParentKeyAttribute(params string[] ParentPrimaryKeyList)
        {
            this._parentPrimaryKeyList = ParentPrimaryKeyList;
        }

        public IEnumerable<string> ParentPrimaryKeyList
        {
            get { return _parentPrimaryKeyList; }
        }
    }

}
