﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class PagePermissionAttribute : Attribute
    {
        IEnumerable<PagePermissionType> pagePermissionTypes;
        public PagePermissionAttribute(string DisplayText,
            string ControllerName, ParentMenue ParentMenue, 
            string IconClass, int DisplayIndex,
            params PagePermissionType[] permissionTypes)
        {
            this.pagePermissionTypes = permissionTypes;
            this._DisplayText = DisplayText;
            this._ControllerName = ControllerName;
            this._ParentMenue = ParentMenue;
            this._DisplayIndex = DisplayIndex;
            this._IconClass = IconClass;
        }

        public IEnumerable<PagePermissionType> PermissionTypes
        {
            get { return pagePermissionTypes; }
        }
        int _DisplayIndex;
        public int DisplayIndex { get { return _DisplayIndex; } }
        string _DisplayText;
        public string DisplayText { get { return _DisplayText; } }
        ParentMenue _ParentMenue;
        public ParentMenue ParentMenue { get { return _ParentMenue;  } }
        string _ControllerName;
        public string ControllerName { get { return _ControllerName;  } }
        string _IconClass;
        public string IconClass { get { return _IconClass;  } }
    }

    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public class PagePermissionTypeAttribute : Attribute
    {
        public PagePermissionTypeAttribute(string DisplayText, string ActionName)
        {
            _DisplayText = DisplayText;
            _ActionName = ActionName;
        }
        string _DisplayText;
        public string DisplayText { get { return _DisplayText; } }

        string _ActionName;
        public string ActionName { get { return _ActionName; } }
    }

    public enum ParentMenue
    {
        [Display(Description = "", Order = 1)]
        StandAlone,
        [Display(Description = "Master", Order = 2)]
        Master,
        [Display(Description = "Transaction", Order = 3)]
        Transaction
    }
}
