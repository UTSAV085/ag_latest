﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace UPCourt.Entity
{
    /// <summary>
    /// Specifies different part of a page url
    /// </summary>
    public enum PageUrlPartition
    {
        /// <summary>
        /// Only page address part of an url address
        /// </summary>
        /// <example>http://localhost:50220/Buyer/BuyerRegistration.aspx</example>
        OnlyPageUrl = 0,

        /// <summary>
        /// The only query string part
        /// </summary>
        /// <example>LinkId=696&LinkToken=c8881096-15f3-4ecd-84ad-dd877c98eac1&SendBy=Merchant</example>
        OnlyQueryStringPart = 1
    }

    /// <summary>
    /// Represent different way to split an word string by capital letter
    /// </summary>
    public enum SplitWord
    {
        /// <summary>
        /// <para>Input=IAmAnINDIAN</para>
        /// <para>OutPut=I Am An INDIAN</para>
        /// </summary>
        ConsiderRelatedCapital,

        /// <summary>
        /// <para>Input=IAmAnINDIAN</para>
        /// <para>OutPut=I Am An I N D I A N</para>
        /// </summary>
        BreakOnAnyCapital
    }

    /// <summary>
    /// Implemts or extends various .net core functions, to make development easy and less error prone.
    /// </summary>
    //[DebuggerStepThrough]
    public static class Extended
    {
        /// <summary>
        /// Character used in date time string as separator
        /// </summary>
        private const char DateTimeReplacableSeparator = '/';

        public const string EclipseCharcter = "...";

        #region Convert Functions

        /// <summary>
        /// Get EnumAttribute of an enumeration member, if specified
        /// </summary>
        /// <param name="value">Enumeration member of which attribute to get</param>
        /// <param name="ReturnEmptyAttributeIfNotPresent">if set to <c>true</c> [return empty attribute if not present].</param>
        /// <returns>A EnumAttribute object</returns>
        public static DisplayAttribute GetEnumDisplayAttribute(this Enum value)
        {
            return value.IsNull() ? new DisplayAttribute() :
                        value.GetType()
                        .GetMember(value.ToString())
                        .FirstOrDefault()
                        .GetCustomAttribute<DisplayAttribute>();
        }
        /// <summary>
        /// Get Custom Attribute
        /// </summary>
        /// <typeparam name="T">Enum</typeparam>
        /// <param name="value">Enum Value</param>
        /// <returns></returns>
        public static T GetCustomAttribute<T>(this Enum value) where T : Attribute
        {
            var enumType = value.GetType();
            var name = Enum.GetName(enumType, value);
            return enumType.GetField(name).GetCustomAttributes(false).OfType<T>().SingleOrDefault();
        }

        ///// <summary>
        ///// Returns string format represents dateformat seperated by /
        ///// </summary>
        ///// <param name="Format">An enumeration value indicates date format to be</param>
        ///// <returns>
        ///// string form of a DateFormat
        ///// </returns>
        //public static string GetFormat(this DateFormat Format)
        //{
        //    string tempFormat = string.Empty;
        //    switch (Format)
        //    {
        //        case DateFormat.DD_MM_YYYY:
        //            tempFormat = "dd/MM/yyyy";
        //            break;

        //        case DateFormat.MM_DD_YYYY:
        //            tempFormat = "MM/dd/yyyy";
        //            break;

        //        case DateFormat.YYYY_MM_DD:
        //            tempFormat = "yyyy/MM/dd";
        //            break;

        //        default:
        //            break;
        //    }

        //    return tempFormat;
        //}

        /// <summary>
        /// Converts the value of the specified object to a boolean value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A true/false value, or false if value is null or DBNull
        /// </returns>
        public static Boolean ToBool(this object obj)
        {
            return Convert.ToBoolean(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a boolean
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A true/false value, or null if value is null or DBNull
        /// </returns>
        public static Boolean? ToBoolN(this object obj)
        {
            return CommonForNullable<Boolean>(obj, ToBool);
        }

        /// <summary>
        /// Converts the value of the specified object to a byte value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A byte value, or zero if value is null or DBNull
        /// </returns>
        public static Byte ToByte(object obj)
        {
            return Convert.ToByte(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a byte[] value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A byte value, or zero if value is null or DBNull
        /// </returns>
        public static Byte[] ToByteArray(object obj)
        {
            return obj as byte[];
        }

        /// <summary>
        /// Converts the value of the specified object to a byte value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A byte value, or null if value is null or DBNull
        /// </returns>
        public static Byte? ToByteN(object obj)
        {
            return CommonForNullable<Byte>(obj, ToByte);
        }

        /// <summary>
        /// Converts the value of the specified object to a char value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A char value, or System.Char.MinValue if value is null or DBNull
        /// </returns>
        public static Char ToChar(object obj)
        {
            return Convert.ToChar(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a char value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A char value, or null if value is null or DBNull
        /// </returns>
        public static Char? ToCharN(object obj)
        {
            return CommonForNullable<Char>(obj, ToChar);
        }

        /// <summary>
        /// Converts the value of the specified object to an equivalent datetime object
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A datetime object equivalent to value, or DateTime.MinValue if value is null
        /// </returns>
        public static DateTime ToDate(this object obj)
        {
            var v = Convert.ToDateTime(obj);
            return v;
        }

        ///// <summary>
        ///// Converts the value of the specified object to an equivalent datetime object, depending on format specified
        ///// </summary>
        ///// <param name="Value">object to be convert</param>
        ///// <param name="Format">Date string format used</param>
        ///// <param name="SeparatorUsed">Character used as separator, default is '-'</param>
        ///// <returns>
        ///// The date and time equivalent of the value, or a date and time equivalent
        ///// of DateTime.MinValue if value is null
        ///// </returns>
        //public static DateTime ToDate(this object Value, DateFormat Format, char SeparatorUsed = '-')
        //{
        //    return ValidateDate(Value, Format, SeparatorUsed).Item2.Value;
        //}

        /// <summary>
        /// Converts the value of the specified object to an equivalent datetime object
        /// </summary>
        /// <param name="obj">object to be converted</param>
        /// <param name="stripTimeComponent">whether to strip the Time Component of the date.</param>
        /// <returns>
        /// Date or DateTime object depending upon stripTimeComponent value
        /// </returns>
        public static DateTime ToDate(this object obj, bool stripTimeComponent)
        {
            DateTime dt = ToDate(obj);
            if (stripTimeComponent)
            {
                return dt.Date;
            }
            return dt;
        }

        /// <summary>
        /// Converts the value of the specified object to an equivalent datetime object
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A datetime object equivalent to value, or null if value is null or DBNull
        /// </returns>
        public static DateTime? ToDateN(this object obj)
        {
            return CommonForNullable<DateTime>(obj, ToDate);
        }

        ///// <summary>
        ///// Converts the value of the specified object to an equivalent datetime object, depending on format specified
        ///// </summary>
        ///// <param name="obj">The object.</param>
        ///// <param name="Format">Date string format used</param>
        ///// <param name="SeparatorUsed">Character used as separator, default is '-'</param>
        ///// <returns>
        ///// The date and time equivalent of the value, or null if value is null or DBNull
        ///// </returns>
        //public static DateTime? ToDateN(this object obj, DateFormat Format, char SeparatorUsed = '/')
        //{
        //    if (obj.IsNull() || obj.ToStringX().IsNullOrEmpty())
        //    {
        //        return null;
        //    }

        //    return ToDate(obj, Format, SeparatorUsed);
        //}

        /// <summary>
        /// Converts the value of the specified object to an equivalent datetime object
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be converted</param>
        /// <param name="stripTimeComponent">whether to strip the Time Component of the date.</param>
        /// <returns>
        /// Null or Date or DateTime object depending upon stripTimeComponent value
        /// </returns>
        public static DateTime? ToDateN(this object obj, bool stripTimeComponent)
        {
            DateTime? dt = CommonForNullable<DateTime>(obj, ToDate);

            if (dt.HasValue && stripTimeComponent)
            {
                return dt.Value.Date;
            }

            return dt;
        }

        /// <summary>
        /// Converts the value of the specified object to an equivalent decimal number
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A decimal number equivalent to value, or zero if value is null
        /// </returns>
        public static Decimal ToDecimal(this object obj)
        {
            return Convert.ToDecimal(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to an equivalent decimal number
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A decimal equivalent to value, or null if value is null or DBNull
        /// </returns>
        public static Decimal? ToDecimalN(this object obj)
        {
            return CommonForNullable<decimal>(obj, ToDecimal);
        }

        /// <summary>
        /// Converts the value of the specified object to a double value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A double value, or zero if value is null or DBNull
        /// </returns>
        public static Double ToDouble(this object obj)
        {
            return Convert.ToDouble(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a double value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A double value, or zero if value is null or DBNull
        /// </returns>
        public static Double? ToDoubleN(this object obj)
        {
            return CommonForNullable<Double>(obj, ToDouble);
        }

        /// <summary>
        /// Converts an object to specified enumeration type
        /// </summary>
        /// <typeparam name="T">Output enumeration type</typeparam>
        /// <param name="value">Object to convert</param>
        /// <returns>
        /// Expected enumeration member
        /// </returns>
        public static T ToEnum<T>(this object value)
        {
            //Checking value is null or DBNull
            if (!value.IsNull())
            {
                return (T)Enum.Parse(typeof(T), value.ToStringX());
            }

            //Returanable object
            object ValueToReturn = null;

            //First checking whether any 'DefaultValueAttribute' is present or not
            var DefaultAtt = (from a in typeof(T).CustomAttributes
                              where a.AttributeType == typeof(DefaultValueAttribute)
                              select a).FirstOrNull();

            //If DefaultAttributeValue is present
            if ((!DefaultAtt.IsNull()) && (DefaultAtt.ConstructorArguments.Count == 1))
            {
                ValueToReturn = DefaultAtt.ConstructorArguments[0].Value;
            }

            //If still no value found
            if (ValueToReturn.IsNull())
            {
                //Trying to get the very first property of that enum
                Array Values = Enum.GetValues(typeof(T));

                //getting very first member of this enum
                if (Values.Length > 0)
                {
                    ValueToReturn = Values.GetValue(0);
                }
            }

            //If any result found
            if (!ValueToReturn.IsNull())
            {
                return (T)Enum.Parse(typeof(T), ValueToReturn.ToStringX());
            }

            return default(T);
        }

        /// <summary>
        /// Converts an object to specified enumeration type
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <typeparam name="T">Output enumeration type</typeparam>
        /// <param name="value">Object to convert</param>
        /// <returns>
        /// Expected enumeration member,else null if object is null or dbnull
        /// </returns>
        public static Nullable<T> ToEnumN<T>(this object value) where T : struct
        {
            // if object is null or db null then return null
            if (value.IsNull())
            {
                return null;
            }

            // else trying to value as enum member
            return ToEnum<T>(value);
        }

        /// <summary>
        /// Converts the value of the specified object to a guid value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A guid value, or empty guid if value is null or DBNull
        /// </returns>
        public static Guid ToGuid(object obj)
        {
            return Guid.Parse(obj.ToStringX());
        }

        /// <summary>
        /// Converts the value of the specified object to a guid value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A guid value, or null if value is null or DBNull
        /// </returns>
        public static Guid? ToGuidN(object obj)
        {
            return CommonForNullable(obj, ToGuid);
        }

        /// <summary>
        /// Converts the value of the specified object to a 32-bit signed integer
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A 32-bit signed integer equivalent to value, or zero if value is null
        /// </returns>
        public static Int32 ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a 32-bit signed integer
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A 32-bit signed integer equivalent to value, or null if value is null or DBNull
        /// </returns>
        public static Int32? ToIntN(this object obj)
        {
            return CommonForNullable<Int32>(obj, ToInt);
        }

        /// <summary>
        /// Converts the value of the specified object to a 64 bit integer value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A 64 bit integer value, or zero if value is null or DBNull
        /// </returns>
        public static Int64 ToLong(object obj)
        {
            return Convert.ToInt64(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a 64 bit integer value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A 64 bit integer value, or null if value is null or DBNull
        /// </returns>
        public static Int64? ToLongN(object obj)
        {
            return CommonForNullable<Int64>(obj, ToLong);
        }

        /// <summary>
        /// Converts the value of the specified object to a Int16 value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A Int16 value, or zero if value is null or DBNull
        /// </returns>
        public static Int16 ToShort(object obj)
        {
            return Convert.ToInt16(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a Int16 value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A Int16 value, or null if value is null or DBNull
        /// </returns>
        public static Int16? ToShortN(object obj)
        {
            return CommonForNullable(obj, ToShort);
        }

        /// <summary>
        /// Converts the value of the specified object to a single value
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A single value value, or zero if value is null or DBNull
        /// </returns>
        public static Single ToSingle(this object obj)
        {
            return Convert.ToSingle(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to a single value
        /// <para>If value is null or DBNull return null</para>
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A single value, or null if value is null or DBNull
        /// </returns>
        public static Single? ToSingleN(object obj)
        {
            return CommonForNullable<Single>(obj, ToSingle);
        }

        ///// <summary>
        ///// Checks and converts an object to datetime object if it is not null or DbNull
        ///// <para>else return null</para>
        ///// </summary>
        ///// <param name="Value">Object to convert</param>
        ///// <param name="Format">Date string format used</param>
        ///// <param name="SeparatorUsed">Character used as separator. Default value is '-'</param>
        ///// <returns>
        ///// <para>Item1 : A true/false value indicates whether it is a valid datetime or not </para>
        ///// <para>item2 : Null or datetime object</para>
        ///// </returns>
        //public static TwoProp<bool, DateTime?> ValidateDate(object Value, DateFormat Format, char SeparatorUsed = '-')
        //{
        //    TwoProp<bool, DateTime?> ReturnValue = new TwoProp<bool, DateTime?> { Item1 = false, Item2 = null };

        //    String tempValue = Value.ToStringX();

        //    // Handling general logic
        //    if (tempValue.IsNullOrEmpty())
        //    {
        //        return ReturnValue;
        //    }

        //    // Replacing separator
        //    tempValue = tempValue.TrimX().Replace(SeparatorUsed, DateTimeReplacableSeparator);

        //    //// Reconstructing date part so that it support various format
        //    //// Like '5/4/14','05/4/2014','05/04/14'
        //    //// Other wise parse exact do not suporting this

        //    //var AllDateField = tempValue.Split(DateTimeReplacableSeparator);

        //    //StringBuilder sb = new StringBuilder();

        //    //// Only datetime binding
        //    //for (int i = 0; i < AllDateField.Length && i < 3; i++)
        //    //{
        //    //    string tempDatePart = AllDateField[i];

        //    //    if (tempDatePart.Length == 1)
        //    //    {
        //    //        tempDatePart = "0" + tempDatePart;
        //    //    }

        //    //    // Handling year value
        //    //    // value to refine '05/06/14 12:58pm'
        //    //    if (tempDatePart.Length > 4)
        //    //    {
        //    //        // handling "14 12:58pm" part
        //    //        tempDatePart = tempDatePart.Split(' ')[0];

        //    //        // handling '14' part
        //    //        if (tempDatePart.Length == 2)
        //    //        {
        //    //            tempDatePart = "20" + tempDatePart;
        //    //        }
        //    //    }

        //    //    sb.Append(tempDatePart);
        //    //    sb.Append(DateTimeReplacableSeparator);
        //    //}

        //    //tempValue = sb.ToString();

        //    //if (!tempValue.IsNullOrEmpty())
        //    //{
        //    //    tempValue = tempValue.Substring(0, tempValue.Length - 1);
        //    //}

        //    DateTime tempDate;
        //    var tempCulture = Extended.CurrentCulture;
        //    tempCulture.DateTimeFormat.ShortDatePattern = Format.GetFormat();

        //    ReturnValue.Item1 = DateTime.TryParse(tempValue, tempCulture, DateTimeStyles.None, out tempDate);

        //    if (ReturnValue.Item1)
        //    {
        //        ReturnValue.Item2 = tempDate;
        //    }

        //    return ReturnValue;
        //}

        /// <summary>
        /// Specified function converts value of the specified object only if object is not null or DBNull
        /// </summary>
        /// <typeparam name="T">Output result type</typeparam>
        /// <param name="obj">Object to convert</param>
        /// <param name="objfunction">Function responbile to convert the object</param>
        /// <returns>T type represention of provided object if object is not null or dbnull; Otherwise null.</returns>
        private static Nullable<T> CommonForNullable<T>(object obj, Func<object, T> objfunction) where T : struct
        {
            // if object is null or db null then return null
            if (IsNull(obj))
            {
                return null;
            }

            // Execute function and return result
            return objfunction(obj);
        }

        /// <summary>
        /// Converts the value of the specified object to int then tostring
        /// </summary>
        /// <param name="obj">object to be convert</param>
        /// <returns>
        /// A string value
        /// </returns>
        public static string ToIntString(object obj)
        {
            return ToInt(obj).ToString();
        }

        #endregion Convert Functions

        #region String Entend functions

        /// <summary>
        /// Split string word to sentense by pattern provided
        /// </summary>
        /// <param name="stringToSplit">String to split</param>
        /// <param name="SplitWordMethod">The pattern used to split the word</param>
        /// <returns>
        /// Splited string as sentence
        /// </returns>
        public static string BreakByCapitalLetter(string stringToSplit, SplitWord SplitWordMethod = SplitWord.BreakOnAnyCapital)
        {
            switch (SplitWordMethod)
            {
                case SplitWord.ConsiderRelatedCapital:
                    var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);
                    return r.Replace(stringToSplit, " ");

                case SplitWord.BreakOnAnyCapital:
                default:
                    return System.Text.RegularExpressions.Regex.Replace(stringToSplit, "([A-Z])", " $1").Trim();
            }
        }

        /// <summary>
        /// Return first not null value in a unite
        /// </summary>
        /// <typeparam name="T">Type of result</typeparam>
        /// <param name="First">First element of unite</param>
        /// <param name="Second">Next element of unite</param>
        /// <returns>
        /// First not null value
        /// </returns>
        public static T Coalesce<T>(Nullable<T> First, T Second) where T : struct
        {
            return First.HasValue ? First.Value : Second;
        }

        /// <summary>
        /// Returns empty string if specified string is null
        /// </summary>
        /// <param name="caller">System.String of which value to compute</param>
        /// <returns>
        /// Empty if value is null, else value
        /// </returns>
        public static string GetEmptyIfNull(this string caller)
        {
            if (caller == null)
            {
                return string.Empty;
            }
            return caller;
        }

        /// <summary>
        /// Returns the description of an enum element. If Description not set then returns enum name as string.
        /// </summary>
        /// <param name="value">The enum element</param>
        /// <returns>
        /// Description string of Enum
        /// </returns>
        public static string GetEnumDescription(this Enum value)
        {
            System.ComponentModel.DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false)
                .SingleOrDefault() as System.ComponentModel.DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }

        /// <summary>
        /// Returns null if specified string is empty
        /// </summary>
        /// <param name="caller">System.String of which value to compute</param>
        /// <param name="UseTrim">Whether to trim string before compare</param>
        /// <returns>
        /// Null if value is empty, else value
        /// </returns>
        public static string GetNullIfEmpty(this string caller, bool UseTrim = true)
        {
            if (IsNullOrEmpty(caller, UseTrim))
            {
                return null;
            }

            return caller;
        }

        /// <summary>
        /// Returns a null value if integer value is equals to parameter value
        /// </summary>
        /// <param name="caller">Integer object, which value to check</param>
        /// <param name="InsteadOfValue">Integer object, with which callr value to compare</param>
        /// <returns>null value if integer value is equals to parameter value; Otherwise caller value.</returns>
        public static Int32? GetNullIfValueIs(this Int32 caller, Int32 InsteadOfValue)
        {
            if (InsteadOfValue == caller)
            {
                return null;
            }

            return caller;
        }

        /// <summary>
        /// Returns null value if specififed integer value is equal to 0
        /// </summary>
        /// <param name="caller">Object of which value to check</param>
        /// <returns>
        /// A 32-bit integer value if value is not zero, else null
        /// </returns>
        public static Int32? GetNullIfZero(this Int32 caller)
        {
            return GetNullIfValueIs(caller, 0);
        }

        // <summary>
        /// Returns null value if specififed decimal value is equal to 0
        /// </summary>
        /// <param name="caller">Object of which value to check</param>
        /// <returns>
        /// A 32-bit decimal value if value is not zero, else null
        /// </returns>
        public static decimal? GetNullIfZero(this decimal caller)
        {
            return caller != 0 ? caller : (decimal?)null;
        }
        /// <summary>
        /// Get zero if the value is null
        /// </summary>
        /// <param name="DeciamlNo">Decimal value to check</param>
        /// <returns>decimal value</returns>
        public static decimal GetZeroIfNull(this decimal caller)
        {
            return caller.IsNull() ? 0 : caller;
        }

        /// <summary>
        /// Get empty string if decimal is null or zero
        /// </summary>
        /// <param name="caller">Decimal value to check</param>
        /// <returns>Empty string if decimal is null or zero</returns>
        public static string GetEmptyIfZeroN2(this decimal caller)
        {
            return caller.IsNullOrZero() ? string.Empty : caller.ToStringN2();
        }

        /// <summary>
        /// Get empty string if decimal is null or zero
        /// </summary>
        /// <param name="caller">Decimal value to check</param>
        /// <returns>Empty string if decimal is null or zero</returns>
        public static string GetEmptyIfZeroN2(this decimal? caller)
        {
            return !caller.HasValue ? string.Empty : caller.Value.IsZero() ? string.Empty : caller.Value.ToStringN2();
        }
        /// <summary>
        /// Get empty string if decimal is null or zero
        /// </summary>
        /// <param name="caller">Decimal value to check</param>
        /// <returns>Empty string if decimal is null or zero</returns>
        public static decimal GetZeroIfEmpty(this decimal? caller)
        {
            return caller.IsNull() ? 0 : !caller.HasValue ? 0 : caller.Value;
        }

        /// <summary>
        /// Returns a null value if Nullable integer value is equals to 0
        /// </summary>
        /// <param name="caller">Nullable int object, which value to check</param>
        /// <returns>
        /// Value if value is not zero, else null
        /// </returns>
        public static Int32? GetNullIfZero(this Int32? caller)
        {
            return GetNullIfValueIs(caller.GetValueOrDefault(), 0);
        }

        public static Int32 GetZeroIfNull(this Int32? caller)
        {
            return caller == null ? 0 : caller.Value;
        }

        /// <summary>
        /// Returns first string value if it is not null or empty, else return second string value
        /// </summary>
        /// <param name="caller">String to check</param>
        /// <param name="ReplaceValue">Value to return if first string is empty</param>
        /// <param name="UseTrim">A boolean value indicates, whether to trim string before checking</param>
        /// <returns>
        /// Returns first string value if first string value is not null or empty, else return second string value
        /// </returns>
        public static string GetValueIfNullOrEmpty(this string caller, string ReplaceValue, bool UseTrim = true)
        {
            if (IsNullOrEmpty(caller, UseTrim))
            {
                return ReplaceValue;
            }

            return caller;
        }

        /// <summary>
        /// Negation of IsNull check
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>
        /// True if object is null else false
        /// </returns>
        public static bool IsNotNull(this object obj)
        {
            return !IsNull(obj);
        }

        /// <summary>
        /// Return a true/false value indicates whether specified object in equal to null or DBNull value
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <returns>
        /// True value if object is null or dbnull, else false
        /// </returns>
        public static bool IsNull(this object obj)
        {
            // if object is null or db null then return true
            if ((obj == null) || (obj == DBNull.Value))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check whether an object is null or empty
        /// </summary>
        /// <param name="obj">Object to check</param>
        /// <param name="UseTrim">A boolean value indicates, whether to trim string before checking</param>
        /// <returns>
        /// True if specified string is null or empty, else false.
        /// </returns>
        public static bool IsNullOrEmpty(this string obj, bool UseTrim = true)
        {
            // if string is null then return true
            if (obj == null)
            {
                return true;
            }

            //string temp = Convert.ToString(obj);
            // If need to trim
            if (UseTrim)
            {
                obj = obj.Trim();
            }

            // Checking empty
            return (obj == string.Empty);
        }
        public static bool IsNotNullOrEmpty(this string obj, bool UseTrim = true)
        {
            return !IsNullOrEmpty(obj, UseTrim);
        }

        /// <summary>
        /// Returns boolean value indicates that string is a number/integer or not
        /// </summary>
        /// <param name="value">string to validate</param>
        /// <param name="ValidateForInteger">A true or false value indicates validation for decimal or interger respectively
        /// <para>By default it validate for decimal</para></param>
        /// <returns>
        /// true if s is number; otherwise, false
        /// </returns>
        public static bool IsNumber(this string value, bool ValidateForInteger = false)
        {
            if (ValidateForInteger)
            {
                int i;
                return Int32.TryParse(value, out i);
            }

            decimal d;
            return decimal.TryParse(value, out d);

            //Below function is closed by 'Moumit'
            //Because it's not capable to Say false for 99999999999999999999999999999999999.9956
            //It's just check whether all number are like Integer or Decimal

            //bool m_return = false;
            //if (!string.IsNullOrEmpty(value))
            //{
            //    bool DotCount = false;

            //    for (int i = 0; i < value.Length; i++)
            //    {
            //        char c = value[i];
            //        if ((!char.IsDigit(c)) && (c != '.'))
            //        {
            //            break;
            //        }
            //        else
            //        {
            //            //It means double dot found
            //            if (c == '.' && DotCount)
            //            {
            //                break;
            //            }
            //            else if (c == '.')
            //            {
            //                if (ValidateForInteger)
            //                {
            //                    break;
            //                }
            //                DotCount = true;
            //            }
            //        }
            //    }

            //    m_return = true;

            //}
            //return m_return;
        }

        /// <summary>
        /// Returns boolean value indicates that string is a number/integer or not
        /// </summary>
        /// <param name="value">string to validate</param>
        /// <param name="ValidateForInteger">A true or false value indicates validation for decimal or interger respectively
        /// <para>By default it validate for decimal</para></param>
        /// <returns>
        /// true if s is number; otherwise, false
        /// </returns>
        public static bool IsDouble(this string value)
        {
            double d;
            return double.TryParse(value, out d);
        }

        /// <summary>
        /// Returns a string containing a specified number of characters from the left side of a string.
        /// </summary>
        /// <param name="s">Required. String expression from which the leftmost characters are returned</param>
        /// <param name="length">Required. Integer expression. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the entire string is returned.</param>
        /// <returns>
        /// Returns a string containing a specified number of characters from the left side of a string.
        /// </returns>
        public static string Left(this string s, int length)
        {
            return s.SubstringX(0, length);
        }

        /// <summary>
        /// Gets the number of characters in specified System.String object
        /// </summary>
        /// <param name="s">System.String of which length to compute</param>
        /// <param name="UseTrim">whether to use trim object before compute length</param>
        /// <returns>
        /// Gets the number of characters, if object is null then zero
        /// </returns>
        public static int LengthX(this string s, bool UseTrim = true)
        {
            // if string is null then return zero
            if (s == null)
            {
                return 0;
            }

            // if string need to trim
            if (UseTrim)
            {
                s = s.Trim();
            }

            // Returning length
            return s.Length;
        }

        /// <summary>
        /// Returns a string containing a specified number of characters from the right side of a string.
        /// </summary>
        /// <param name="s">Required. String expression from which the rightmost characters are returned.</param>
        /// <param name="length">Required. Integer. Numeric expression indicating how many characters to return. If 0, a zero-length string ("") is returned. If greater than or equal to the number of characters in str, the entire string is returned.</param>
        /// <returns>
        /// Returns a string containing a specified number of characters from the right side of a string.
        /// </returns>
        public static string Right(this string s, int length)
        {
            return s.SubstringX(s.LengthX() - length);
        }

        /// <summary>
        /// Append string value after caller value if caller value is not empty or null
        /// </summary>
        /// <param name="caller">String valuem, which need to check and value should stuff after</param>
        /// <param name="StringToStuff">String value to stuff after caller string</param>
        /// <returns>
        /// Caller string + StringToStuff if caller is not null or empty, else empty string
        /// </returns>
        public static string StuffIfNotEmpty(this string caller, string StringToStuff)
        {
            //if caller is null or empty, returning empty string
            if (IsNullOrEmpty(caller))
            {
                return string.Empty;
            }
            return caller + StringToStuff;
        }

        /// <summary>
        /// Get substring from a string
        /// </summary>
        /// <param name="Value">The string from which substring generating</param>
        /// <param name="Start">The zero-based starting character position of a substring</param>
        /// <returns>A string object</returns>
        public static string SubstringX(this string Value, int Start)
        {
            //Handling Null
            //If Start length does not meet criteria
            if (Value == null || Value.Length < Start)
            {
                return null;
            }

            // else retun result
            return Value.Substring(Start);
        }

        /// <summary>
        /// Get substring from a string
        /// </summary>
        /// <param name="Value">The string from which substring generating</param>
        /// <param name="Start">The zero-based starting character position of a substring</param>
        /// <param name="Length">The number of characters in the substring</param>
        /// <returns>A string object</returns>
        public static string SubstringX(this string Value, int Start, int Length)
        {
            //Handling Null
            if (Value == null)
            {
                return null;
            }

            if (Length < 0)
            {
                Length = 0;
            }

            // Minimum length of string
            int minLenth = Length;

            //If start length does meet criteria
            if (Value.Length >= Start)
            {
                // Recomputing minlength of string to avoid exception
                if (Value.Length < Start + Length)
                {
                    minLenth = Value.Length - Start;
                }

                //Finally Getting the value
                return Value.Substring(Start, minLenth);
            }

            return null;
        }
        public static string GetFirstXCChar(this String Value, int MaxChar)
        {
            if (Value == null)
            {
                return null;
            }

            var tempValue = Value.SubstringX(0, MaxChar);

            if (tempValue.Length < Value.Length)
            {
                return tempValue + "...";
            }

            return tempValue;
        }
        /// <summary>
        /// Converts the value of the specified DateTime object to dd-MMM-yyyy string format
        /// </summary>
        /// <param name="dt">Date to represent as string</param>
        /// <returns>
        /// dd-MMM-yyyy string format
        /// </returns>
        public static string ToDdMmmYyyy(this DateTime dt, bool includeTime = false)
        {
            var frmt = "dd/MMM/yyyy";
            if (includeTime)
            {
                frmt += " HH:MM";
            }

            return dt.ToString(frmt);
        }

        /// <summary>
        /// Converts the value of the specified DateTime object to dd-MM-yyyy string format
        /// </summary>
        /// <param name="dt">Date to represent as string</param>
        /// <returns>
        /// dd-MM-yyyy string format
        /// </returns>
        public static string ToDdMmYyyy(this DateTime dt)
        {
            return dt.ToString(@"dd\/MM\/yyyy");
        }

        /// <summary>
        /// Converts the value of the specified DateTime object to dd-MM-yyyy string format
        /// </summary>
        /// <param name="dt">Date to represent as string</param>
        /// <returns>
        /// dd-MM-yyyy string format
        /// </returns>
        public static string ToDdMmYyyy(this DateTime? dt)
        {
            if (!dt.HasValue)
            {
                return null;
            }

            return ToDdMmYyyy(dt.Value);
        }

        /// <summary>
        /// Returns a string that represents specified object, if object is not null or DBNull return bull
        /// </summary>
        /// <param name="Value">Object to represent</param>
        /// <returns>
        /// A string that represents specified object, returns null if object is null or DBNull
        /// </returns>
        public static string ToStringX(this object Value)
        {
            // if object is null or db null then return null
            if (Value.IsNull())
            {
                return null;
            }

            return Value.ToString();
        }

        /// <summary>
        /// Returns a copy of this string converted to lowercase after trimming
        /// </summary>
        /// <param name="caller">String to check</param>
        /// <param name="ReturnEmptyStringIfNull">A boolean value indicates whether to return empty string if specified string is null</param>
        /// <returns>
        /// String.empty if string is null else the lowercase equivalent of the current string after trim
        /// </returns>
        public static string ToTrimLower(this string caller, bool ReturnEmptyStringIfNull = true)
        {
            if (caller == null)
            {
                return ReturnEmptyStringIfNull ? string.Empty : null;
            }

            return caller.Trim().ToLower();
        }

        /// <summary>
        /// Returns a copy of this string converted to uppercase after trimming
        /// </summary>
        /// <param name="caller">String to check</param>
        /// <param name="ReturnEmptyStringIfNull">A boolean value indicates whether to return empty string if specified string is null</param>
        /// <returns>
        /// String.empty if string is null else the uppercase equivalent of the current string after trim.
        /// </returns>
        public static string ToTrimUpper(this string caller, bool ReturnEmptyStringIfNull = true)
        {
            if (caller == null)
            {
                return ReturnEmptyStringIfNull ? string.Empty : null;
            }

            return caller.Trim().ToUpper();
        }

        /// <summary>
        /// First check if string is null or not then Removes all leading and trailing white-space characters from the current
        /// System.String object.
        /// </summary>
        /// <param name="obj">Object need to trim</param>
        /// <returns>
        /// The string that remains after all white-space characters are removed from
        /// the start and end of the current string.
        /// </returns>
        public static string TrimX(this string obj)
        {
            // if object is null then return null
            if (obj == null)
            {
                return obj;
            }

            return obj.Trim();
        }

        public static string ReplaceX(this string obj, string oldValue, string newValue)
        {
            if (obj == null)
            {
                return null;
            }

            return obj.Replace(oldValue, newValue);
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        public static string RemoveStartingPlus(this string StringtoClean)
        {
            if (StringtoClean == null)
            {
                return StringtoClean;
            }

            StringtoClean = StringtoClean.Trim();
            if (StringtoClean.StartsWith("+"))
            {
                StringtoClean = StringtoClean.Substring(1).Trim();
            }
            return StringtoClean;
        }

        public static string RemoveEndCharacter(this string StringtoClean, string charToFind = ",")
        {
            if (StringtoClean == null)
            {
                return StringtoClean;
            }

            if (StringtoClean.EndsWith(charToFind))
            {
                StringtoClean = StringtoClean.Substring(1).Trim();
            }
            return StringtoClean;
        }

        public static string Generate16CharUniqueString()
        {
            //return Guid.NewGuid().ToString().Replace("-", "").SubstringX(UniqueStringLength);
            return DateTime.Now.ToString("yyMMddHHmmssffff");
        }

        public static string GenerateCharUniqueString(int size = 16)
        {
            RandomNumberGenerator generator = RandomNumberGenerator.Create();
            byte[] tempPassword = new byte[size + 10];
            generator.GetNonZeroBytes(tempPassword);
            string temp = Convert.ToBase64String(tempPassword);
            return temp.Replace("/", string.Empty).Replace("+", string.Empty).Replace("=", string.Empty).Substring(0, size).ToUpper();
        }

        public static int IndexX(this string Value, string Pattern, int NoOfOccureance, int Skip)
        {
            int FinalIndex = -1;
            int tempIndex = -1;

            for (int i = 0; i < NoOfOccureance; i++)
            {
                tempIndex = Value.IndexOf(Pattern);

                if (tempIndex == 0)
                {
                    Value = Value.Substring(1);
                    i--;
                    continue;
                }

                if (Skip != 0)
                {
                    Skip--;

                    if (Pattern == "<option")
                    {
                        i--;
                        tempIndex = Value.IndexOf("</option>");
                    }

                    tempIndex += "</option>".Length;
                    FinalIndex += tempIndex;
                    Value = Value.Substring(tempIndex);
                    FinalIndex += Extended.TrimAndGetLengthGap(ref Value);
                    continue;
                }

                if (tempIndex == -1)
                {
                    return FinalIndex;
                }

                if (Pattern == "</option>")
                {
                    tempIndex += "</option>".Length;
                }
                FinalIndex += tempIndex;
                Value = Value.Substring(tempIndex);
                FinalIndex += Extended.TrimAndGetLengthGap(ref Value);
            }

            return FinalIndex;
        }

        public static int TrimAndGetLengthGap(ref string Value)
        {
            int BeforeTrim = Value.Length;
            Value = Value.Trim();
            return BeforeTrim - Value.Length;
        }

        #endregion String Entend functions

        #region Decimal/Number

        /// <summary>
        /// Return minimum value of a sequence
        /// </summary>
        /// <param name="Caller">The first value with which to compare</param>
        /// <param name="Others">remaining values of list</param>
        /// <returns>Minimum value of a sequence</returns>
        public static Int32 GetMin(this int Caller, params Int32[] Others)
        {
            int Minvalue = Caller;

            foreach (int item in Others)
            {
                Minvalue = Minvalue < item ? Minvalue : item;
            }

            return Minvalue;
        }

        /// <summary>
        /// Return a true/false value indicates whether integer value is Zero or not
        /// </summary>
        /// <param name="caller">Value to check</param>
        /// <returns>
        /// True if value is zero; Otherwise false
        /// </returns>
        public static bool IsZero(this int caller)
        {
            return caller == 0;
        }

        public static string GetTwoDigitDay(int Day)
        {
            //Making day string like "1" to "01" patten
            var tempDayStr = "0" + Day.ToString();
            return tempDayStr.Substring(tempDayStr.Length - 2, 2);
        }

        /// <summary>
        /// Return a true/false value indicates whether integer value is Zero or not
        /// </summary>
        /// <param name="caller">Value to check</param>
        /// <returns>
        /// True if value is zero; Otherwise false
        /// </returns>
        public static bool IsZero(this long caller)
        {
            return caller == 0;
        }

        /// <summary>
        /// Return a true/false value indicates whether integer value is Zero or not
        /// </summary>
        /// <param name="caller">Value to check</param>
        /// <returns>
        /// True if value is zero; Otherwise false
        /// </returns>
        public static bool IsZero(this decimal caller)
        {
            return caller == 0;
        }
        /// <summary>
        /// Return a true/false value indicates whether integer value is Null or Zero or not
        /// </summary>
        /// <param name="caller">Value to check</param>
        /// <returns>
        /// True if value is null or zero; Otherwise false
        /// </returns>
        public static bool IsNullOrZero(this decimal caller)
        {
            return caller.IsNull() || caller.IsZero();
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation with two precision
        /// </summary>
        /// <param name="DeciamlNo">value to convert into string</param>
        /// <returns>
        /// The string representation of the value of this instance as specified by format
        /// </returns>
        public static string ToStringN(this decimal DeciamlNo)
        {
            return DeciamlNo.ToString("N").Replace(",", "");
        }

        public static string ToStringN(this decimal DeciamlNo, int Precision)
        {
            return DeciamlNo.ToString("N" + Precision).Replace(",", "");
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation with two precision
        /// </summary>
        /// <param name="DeciamlNo">value to convert into string</param>
        /// <returns>
        /// The string representation of the value of this instance as specified by format
        /// </returns>
        public static string ToStringN2(this decimal DeciamlNo)
        {
            return DeciamlNo.ToString("N2").Replace(",", "");
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation with two precision
        /// </summary>
        /// <param name="DeciamlNo">value to convert into string</param>
        /// <returns>
        /// The string representation of the value of this instance as specified by format
        /// </returns>
        public static string ToStringN2(this decimal? DeciamlNo)
        {
            if (!DeciamlNo.HasValue)
            {
                return null;
            }

            return ToStringN2(DeciamlNo.Value);
        }

        /// <summary>
        /// Converts the numeric value of this instance to its equivalent string representation with three precision
        /// </summary>
        /// <param name="DeciamlNo">value to convert into string</param>
        /// <returns>
        /// The string representation of the value of this instance as specified by format
        /// </returns>
        public static string ToStringN3(this decimal DeciamlNo)
        {
            return DeciamlNo.ToString("N3");
        }

        public static string NumbersToWords(long inputNumber)
        {
            long inputNo = inputNumber;
            if (inputNo == 0)
                return "Zero";
            long[] numbers = new long[4];
            long first = 0;
            long u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }
            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };
            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs
            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (long i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("and ");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            var final = sb.ToString().TrimEnd();
            if (final != null && final.StartsWith("and "))
            {
                return final.Substring("and ".Length);
            }
            return final;
        }

        public static decimal? GetRoundOff(decimal? amount)
        {
            if (!amount.HasValue)
            {
                return null;
            }
            return Math.Round(amount.Value, 2);

        }


        //end function

        #endregion Decimal/Number

        #region Encrypt/Decrypt functions

        /// <summary>
        /// A arbitary string const used in
        /// </summary>
        private const string StaticHash = @"ABC4FfGH$Q@IJsdf$QMsf55NQWE243LQWRf!WIOR#";

#if ByPassBuild
        private const string EncryptSpaceReplacer = "╥";
#endif

        /// <summary>
        /// Decrypt a string using dual encryption method. Return a decrypted string
        /// </summary>
        /// <param name="StringToDecrypt">Encrypted string need to decrypt</param>
        /// <param name="EncryptHash">Hash used to encrypt the string, default is static hash</param>
        /// <returns>The Decrypted string object</returns>
        public static string Decrypt(this string StringToDecrypt, string EncryptHash = null)
        {
            if (StringToDecrypt.IsNullOrEmpty())
            {
                return StringToDecrypt;
            }

            if (EncryptHash.IsNullOrEmpty())
            {
                EncryptHash = StaticHash;
            }

#if ByPassBuild
            EncryptHash = StaticHash;
#endif
            StringToDecrypt = StringToDecrypt.Replace(" ", "+");

            //
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(StringToDecrypt);

            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(EncryptHash));
            hashmd5.Clear();

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        /// <summary>
        /// Encrypt an object using dual encryption method. Return a encrypted cipher Text
        /// </summary>
        /// <param name="ObjToEncrypt">object to be encrypted</param>
        /// <param name="EncryptHash">Hash will use to ecrypt the string,default is static hash</param>
        /// <returns>
        /// Encrypted string
        /// </returns>
        public static string Encrypt(this object ObjToEncrypt, string EncryptHash = null)
        {
            if (EncryptHash.IsNullOrEmpty())
            {
                EncryptHash = StaticHash;
            }

            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(ObjToEncrypt.ToStringX());

            //keyArray = UTF8Encoding.UTF8.GetBytes(APSession.EncryptHash);
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(EncryptHash));
            hashmd5.Clear();
            //
            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;
            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            var EncryptedString = Convert.ToBase64String(resultArray, 0, resultArray.Length);

            return EncryptedString;
        }

        /// <summary>
        /// Try to decrypt a string using dual encryption method with in try and catch block, exception will be consumed. Return decrypted string
        /// </summary>
        /// <param name="StringToDecrypt">Encrypted string need to decrypt</param>
        /// <param name="EncryptHash">Hash used to ecrypt the string, default static hash</param>
        /// <returns>A string object</returns>
        public static string TryDecrypt(this string StringToDecrypt, string EncryptHash = null)
        {
            try
            {
                return Decrypt(StringToDecrypt, EncryptHash);
            }
            catch
            {
                return null;
            }
        }

        #endregion Encrypt/Decrypt functions

        #region Reflection

        /// <summary>
        /// Convert an object to provided type
        /// </summary>
        /// <typeparam name="TResult">The result type expected</typeparam>
        /// <param name="Value">Object to convert</param>
        /// <returns>
        /// Spefied return type object
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        //public static TResult CConvert<TResult>(object Value)
        //{
        //    //Getting object type
        //    Type ResultType = typeof(TResult);

        //    //Handling All NullableType At Once
        //    //It will return a null value only if Return Type is nullable and value is null
        //    if (Value.IsNull() && ResultType.IsNullableType())
        //    {
        //        return default(TResult);
        //    }

        //    //Convert from Not Nullable T nullable may Cause Problem so Choosing according to
        //    Type ConvertType = ResultType.GetCoreType();

        //    //If value is not of type nullable then let it convert to specified type
        //    if (ConvertType == typeof(Int32))
        //    {
        //        return (TResult)Convert.ChangeType(ToInt(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(String))
        //    {
        //        return (TResult)Convert.ChangeType(ToStringX(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Boolean))
        //    {
        //        return (TResult)Convert.ChangeType(ToBool(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Double))
        //    {
        //        return (TResult)Convert.ChangeType(ToDouble(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Decimal))
        //    {
        //        return (TResult)Convert.ChangeType(ToDecimal(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Int64))
        //    {
        //        return (TResult)Convert.ChangeType(ToLong(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Int16))
        //    {
        //        return (TResult)Convert.ChangeType(ToSingle(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Single))
        //    {
        //        return (TResult)Convert.ChangeType(ToSingle(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(SByte))
        //    {
        //        return (TResult)Convert.ChangeType(ToSingle(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(UInt32))
        //    {
        //        return (TResult)Convert.ChangeType(ToSingle(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Byte))
        //    {
        //        return (TResult)Convert.ChangeType(ToSingle(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(Guid))
        //    {
        //        return (TResult)Convert.ChangeType(ToGuid(Value), ConvertType);
        //    }
        //    else if (ConvertType == typeof(DateTime))
        //    {
        //        return (TResult)Convert.ChangeType(ToDate(Value), ConvertType);
        //    }
        //    else if (ConvertType.IsEnum)
        //    {
        //        return (TResult)Convert.ChangeType(ToEnum<TResult>(Value), ConvertType);
        //    }
        //    else
        //    {
        //        throw new NotImplementedException();
        //    }
        //}


        /// <summary>
        /// Convert an object to provided type
        /// </summary>
        /// <typeparam name="TResult">The result type expected</typeparam>
        /// <param name="Value">Object to convert</param>
        /// <returns>
        /// Spefied return type object
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static object CConvert(object Value, Type ResultType)
        {
            //Handling All NullableType At Once
            //It will return a null value only if Return Type is nullable and value is null
            if (Value.IsNull() && (ResultType.IsNullableType() || ResultType.IsGenericType))
            {
                return null;
            }

            //Convert from Not Nullable T nullable may Cause Problem so Choosing according to
            Type ConvertType = ResultType.GetCoreType();

            //If value is not of type nullable then let it convert to specified type
            if (ConvertType == typeof(Int32))
            {
                return Convert.ChangeType(ToInt(Value), ConvertType);
            }
            else if (ConvertType == typeof(String))
            {
                return Convert.ChangeType(ToStringX(Value), ConvertType);
            }
            else if (ConvertType == typeof(Boolean))
            {
                return Convert.ChangeType(ToBool(Value), ConvertType);
            }
            else if (ConvertType == typeof(Double))
            {
                return Convert.ChangeType(ToDouble(Value), ConvertType);
            }
            else if (ConvertType == typeof(Decimal))
            {
                return Convert.ChangeType(ToDecimal(Value), ConvertType);
            }
            else if (ConvertType == typeof(Int64))
            {
                return Convert.ChangeType(ToLong(Value), ConvertType);
            }
            else if (ConvertType == typeof(Int16))
            {
                return Convert.ChangeType(ToSingle(Value), ConvertType);
            }
            else if (ConvertType == typeof(Single))
            {
                return Convert.ChangeType(ToSingle(Value), ConvertType);
            }
            else if (ConvertType == typeof(SByte))
            {
                return Convert.ChangeType(ToSingle(Value), ConvertType);
            }
            else if (ConvertType == typeof(UInt32))
            {
                return Convert.ChangeType(ToSingle(Value), ConvertType);
            }
            else if (ConvertType == typeof(Byte))
            {
                return Convert.ChangeType(ToSingle(Value), ConvertType);
            }
            else if (ConvertType == typeof(Guid))
            {
                return Convert.ChangeType(ToGuid(Value), ConvertType);
            }
            else if (ConvertType == typeof(DateTime))
            {
                DateTime valueConverted;
                if (Value.ToString().Contains("GMT"))
                {
                    valueConverted = DateTime.ParseExact(Value.ToString().Substring(0, 24),
                  "ddd MMM dd yyyy HH:mm:ss",
                  CultureInfo.InvariantCulture);
                }
                else
                {
                    valueConverted = ToDate(Value);
                }

                return Convert.ChangeType(valueConverted, ConvertType);
            }
            else if (ConvertType == typeof(List<>) || ConvertType.IsGenericType)
            {
                Type myListElementType = ConvertType.GetGenericArguments().Single();

                var listType = typeof(List<>).MakeGenericType(myListElementType);

                var list = JsonConvert.DeserializeObject(Value.ToString(), listType);
                return list;
            }
            else if (ConvertType.IsEnum)
            {
                object value = null;
                try
                {
                    //value = Enum.Parse(ConvertType, Value.ToString());

                    string tempValue = Value.ToString().ToLower();
                    var enumValues = Enum.GetValues(ConvertType);

                    foreach (var item in enumValues)
                    {
                        var strValue = item.ToString().ToLower();
                        var intValue = ((int)item).ToString().ToLower();
                        if (strValue == tempValue || intValue == tempValue)
                        {
                            value = item;
                        }
                    }

                    return value;

                }
                catch
                {
                    return null;
                }

                return Convert.ChangeType(value, ConvertType);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Cast an object to another type object
        /// </summary>
        /// <typeparam name="T">Type expected as result type</typeparam>
        /// <param name="Value">Object to cast</param>
        /// <returns>
        /// Reult type object if cast is valid; otherwise null
        /// </returns>
        public static T DirectCast<T>(object Value) where T : class
        {
            if (Value.IsNull())
            {
                return null;
            }

            return (T)Value;
        }

        /// <summary>
        /// Get underlying core type of a nullable data type
        /// </summary>
        /// <param name="Value">The value.</param>
        /// <returns>A Type object</returns>
        public static Type GetCoreType(this Type Value)
        {
            Type u = Nullable.GetUnderlyingType(Value);
            return u ?? Value;
        }

        /// <summary>
        /// Get a boolean value indicates whether provided type is defined in .net assemblies or not
        /// </summary>
        /// <param name="Value">Type need to verify</param>
        /// <returns>
        /// True if class defined in .net assemblies; Otherwise false
        /// </returns>
        public static bool IsCustomClass(this Type Value)
        {
            return !Value.Namespace.StartsWith("System");
        }

        /// <summary>
        /// Get a boolean value indicates whether provided type is nullable type or not
        /// </summary>
        /// <param name="Value">Type need to verify</param>
        /// <returns>
        /// True if provided type is nullable type; otherwise false
        /// </returns>
        public static bool IsNullableType(this Type Value)
        {
            return (Value.IsGenericType && Value.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        /// <summary>
        /// Convert a list of generic class objects to datatable
        /// </summary>
        /// <typeparam name="T">The generic class type</typeparam>
        /// <param name="data">List of objects, need to convert</param>
        /// <returns>
        /// A datatable containing provided list of data
        /// </returns>
        public static System.Data.DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            var AllProperty = (typeof(T)).GetProperties().ToList();

            //var AllInsteadOf = InsteadOfProperty.Split('|').SkipWhile(i => string.IsNullOrEmpty(i));

            int propcount = AllProperty.Count();

            var table = new System.Data.DataTable();

            for (int i = 0; i < propcount; i++)
            {
                var prop = AllProperty[i];
                if (prop.CanRead)
                {
                    ////If Class level property need to exculde
                    //if(ExcludeClassLevelProperty && (prop.PropertyType.IsCustomClass()))
                    //{
                    //    continue;
                    //}

                    //If property type is Nullable<T> or It's a string type or it's a custom class theb
                    //column allowDBNull will be true
                    bool allowDBNull = false;

                    if ((prop.PropertyType.IsNullableType()) || (prop.PropertyType == typeof(String)) || (prop.PropertyType.IsCustomClass()))
                    {
                        allowDBNull = true;
                    }

                    table.Columns.Add(prop.Name, prop.PropertyType.GetCoreType()).AllowDBNull = allowDBNull;
                }
            }

            object[] values = new object[propcount];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    var prop = AllProperty[i];
                    if (prop.CanRead)
                    {
                        values[i] = prop.GetValue(item, null);
                    }
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static List<T> CopyListX<T>(this IEnumerable<T> data) where T : new()
        {
            List<T> tempList = new List<T>();

            foreach (T itemToCopy in data)
            {
                T itemAfterCopy = CopyX<T>(itemToCopy);
                tempList.Add(itemAfterCopy);
            }

            return tempList;
        }

        public static List<T> CopyListX<S, T>(this IEnumerable<S> data) where T : new()
        {
            List<T> tempList = new List<T>();

            foreach (S itemToCopy in data)
            {
                T itemAfterCopy = CopyX<S, T>(itemToCopy);
                tempList.Add(itemAfterCopy);
            }

            return tempList;
        }

        public static T CopyX<T>(T data) where T : new()
        {
            if (data == null)
            {
                return data;
            }

            var AllProperty = (typeof(T)).GetProperties().ToList();

            //var AllInsteadOf = InsteadOfProperty.Split('|').SkipWhile(i => string.IsNullOrEmpty(i));

            int propcount = AllProperty.Count();

            T newObject = new T();

            for (int i = 0; i < propcount; i++)
            {
                var prop = AllProperty[i];
                if (prop.CanWrite)
                {
                    if (!prop.PropertyType.IsCustomClass())
                    {
                        prop.SetValue(newObject, prop.GetValue(data, null));
                    }
                }
            }

            return newObject;
        }

        public static T CopyX<S, T>(S data) where T : new()
        {
            if (data == null)
            {
                return default(T);
            }

            var AllPropertyS = (typeof(S)).GetProperties().ToList();
            var AllPropertyT = (typeof(T)).GetProperties().ToList();

            //var AllInsteadOf = InsteadOfProperty.Split('|').SkipWhile(i => string.IsNullOrEmpty(i));

            int propcount = AllPropertyS.Count();

            T newObject = new T();

            for (int i = 0; i < propcount; i++)
            {
                var propS = AllPropertyS[i];
                var propT = AllPropertyT.Find(i1 => i1.Name == propS.Name);
                if (propS.CanRead && propT != null && propT.CanWrite)
                {
                    if ((propS.PropertyType.IsEnum || !propS.PropertyType.IsCustomClass()) && propS.PropertyType.GetCoreType() == propT.PropertyType.GetCoreType())
                    {
                        propT.SetValue(newObject, propS.GetValue(data, null));
                    }
                }
            }

            return newObject;
        }

        /// <summary>
        /// Returns a true/false value indicates whether specified type is basic .net class or struct
        /// <para>String, datetime, long, decimal, double are counted as basic class</para>
        /// </summary>
        /// <param name="t">The type on which query will execute</param>
        /// <returns>
        /// True if specified type is basic class in .net, otherwise false
        /// </returns>
        private static bool IsBasicType(Type t)
        {
            bool m_return = false;
            if (t.IsPrimitive || t == typeof(Decimal) || t == typeof(String) || t == typeof(long) || t == typeof(Double) || t == typeof(DateTime))
            {
                m_return = true;
            }

            return m_return;
        }

        public static void SetPropertyValue<T>(T tempObj, string PropertyName, string PropertyValue)
        {
            var tempProperty = typeof(T).GetProperty(PropertyName);

            if (tempProperty == null)
            {
                return;
            }

            object tempPropertyValue = null;
            if (tempProperty.PropertyType.IsEnum)
            {
                tempPropertyValue = Enum.Parse(tempProperty.PropertyType, PropertyValue);
            }
            else
            {
                tempPropertyValue = Convert.ChangeType(PropertyValue, tempProperty.PropertyType);
            }

            tempProperty.SetValue(tempObj, tempPropertyValue);
        }

        public static void SetPropertyValue<T, TValue>(this T obj, Expression<Func<T, TValue>> expression, TValue value)
        {
            ParameterExpression valueParameterExpression = Expression.Parameter(typeof(TValue));
            Expression targetExpression = expression.Body is UnaryExpression ? ((UnaryExpression)expression.Body).Operand : expression.Body;

            var newValue = Expression.Parameter(expression.Body.Type);
            var assign = Expression.Lambda<Action<T, TValue>>
                        (
                            Expression.Assign(targetExpression, Expression.Convert(valueParameterExpression, targetExpression.Type)),
                            expression.Parameters.Single(),
                            valueParameterExpression
                        );

            assign.Compile().Invoke(obj, value);
        }

        #endregion Reflection

        #region ADO

        /// <summary>
        /// By treating first row of readear as column info, return an array of string contains all column of reader.
        /// </summary>
        /// <param name="reader">Datareader, of which schema information need to extract</param>
        /// <returns>A string array object</returns>
        public static string[] GetSchemaTableFromReaderRow(this IDataReader reader)
        {
            string[] tempSchemaTable = new string[reader.FieldCount];

            for (int i = 0; i < reader.FieldCount; i++)
            {
                tempSchemaTable[i] = reader[i].ToStringX().ToTrimUpper();
            }

            return tempSchemaTable;
        }

        /// <summary>
        /// Return a boolean value indicates whether specified reader contains mentioned columnName.
        /// </summary>
        /// <param name="reader">Reader contains the schema table</param>
        /// <param name="TableSchema">An array of string contains reader schema defination</param>
        /// <param name="columnName">Name of the column, which need to found</param>
        /// <returns>
        /// true if column name found with in tableschema defination; Othwersise false.
        /// </returns>
        public static Boolean IsColumnExists(this IDataReader reader, string[] TableSchema, string columnName)
        {
            //Active one is more faster that commented one
            //return TableSchema.Contains(columnName);
            return Array.IndexOf<string>(TableSchema, columnName.ToUpper()) != -1;
            //return Array.BinarySearch(TableSchema, columnName.ToUpper()) > -1;
        }

        /// <summary>
        /// Return a boolean value indicates whether specified reader contains mentioned columnName.
        /// </summary>
        /// <param name="dr">The dr.</param>
        /// <param name="columnName">Name of the column, which need to found</param>
        /// <returns>
        /// true if column name found with in tableschema of datareader; Othwersise false.
        /// </returns>
        public static bool IsColumnExists(this IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Get value from datareader, if datareader contain specified column and then convert dbvalue by parameterized function the return result
        /// </summary>
        /// <typeparam name="T">Expected result type</typeparam>
        /// <param name="reader">Datareader that contain data</param>
        /// <param name="TableSchema">Table schema of datareader</param>
        /// <param name="objfunction">Function by which dbvalue to convert</param>
        /// /// <param name="columnName">Column name to found in reader</param>
        /// <returns>
        /// Converted value if datareader contain column; Otherwise default of result type
        /// </returns>
        public static T IsColumnExists<T>(this IDataReader reader, string[] TableSchema, string columnName, Func<object, T> objfunction)
        {
            //Finding column index in reader
            int readerIndex = Array.IndexOf<string>(TableSchema, columnName.ToUpper());

            //If any column found then convert result
            if (readerIndex > -1 && reader[readerIndex] != DBNull.Value)
            {
                return objfunction(reader[readerIndex]);
            }

            return default(T);
        }

        /// <summary>
        /// Get value from datareader, if datareader contain specified column and then convert dbvalue by parameterized function the return result
        /// </summary>
        /// <typeparam name="T">Expected result type</typeparam>
        /// <param name="reader">Datareader that contain data</param>
        /// <param name="TableSchema">Table schema of datareader</param>
        /// /// <param name="objfunction">Function by which dbvalue to convert</param>
        /// <param name="columnNames">Column names to found in reader. First column will get higheer preference</param>
        /// <returns>
        /// Converted value if datareader contain column; Otherwise default of result type
        /// </returns>
        public static T IsColumnExists<T>(this IDataReader reader, string[] TableSchema, Func<object, T> objfunction, params string[] columnNames)
        {
            //
            int readerIndex = -1;
            for (int i = 0; i < columnNames.Length; i++)
            {
                //Finding column index in reader
                readerIndex = Array.IndexOf<string>(TableSchema, columnNames[i].ToUpper());

                //If any column found then convert result and return, no further execution will happen
                if (readerIndex > -1 && reader[readerIndex] != DBNull.Value)
                {
                    return objfunction(reader[readerIndex]);
                }
            }

            return default(T);
        }

        #endregion ADO

        #region Enum

        private static string GetDescription<T>(this Enum value) where T : DescriptionAttribute
        {
            T attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(T), false)
                .SingleOrDefault() as T;
            return attribute == null ? value.ToString() : attribute.Description;
        }

        /// <summary>
        /// Returns description of an enum element.If Description not set then returns enum name as string.
        /// </summary>
        /// <param name="value">The enum element</param>
        /// <returns>
        /// Description string of Enum
        /// </returns>
        public static string GetDescription(this Enum value)
        {
            return GetDescription<DescriptionAttribute>(value);
        }

        public static T GetEnumFromDescription<T>(this string description) where T : Enum
        {
            foreach (var field in typeof(T).GetFields())
            {
                if (Attribute.GetCustomAttribute(field,
                typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", nameof(description));
            // Or return default(T);
        }

        /// <summary>
        /// Gets numeric represtion of an array of enum as comma separated string.
        /// </summary>
        /// <param name="AllStatus">Param array of enums need to represent as comma separated</param>
        /// <returns>
        /// String contains all numeric representation of enums array
        /// </returns>
        public static string GetStatus(params Enum[] AllStatus)
        {
            StringBuilder sb = new StringBuilder();
            //creating the Status filed
            for (int i = 0; i < AllStatus.Length; i++)
            {
                Enum Temp = AllStatus[i];
                int b = Temp.ToInt();
                sb.Append(b.ToStringX() + ",");
            }
            string ReturnStatus = sb.ToString();
            //Removing last ',' symbol if required
            if (!ReturnStatus.IsNullOrEmpty())
            {
                ReturnStatus = ReturnStatus.Substring(0, ReturnStatus.Length - 1);
            }

            return ReturnStatus;
        }

        /// <summary>
        /// Get a list of EnumDataBind class objects, contains specified enum mermbers data
        /// </summary>
        /// <param name="enm">Type of Enum</param>
        /// <returns>
        /// List of EnumDataBind objects, contains enum member
        /// </returns>
        //public static List<DdlDataBinder> GetToDataBind(this Enum enm)
        //{
        //    var AllValues = Enum.GetValues(enm.GetType());

        //    List<DdlDataBinder> returnAble = new List<DdlDataBinder>();

        //    foreach (var item in AllValues)
        //    {
        //        DdlDataBinder temp = new DdlDataBinder();
        //        temp.Value = item.ToInt().ToString();
        //        temp.ActualText = item.ToStringX();
        //        temp.Display = GetDescription((Enum)item);

        //        returnAble.Add(temp);
        //    }
        //    return returnAble;
        //}

        //public static List<DdlDataBinder> GetToDataBindByMember(params Enum[] enmMember)
        //{
        //    List<DdlDataBinder> returnAble = new List<DdlDataBinder>();

        //    foreach (var item in enmMember)
        //    {
        //        DdlDataBinder temp = new DdlDataBinder();
        //        temp.Value = item.ToInt().ToString();
        //        temp.ActualText = item.ToStringX();
        //        temp.Display = GetDescription((Enum)item);

        //        returnAble.Add(temp);
        //    }
        //    return returnAble;
        //}

        /// <summary>
        /// A boolean indicates whether enum mermber have obsolete attribute or not.
        /// </summary>
        /// <param name="value">The enum member to check</param>
        /// <returns>
        /// True if enum merber is obsolete; Otherwise false
        /// </returns>
        public static bool IsObsolete(this Enum value)
        {
            ObsoleteAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(ObsoleteAttribute), false)
                .SingleOrDefault() as ObsoleteAttribute;
            return attribute != null;
        }

        public static string ListEnumToString<E>(IEnumerable<E> value, bool ConvertValueToInt = true)
        {
            if (value.IsEmptyCollection())
            {
                return null;
            }

            //Keep value in int format for enum
            if (ConvertValueToInt)
            {
                var intValueList = Array.ConvertAll(value.ToArray(), e => e.ToInt());

                return intValueList.Join(',');
            }

            return value.Join(',');
        }

        public static List<E> StringToEnumList<E>(string value) where E : struct
        {
            List<E> list = new List<E>();

            if (value.IsNullOrEmpty())
            {
                return list;
            }

            list = value.Split(',').Select(i => i.ToEnum<E>()).ToList();

            return list;
        }

        #endregion Enum

        #region Enumerable

        /// <summary>
        /// Gets the number of elements contained in the collections, return zero if collection is null
        /// </summary>
        /// <typeparam name="T">Type of collection</typeparam>
        /// <param name="collection">Collection of which elements needs to count</param>
        /// <returns>
        /// The number of elements contained in the collections, return zero if collection is null
        /// </returns>
        public static int CountX<T>(this T collection) where T : ICollection, IList, IEnumerable
        {
            return (collection == null) ? 0 : collection.Count;
        }

        /// <summary>
        /// Return a generic list of T after filtered by ValueToCompare
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="Filterable">Generic list on which filtering will be done</param>
        /// <param name="PropertyName">property or field of type specified with which value need to compare</param>
        /// <param name="ValueToCompare">Value, which need to compare</param>
        /// <returns>
        /// Filtered generic list of object
        /// </returns>
        public static List<T> FilterEqual<T>(this List<T> Filterable, string PropertyName, object ValueToCompare)
        {
            ConstantExpression c = Expression.Constant(ValueToCompare);
            ParameterExpression p = Expression.Parameter(typeof(T), "xx");
            MemberExpression m = Expression.PropertyOrField(p, PropertyName);
            var Lambda = Expression.Lambda<Func<T, Boolean>>(Expression.Equal(c, m), new ParameterExpression[] { p });
            Func<T, Boolean> func = Lambda.Compile();
            return Filterable.Where(func).ToList();
        }

        /// <summary>
        /// Get property name from Member/ Unary expression
        /// </summary>
        /// <typeparam name="T">Type of which member parsed</typeparam>
        /// <typeparam name="U">Type of member it self</typeparam>
        /// <param name="expression">the expression</param>
        /// <returns>name of the member</returns>
        public static string GetPropertyName<T, U>(Expression<Func<T, U>> expression)
        {
            MemberExpression body = expression.Body as MemberExpression;
            // if expression is not a member expression
            if (body == null)
            {
                UnaryExpression ubody = (UnaryExpression)expression.Body;
                body = ubody.Operand as MemberExpression;
            }
            return string.Join(".", body.ToString().Split('.').Skip(1));
        }

        /// <summary>
        /// Get properties name from Member/ Unary expression
        /// </summary>
        /// <typeparam name="T">Type of which member parsed</typeparam>
        /// <typeparam name="U">Type of member it self</typeparam>
        /// <param name="expression">the expression</param>
        /// <returns>name of the member</returns>
        public static string GetMultiplePropertyName<T>(params Expression<Func<T, object>>[] expressions)
        {
            string[] propertyNames = new string[expressions.Count()];
            for (int i = 0; i < propertyNames.Length; i++)
            {
                propertyNames[i] = GetPropertyName(expressions[i]);
            }

            return propertyNames.Join();
        }

        /// <summary>
        /// Returns the first element of a sequence, return null if sequence is null or empty
        /// </summary>
        /// <typeparam name="T">The type of caller</typeparam>
        /// <param name="caller">The IEnumerable caller.</param>
        /// <returns>
        /// The first element in the specified sequence if sequence is not empty or null; Otherwise null.
        /// </returns>
        public static T FirstOrNull<T>(this IEnumerable<T> caller) where T : class
        {
            var TempAll = caller == null ? null : caller.ToList();
            if (TempAll == null || TempAll.Count == 0)
            {
                return default(T);
            }
            else
            {
                return TempAll.First();
            }
        }

        /// <summary>
        /// Remove and place given item at the end
        /// </summary>
        /// <typeparam name="T">Type of the list item</typeparam>
        /// <param name="caller"></param>
        /// <param name="item">list item</param>
        public static void MoveToEnd<T>(this List<T> caller, T item) where T : class
        {
            if (caller == null || !caller.Any() || item == null)
            { return; }

            caller.Remove(item);
            caller.Add(item);
        }

        /// <summary>
        /// Returns the first element of a sequence, return null if sequence is null or empty
        /// </summary>
        /// <typeparam name="T">The type of caller</typeparam>
        /// <param name="caller">The IEnumerable caller.</param>
        /// <returns>
        /// The first element in the specified sequence if sequence is not empty or null; Otherwise null.
        /// </returns>
        public static Nullable<T> FirstOrNullN<T>(this IEnumerable<T> caller) where T : struct
        {
            var TempAll = caller == null ? null : caller.ToList();
            if (TempAll == null || TempAll.Count == 0)
            {
                return null;
            }
            else
            {
                return TempAll.First();
            }
        }

        /// <summary>
        /// Compares that current object value fall between from object value and to object value of the same type
        /// </summary>
        /// <typeparam name="T">The type of object</typeparam>
        /// <param name="caller">Current object, with which comparision is doing</param>
        /// <param name="from">The lower boundary</param>
        /// <param name="to">The upper boundary</param>
        /// <returns>
        /// True if caller value fall between lower and upper boundaries
        /// </returns>
        public static bool IsBetween<T>(this T caller, T from, T to) where T : IComparable<T>
        {
            return caller.CompareTo(from) >= 0 && caller.CompareTo(to) <= 0;
        }

        /// <summary>
        /// Return a boolean value indicates whether provided collection is null or having no elements
        /// </summary>
        /// <typeparam name="T">Type of collection</typeparam>
        /// <param name="collection">Collection to verify</param>
        /// <returns>
        /// True if collection is null or contains no elements; otherwise false
        /// </returns>
        public static bool IsEmptyCollection<T>(this T collection) where T : IEnumerable, ICollection, IList
        {
            return collection == null || collection.Count == 0;
        }

        /// <summary>
        /// Return a boolean value indicates whether provided collection is null or having no elements
        /// </summary>
        /// <typeparam name="T">Type of collection</typeparam>
        /// <param name="collection">Collection to verify</param>
        /// <returns>
        /// True if collection is null or contains no elements; otherwise false
        /// </returns>
        public static bool IsNotEmptyCollection<T>(this T collection) where T : IEnumerable, ICollection, IList
        {
            return !(collection == null || collection.Count == 0);
        }

        /// <summary>
        /// Return a boolean value indicates whether provided IEnumerable collection is null or having no elements
        /// </summary>
        /// <typeparam name="T">Type of IEnumerable collection</typeparam>
        /// <param name="collection">IEnumerable Collection to verify</param>
        /// <returns>
        /// True if IEnumerable collection is null or contains no elements; otherwise false
        /// </returns>
        public static bool IsEmptyCollection<T>(this IEnumerable<T> collection)
        {
            return collection == null || collection.Count() == 0;
        }

        /// <summary>
        /// Concatenates all the elements of a string array, using the specified separator  between each element.
        /// </summary>
        /// <param name="delimeter">The char to use as a separator.</param>
        /// <param name="value">An array that contains the elements to concatenate.</param>
        /// <returns>
        /// A string that consists of the elements in value delimited by the separator
        /// string. If value is an empty array, the method returns System.String.Empty
        /// </returns>
        public static string Join(char delimeter, params string[] value)
        {
            return Join<string>(value, delimeter);
        }

        /// <summary>
        /// Concatenates all the elements of a string array, using the specified separator  between each element.
        /// </summary>
        /// <typeparam name="T">The type of the members of values.</typeparam>
        /// <param name="value">An array that contains the elements to concatenate.</param>
        /// <param name="delimeter">The char to use as a separator.</param>
        /// <returns>
        /// A string that consists of the elements in value delimited by the separator
        /// string. If value is an empty array, the method returns System.String.Empty
        /// </returns>
        public static string Join<T>(this IEnumerable<T> value, char delimeter = ',')
        {
            if (value.IsNull())
            {
                return null;
            }

            StringBuilder sb = new StringBuilder();
            foreach (T item in value)
            {
                sb.Append(item.ToString() + delimeter);
            }

            string TempString = sb.ToString();

            //Removing end symbol
            if (TempString.Length > 0)
            {
                TempString = TempString.SubstringX(0, TempString.Length - 1);
            }

            return TempString;
        }

        /// <summary>
        /// Concatenates all the elements of a enum array's int value, using the specified separator.
        /// </summary>
        /// <typeparam name="T">The type of enum</typeparam>
        /// <param name="AllStatus">An array of enum members</param>
        /// <returns>
        /// A string that consists of the elements in value delimited by the separator
        /// string. If value is an empty array, the method returns System.String.Empty
        /// </returns>
        public static string JoinEnumIntValue<T>(this IEnumerable<T> AllStatus) where T : struct, IConvertible
        {
            if (AllStatus == null)
            {
                return null;
            }

            var tempAllStatus = Array.ConvertAll(AllStatus.ToArray(), item => ToInt(item));

            StringBuilder sb = new StringBuilder();

            //creating the Status filed
            for (int i = 0; i < tempAllStatus.Length; i++)
            {
                sb.Append(tempAllStatus[i].ToStringX() + ',');
            }

            string ReturnStatus = sb.ToString();

            //Removing last ',' symbol if required
            if (!ReturnStatus.IsNullOrEmpty())
            {
                ReturnStatus = ReturnStatus.Substring(0, ReturnStatus.Length - 1);
            }

            return ReturnStatus;
        }

        /// <summary>
        /// Represent params elements as array
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="Collection">Param elements</param>
        /// <returns>
        /// Param elements as an array
        /// </returns>
        public static T[] ToArray<T>(params T[] Collection)
        {
            return Collection;
        }

        /// <summary>
        /// Return a list of object for the same type.
        /// <para>And then add refering object as first element of this list</para>
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="SingleObject">First element of list</param>
        /// <returns>
        /// System.Collections.Generic.List objects containing caller as first element
        /// </returns>
        public static List<T> ToListCollection<T>(T SingleObject)
        {
            List<T> Returnable = new List<T>();
            Returnable.Add(SingleObject);
            return Returnable;
        }

        /// <summary>
        /// Creates a System.Collections.Generic.List<T> from an System.Collections.Generic.IEnumerable<T>, retun null if sequence is null or empty
        /// </summary>
        /// <typeparam name="T">The type of the elements of source</typeparam>
        /// <param name="caller">The System.Collections.Generic.IEnumerable<T> to return the first element of</param>
        /// <returns>A list of T type objects</returns>
        public static List<T> ToListX<T>(this IEnumerable<T> caller) where T : class
        {
            return (caller == null) ? null : caller.ToList();
        }

        #endregion Enumerable

        #region Extra

        /// <summary>
        /// Add an elemrnt to provided array and return a new array
        /// </summary>
        /// <typeparam name="T">The type of array</typeparam>
        /// <param name="Array">An array, to which new element is going to add</param>
        /// <param name="ElementToAdd">the element to add</param>
        /// <returns>
        /// An array after adding new element
        /// </returns>
        public static T[] AddElementToArray<T>(this T[] Array, T ElementToAdd)
        {
            int newLength = Array.Length + 1;

            T[] result = new T[newLength];

            for (int i = 0; i < newLength - 1; i++)
            {
                result[i] = Array[i];
            }
            result[newLength - 1] = ElementToAdd;

            return result;
        }

        /// <summary>
        /// Only applicable for serializable object
        /// Makes a copy from the object.
        /// Doesn't copy the reference memory, only data.
        /// </summary>
        /// <typeparam name="T">Type of the return object.</typeparam>
        /// <param name="item">Object to be copied.</param>
        /// <returns>
        /// Returns the copied object.
        /// </returns>
        public static T CloneForSerilizable<T>(T item)
        {
            if (item != null)
            {
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream stream = new MemoryStream();

                formatter.Serialize(stream, item);
                stream.Seek(0, SeekOrigin.Begin);

                T result = (T)formatter.Deserialize(stream);

                stream.Close();

                return result;
            }
            else
                return default(T);
        }

        //public static IList<T> Clone<T>(this IList<T> listToClone)
        //{
        //    return listToClone.Select(item => (T)item.Clone()).ToList();
        //}

        /// <summary>
        /// Gets the unique no for100 year.
        /// </summary>
        /// <returns>An unique string</returns>
        public static String GetUniqueNoFor100Year()
        {
            return DateTime.Now.ToString("yyXXXhhmmssff").Replace("XXX", DateTime.Now.DayOfYear.ToString());
        }

        /// <summary>
        /// Determines whether a sequence contains a specified element by using the default equality comparer
        /// </summary>
        /// <typeparam name="T">The type of the elements of source.</typeparam>
        /// <param name="obj">The value to locate in the sequence</param>
        /// <param name="args">A sequence in which to locate a value</param>
        /// <returns>
        /// True if the source sequence contains an element that has the specified value; otherwise, false
        /// </returns>
        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        /// <summary>
        /// Determines whether a sequence contains a specified element by using the default equality comparer
        /// </summary>
        /// <typeparam name="T">The type of the elements of source.</typeparam>
        /// <param name="obj">The value to locate in the sequence</param>
        /// <param name="args">A sequence in which to locate a value</param>
        /// <returns>
        /// False if the source sequence contains an element that has the specified value; otherwise, true
        /// </returns>
        public static bool NotIn<T>(this T obj, params T[] args)
        {
            return !In(obj, args);
        }

        /// <summary>
        /// Removes html tags from a text
        /// </summary>
        /// <param name="htmlText">string containing html tag</param>
        /// <returns>
        /// string with out html tag
        /// </returns>
        public static string ReplaceHtml(this string htmlText)
        {
            string noHTML = Regex.Replace(htmlText, @"<[^>]+>|&nbsp;", "").Trim();
            //removing concurrent spaces
            return Regex.Replace(noHTML, @"\s{2,}", " ");
        }

        /// <summary>
        /// Convert an datetime object to julian string format
        /// </summary>
        /// <param name="value">the datetime object</param>
        /// <returns>
        /// Julian date string represtation
        /// </returns>
        public static string ToJulian(this DateTime value)
        {
            return value.Year.ToStringX().Substring(2, 2) + value.DayOfYear;
        }

        /// <summary>
        /// Representation an object in xml format
        /// <para>Opeartes on serialization</para>
        /// </summary>
        /// <param name="oObject">The object need to format</param>
        /// <returns>
        /// An string contain the xml representaion
        /// </returns>
        public static string ToXML(object oObject)
        {
            using (StringWriter textWriter = new StringWriter())
            {
                XmlSerializer xmlSerializer = new XmlSerializer(oObject.GetType());
                xmlSerializer.Serialize(textWriter, oObject);
                return textWriter.ToString();
            }
        }

        #endregion Extra

        /// <summary>
        /// Culture used in various convert function
        /// </summary>
        /// <value>

        /// </value>
        public static CultureInfo CurrentCulture
        {
            get
            {
                var tempCulture = CultureInfo.CreateSpecificCulture("en-IN");
                return tempCulture;
            }
        }

        public static DateTime CurrentIndianTime
        {
            get
            {
                return new DateTime(DateTime.UtcNow.AddMinutes(330).Ticks, DateTimeKind.Unspecified);
                //return DateTime.UtcNow.AddMinutes(330);
                //DateTime utcTime = DateTime.UtcNow.ad;
                //TimeZoneInfo myZone = TimeZoneInfo.CreateCustomTimeZone("INDIA", new TimeSpan(+5, +30, 0), "India", "India");
                //DateTime custDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, myZone);
                //return custDateTime;
            }
        }

        /// <summary>
        /// Get local path from url
        /// </summary>
        /// <param name="URLPath">full url address</param>
        /// <returns>
        /// String contains local path of file
        /// </returns>
        public static string GetLocalPathFromURL(this string URLPath)
        {
            string tempImageLocalPath = URLPath;

            if (tempImageLocalPath.ToUpper().StartsWith("HTTP"))
            {
                Uri tempUri = new Uri(tempImageLocalPath);
                tempImageLocalPath = tempUri.LocalPath;
            }

            return tempImageLocalPath;
        }

        public static bool IsWeekEnd(this DateTime DateToValidate, bool ConsiderSaturdayAsHoliday = true)
        {
            if (DateToValidate.DayOfWeek == DayOfWeek.Saturday
                || DateToValidate.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }

            return false;
        }

        public static bool IsEqualDateX(this DateTime FirstDate, DateTime SeconDate)
        {
            if (FirstDate.Year == SeconDate.Year
                && FirstDate.Month == SeconDate.Month
                && FirstDate.Day == SeconDate.Day
                && FirstDate.Hour == SeconDate.Hour
                && FirstDate.Minute == SeconDate.Minute
                && FirstDate.Second == SeconDate.Second)
            {
                return true;
            }

            return false;
        }

        public static string ToMonthName(this int month)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(month);
        }



        public static byte[] ToByteArray(this Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        //public static List<DdlDataBinder> ToDataBindList<T>(this List<T> data, Func<T, DdlDataBinder> selector)
        //{
        //    var data2 = data.Select(selector).ToList();
        //    return data2;
        //}

        public static T CreateNew<T>(Type Controltype)
        {
            var func = Expression.Lambda<Func<T>>(Expression.New(Controltype)).Compile();
            return func.Invoke();
        }

        /// <summary>
        /// Creates a new instance of same type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        public static T CreateNew<T>(T control)
        {
            Type controlType = control.GetType();

            var Flag = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            object objectToReturn = null;
            int? ParameterCount = null;

            ConstructorInfo ci = ci = controlType.GetConstructors(Flag).FirstOrNull();
            ParameterCount = ci.GetParameters().CountX().GetNullIfZero();

            if (!ParameterCount.HasValue)
            {
                objectToReturn = ci.Invoke(null);
            }
            else
            {
                var allParameterInfo = ci.GetParameters();
                object[] parameters = new object[ParameterCount.Value];

                for (int i = 0; i < ParameterCount.Value; i++)
                {
                    ParameterInfo item = allParameterInfo[i];
                    //not fully right but working for the time being
                    //this fix has been implemented for System.Web.UI.ResourceBasedLiteralControl
                    parameters[i] = controlType.GetField("_" + item.Name, Flag).GetValue(control);
                }

                objectToReturn = ci.Invoke(parameters);
            }

            return (T)objectToReturn;
        }
    }

    public static class PasswordHelper
    {
        public static string GetPasswordSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[32];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        public static string EncodePassword(string password, string salt)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] src = Encoding.Unicode.GetBytes(salt);
            byte[] dst = new byte[src.Length + bytes.Length];
            Buffer.BlockCopy(src, 0, dst, 0, src.Length);
            Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);
            HashAlgorithm algorithm = HashAlgorithm.Create("SHA1");
            byte[] inarray = algorithm.ComputeHash(dst);
            return Convert.ToBase64String(inarray);
        }
    }

    public class NowX
    {
        public static string CurrentTime
        {
            get
            {
                return DateTime.Now.ToString("dd MM yyyy hh mm ss ffffff");
            }
        }

        public static void WriteCurrentTime()
        {
            Debug.WriteLine(CurrentTime);
        }
    }

    public class TwoProperty<T, S>
    {
        public T Item1 { get; set; }
        public S Item2 { get; set; }
    }

    public class ThreeProperty<T, S, U> : TwoProperty<T, S>
    {
        public U Item3 { get; set; }

    }
    public class FourProperty<T, S, U, V> : ThreeProperty<T, S, U>
    {
        public V Item4 { get; set; }

    }

    public class KSFileInfo
    {
        public string Name { get; set; }
        public string URL { get; set; }
    }
}
