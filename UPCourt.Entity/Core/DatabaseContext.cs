﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;
using UPCourt.Entity.Core.Converter;

namespace UPCourt.Entity
{
    public class DatabaseContext : DbContext
    {
        public virtual DbSet<Advocate> Advocates { get; set; }
        public virtual DbSet<EntityChangeTrakerLog> EntityChangeTrakerLogs { get; set; }
        public virtual DbSet<Designation> Designations { get; set; }
        public virtual DbSet<AdvocateCourt> AdvocateCourts { get; set; }
        public virtual DbSet<Court> Courts { get; set; }
        public virtual DbSet<CourtOpenCloseLog> CourtOpenCloseLogs { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<AGDepartment> AGDepartments { get; set; }
        public virtual DbSet<GovernmentDepartment> GovernmentDepartments { get; set; }
        public virtual DbSet<GovernmentDepartmentAddress> GovernmentDepartmentAddresses { get; set; }
        public virtual DbSet<GovernmentDepartmentContactNo> GovernmentDepartmentContactNos { get; set; }
        public virtual DbSet<PagePermission> PagePermissions { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserRolePagePermission> UserRolePagePermissions { get; set; }
        public virtual DbSet<UserLogInLog> UserLogInLogs { get; set; }
        public virtual DbSet<UserCase> UserCases { get; set; }
        public virtual DbSet<Notice> Notices { get; set; }
        public virtual DbSet<NoticeBasicIndex> NoticeBasicIndexs { get; set; }
        public virtual DbSet<PartyAndRespondent> PartyAndRespondents { get; set; }
        public virtual DbSet<NoticeCounselor> NoticeCounselors { get; set; }
        public virtual DbSet<Document> Documents { get; set; }
        public virtual DbSet<Petitioner> Petitioners { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Case> Cases { get; set; }
        public virtual DbSet<CaseHearing> CaseHearings { get; set; }
        public virtual DbSet<CaseStatus> CaseStatuses { get; set; }
        public virtual DbSet<UserCredentialRequest> UserCredentialRequests { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            optionsBuilder.UseMySQL(connectionString);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Case>()
              .HasMany(a => a.Parties)
              .WithOne(b => b.CaseParty)
             .HasForeignKey(e => e.CasePartyId);

            modelBuilder.Entity<Case>()
              .HasMany(a => a.Respondents)
              .WithOne(b => b.CaseRespondent)
             .HasForeignKey(e => e.CaseRespondentId);
            modelBuilder.Entity<Notice>()
              .HasMany(a => a.Parties)
              .WithOne(b => b.NoticeParty)
             .HasForeignKey(e => e.NoticePartyId);
            modelBuilder.Entity<Notice>()
              .HasMany(a => a.Respondents)
              .WithOne(b => b.NoticeRespondent)
             .HasForeignKey(e => e.NoticeRespondentId);

            var enumConverter = new EnumListToStringValueConverter<PagePermissionType>();

            modelBuilder.Entity<PagePermission>().Property(e => e.PermissionTypes).HasConversion(enumConverter);
            modelBuilder.Entity<UserRolePagePermission>().Property(e => e.PermittedPermissionTypes).HasConversion(enumConverter);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            //TODO: on add any entity add here the same
            foreach (var item in modelBuilder.Model.GetEntityTypes())
            {
                var p = item.FindPrimaryKey().Properties.FirstOrDefault(i => i.ValueGenerated != Microsoft.EntityFrameworkCore.Metadata.ValueGenerated.Never);
                if (p != null)
                {
                    p.ValueGenerated = Microsoft.EntityFrameworkCore.Metadata.ValueGenerated.Never;
                }

            }
        }

        public override int SaveChanges()
        {
            CheckEntityTracker();

            return base.SaveChanges();
        }

        private void CheckEntityTracker()
        {
            var changes = this.ChangeTracker.Entries().Where(i => i.State != EntityState.Unchanged).ToList();
            var VirtualSessionId = Guid.NewGuid();
            var currentTimeStamp = Extended.CurrentIndianTime;

            foreach (var item in changes)
            {
                var tempETA = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerTableAttribute)) as EntityChangeTrackerTableAttribute;

                if (tempETA == null || !tempETA.EnableChangeLog)
                {
                    continue;
                }

                var tempParentKeyAtt = item.Entity.GetType().GetCustomAttribute(typeof(EntityChangeTrackerParentKeyAttribute)) as EntityChangeTrackerParentKeyAttribute;
                //var d = item.Entity;
                //var e = item.CurrentValues;
                //var s = item.State;

                //var keyName = item.Metadata.FindPrimaryKey().Properties.Select(x => x.Name).Single();
                //var primarykeyValue = item.Entity.GetType().GetProperty(keyName).GetValue(item.Entity, null);

                //var settings = new Newtonsoft.Json.JsonSerializerSettings
                //{
                //    MaxDepth = 1,
                //    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                //};

                //var formatedData = SerializeObject(item.Entity, 1);

                //var KeyValue2 = item.Properties.Where(p => p.Metadata.IsPrimaryKey()).Single().CurrentValue;
                //var NewValues2 = item.Properties.Where(p => item.State == EntityState.Added || item.State == EntityState.Modified).ToDictionary(p => p.Metadata.Name, p => p.CurrentValue).NullIfEmpty();
                //var OldValues = JsonConvert.SerializeObject(item.Properties.Where(p => item.State == EntityState.Deleted || item.State == EntityState.Modified).ToDictionary(p => p.Metadata.Name, p => p.OriginalValue).NullIfEmpty());


                Dictionary<string, object> KeyValues = new Dictionary<string, object>();
                Dictionary<string, object> CurrentValues = new Dictionary<string, object>();
                Dictionary<string, object> deltaChanges = new Dictionary<string, object>();
                string displayPropValue = null;
                string userNamePropValue = null;
                Dictionary<string, object> parentParentKeyValues = new Dictionary<string, object>();

                foreach (var property in item.Properties)
                {
                    // The following condition is ok with EF Core 2.2 onwards.
                    // If you are using EF Core 2.1, you may need to change the following condition to support navigation properties: https://github.com/dotnet/efcore/issues/17700
                    // if (property.IsTemporary || (entry.State == EntityState.Added && property.Metadata.IsForeignKey()))

                    var tempIgnoreAttribute = property.Metadata.PropertyInfo.GetCustomAttribute(typeof(EntityChangeTrackerIgnoreAttribute)) as EntityChangeTrackerIgnoreAttribute;

                    //if ignore attribute is present no need to log prop name with default value like 'N/A'
                    if (tempIgnoreAttribute != null && !tempIgnoreAttribute.KeepProp)
                    {
                        continue;
                    }

                    string propertyName = property.Metadata.Name;

                    if (tempETA.DisplayProp != null && tempETA.DisplayProp == propertyName)
                    {
                        displayPropValue = property.CurrentValue?.ToString();
                    }

                    if (tempETA.UserNameProp != null && tempETA.UserNameProp == propertyName)
                    {
                        userNamePropValue = property.CurrentValue?.ToString();
                    }

                    if (tempParentKeyAtt != null && tempParentKeyAtt.ParentPrimaryKeyList.Contains(propertyName) && property.CurrentValue != null)
                    {
                        parentParentKeyValues[propertyName] = property.CurrentValue;
                    }

                    if (property.Metadata.IsPrimaryKey())
                    {
                        KeyValues[propertyName] = property.CurrentValue;
                    }

                    //if ignore attribute and need to log default value instead of actual
                    Object propCurrentValueToWrite = property.CurrentValue;
                    if (tempIgnoreAttribute != null)
                    {
                        propCurrentValueToWrite = tempIgnoreAttribute.KeepPropDefaultValue;
                    }

                    CurrentValues[propertyName] = propCurrentValueToWrite;


                    IForeignKey foreignKey = null;
                    object NewFKNameValue = null;
                    PropertyInfo foreignKeyProp = null;
                    if (property.Metadata.IsForeignKey() && tempIgnoreAttribute == null && property.CurrentValue != null)
                    {
                        foreignKey = property.Metadata.GetContainingForeignKeys().Single();

                        var dataNew = this.Find(foreignKey.PrincipalEntityType.ClrType, property.CurrentValue);
                        var tempAttribute = foreignKey.PrincipalEntityType.ClrType.GetCustomAttribute(typeof(EntityChangeTrackerTableAttribute)) as EntityChangeTrackerTableAttribute;


                        if (tempAttribute == null)
                        {
                            throw new Exception($"All table should be covered by EntityChangeTrackerTableAttribute, missing in {foreignKey.PrincipalEntityType.DisplayName()}");
                            //continue;
                        }

                        if (tempAttribute.DisplayProp == null)
                        {
                            //This scenario should not be happen that and entity disply prop is null and it has direct forienkey, this prop will be only null in mapping table
                            continue;
                        }


                        foreignKeyProp = foreignKey.PrincipalEntityType.ClrType
                           .GetProperty(tempAttribute.DisplayProp);
                        if (dataNew != null)
                        {
                            NewFKNameValue = foreignKeyProp.GetValue(dataNew);
                        }


                        Dictionary<string, object> navigationData = new Dictionary<string, object>();
                        navigationData[tempAttribute.DisplayProp] = NewFKNameValue;
                        CurrentValues[foreignKey.DependentToPrincipal.Name] = navigationData;
                    }

                    if (property.IsModified)
                    {
                        Object OldFKName = null;
                        Object OldValue = property.OriginalValue;
                        if (tempIgnoreAttribute != null)
                        {
                            OldValue = tempIgnoreAttribute.KeepPropDefaultValue;
                        }
                        if (property.Metadata.IsForeignKey() && tempIgnoreAttribute == null && property.OriginalValue != null)
                        {
                            //var data = a1.PrincipalEntityType.FindPrimaryKey();                            
                            var dataOld = this.Find(foreignKey.PrincipalEntityType.ClrType, property.OriginalValue);

                            if (dataOld != null)
                            {
                                OldFKName = foreignKeyProp.GetValue(dataOld);
                            }
                        }


                        deltaChanges[propertyName] = new { Old = OldValue, OldFKName = OldFKName, New = propCurrentValueToWrite, NewFKName = NewFKNameValue };
                    }
                }

                //If modified state but deltachanges not present
                //Not needed as it's handled by entity core i guess
                if (item.State == EntityState.Modified && deltaChanges.Count == 0)
                {
                    continue;
                }

                var convertSetting = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                };

                Guid? tempParentEntityId = null;
                if (!parentParentKeyValues.Count.IsZero())
                {
                    tempParentEntityId = Guid.Parse(parentParentKeyValues.First().Value.ToString());
                }

                var tempChange = new EntityChangeTrakerLog
                {
                    EntityChangeTrakerLogId = Guid.NewGuid(),
                    DisplayPropValue = displayPropValue,
                    TableName = item.Metadata.GetTableName(),
                    EntityId = Guid.Parse(KeyValues.Single().Value.ToString()),
                    EntryDate = currentTimeStamp,
                    CurrentData = CurrentValues.Count == 0 ? null : JsonConvert.SerializeObject(CurrentValues, convertSetting),
                    DeltaChanges = deltaChanges.Count == 0 ? null : JsonConvert.SerializeObject(deltaChanges, convertSetting),
                    EntryBy = userNamePropValue,
                    Action = item.State,
                    ParentEntityId = tempParentEntityId,
                    VirtualSessionId = VirtualSessionId
                };

                this.EntityChangeTrakerLogs.Add(tempChange);
            }

        }
    }


}
