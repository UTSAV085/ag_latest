﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    public static class DatabaseContextHelper
    {
        public static void EnsureSeedDataForContext(this DatabaseContext db)
        {

            const string SystemName = "System";
            DateTime currentTime = Extended.CurrentIndianTime;

            //Create all page permissions
            var allEnumPages = Enum.GetValues(typeof(PageName));

            foreach (Enum tempEnum in allEnumPages)
            {
                var pageEnum = (PageName)tempEnum;
                var dbPage = db.PagePermissions.FirstOrDefault(i => i.PageName == pageEnum);
                var allPermissions = tempEnum.GetType()
                .GetField(tempEnum.ToString())
                .GetCustomAttribute(typeof(PagePermissionAttribute)) as PagePermissionAttribute;

                //var allPermissions =   tempEnum.GetType().GetCustomAttribute(typeof(PagePermissionAttribute)) as PagePermissionAttribute;
                if (dbPage == null)
                {
                    dbPage = new PagePermission();
                    dbPage.PagePermissionId = Guid.NewGuid();
                    dbPage.ModifyBy = SystemName;
                    dbPage.PageName = pageEnum;
                    dbPage.PermissionTypes = allPermissions.PermissionTypes;

                    db.PagePermissions.Add(dbPage);
                }

                if (!dbPage.PermissionTypes.SequenceEqual(allPermissions.PermissionTypes))
                {
                    dbPage.PermissionTypes = allPermissions.PermissionTypes;
                    dbPage.ModifyBy = SystemName;
                }
            }

            db.SaveChanges();

            //check if default admin role exists

            var dbRoleDefault = db.UserRoles.FirstOrDefault(i => i.IsSytemCreated);

            if (dbRoleDefault == null)
            {
                dbRoleDefault = new UserRole();
                dbRoleDefault.UserRoleId = Guid.NewGuid();
                dbRoleDefault.Name = "Admin";

                dbRoleDefault.ModifyBy = SystemName;
                dbRoleDefault.IsSytemCreated = true;
                dbRoleDefault.Status = UserRoleStatus.Active;
                db.UserRoles.Add(dbRoleDefault);
            }
            db.SaveChanges();

            //Confirming admin has all page permissions

            var dbAllPages = db.PagePermissions.ToList();

            foreach (var dbPage in dbAllPages)
            {
                var dbURPP = db.UserRolePagePermissions.FirstOrDefault(i => i.UserRoleId == dbRoleDefault.UserRoleId && i.PagePermissionId == dbPage.PagePermissionId);

                if (dbURPP == null)
                {
                    dbURPP = new UserRolePagePermission();

                    dbURPP.ModifyBy = SystemName;
                    dbURPP.PermittedPermissionTypes = dbPage.PermissionTypes;
                    dbURPP.PagePermissionId = dbPage.PagePermissionId;
                    dbURPP.UserRoleId = dbRoleDefault.UserRoleId;
                    dbURPP.UserRolePagePermissionId = Guid.NewGuid();
                    db.UserRolePagePermissions.Add(dbURPP);
                }

                if (!dbURPP.PermittedPermissionTypes.SequenceEqual(dbPage.PermissionTypes))
                {
                    dbURPP.PermittedPermissionTypes = dbPage.PermissionTypes;
                    dbURPP.ModifyBy = SystemName;
                }

            }

            db.SaveChanges();

            if (!db.Users.Any())
            {
                User adminuser = new User();

                adminuser.UserId = Guid.NewGuid();

                adminuser.Name = "ks";
                adminuser.Email = "sales@koushikisoftware.com";
                adminuser.Phone = "7044091991";
                adminuser.LoginId = "ks";
                adminuser.Address = "Sodepur, Kolkata";
                adminuser.UserRoleId = dbRoleDefault.UserRoleId;
                adminuser.Status = UserStatus.Active;

                adminuser.ModifyBy = SystemName;



                var passWordSalt = PasswordHelper.GetPasswordSalt();
                var passwordHash = PasswordHelper.EncodePassword("123", passWordSalt);

                adminuser.PasswordSalt = passWordSalt;
                adminuser.PasswordHash = passwordHash;

                db.Users.Add(adminuser);
                db.SaveChanges();

            }


            CaseStatus disposeType = db.CaseStatuses.FirstOrDefault(i => i.Type == CaseStatusType.Dispose);
            if (disposeType==null)
            {
                //Default status for cases
                disposeType = new CaseStatus();
                disposeType.CaseStatusId = Guid.NewGuid();
                disposeType.Name = "Dispose";
                disposeType.Type = CaseStatusType.Dispose;
                disposeType.HexColorCode = "#FF0000";
                disposeType.ModifyBy = SystemName;
               
                db.CaseStatuses.Add(disposeType);
                db.SaveChanges();
            }
           

        }
    }
}
