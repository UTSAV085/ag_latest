﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class User
    {
        public User()
        {
            this.UserLogInLogs = new HashSet<UserLogInLog>();
            this.UserCases = new HashSet<UserCase>();
        }
        public Guid UserId { get; set; } 
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string LoginId { get; set; }

        [EntityChangeTrackerIgnore]
        public string PasswordSalt { get; set; }
        [EntityChangeTrackerIgnore]
        public string PasswordHash { get; set; }
        public Guid UserRoleId { get; set; }
        public UserStatus Status { get; set; }
        public bool IsDefault { get; set; }      
        public string ModifyBy { get; set; }
        public UserType UserType { get; set; }
        public Guid? AGDepartmentId { get; set; }
        public Guid? GovernmentDepartmentId { get; set; }
        /// <summary>
        /// If user type is CaseStudyUser then this mapping will be used to check case permission to see
        /// </summary>
        public ICollection<UserCase> UserCases { get; set; }
        public virtual ICollection<UserLogInLog> UserLogInLogs { get; set; }
        public UserRole UserRole { get; set; }
        public AGDepartment AGDepartment { get; set; }
        public GovernmentDepartment GovernmentDepartment { get; set; }

        public UserCredentialRequest UserCredentialRequest { get; set; }
    }
    public enum UserStatus : byte
    {
        Active = 1,
        Inactive = 0
    }
    public enum UserType
    {
        /// <summary>
        /// This will be handled only by UserRole
        /// </summary>
        [Description("Attorney general")]
        AG = 0,
        /// <summary>
        /// AGDepartmentId
        /// </summary>
        [Description("AG-Department")]
        AGDepartment = 1,
        /// <summary>
        /// This will be handled only by UserRole
        /// </summary>
        [Description("Assistant attorney general")]
        AAG = 2,
        /// <summary>
        ///  This will be handled only by UserRole
        /// </summary>
        [Description("Chief standing counsel")]
        CSC=3,
        /// <summary>
        /// GovernmentDepartmentId
        /// </summary>
        [Description("Government Department")]
        GovernmentDepartment = 4,
        /// <summary>
        /// UserCases mapping will be used to check permission
        /// </summary>
        [Description("External user can see one/many cases")]
        CaseStudyUser =5,
        /// <summary>
        /// Advocate
        /// </summary>
        [Description("Advocate")]
        Advocate = 6

    }
    //public enum UserRole
    //{
    //    Admin = 0
    //}
}
