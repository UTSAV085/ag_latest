﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    [EntityChangeTrackerParentKey(nameof(NoticeBasicIndexId), nameof(CaseHearingId))]
    public class Document
    {
        public Guid DocumentId { get; set; }
        public string Name { get; set; }
        public Guid? NoticeBasicIndexId { get; set; }
        public Guid? CaseHearingId { get; set; }
        public DocumentType Type { get; set; }
        public DocumentEntityType EntityType { get; set; }
        public string DocumentUrl { get; set; }
        public int Ordinal { get; set; } 
        public string ModifyBy { get; set; }
        public DocumentStatus Status { get; set; }
        public int Size { get; set; }
        public string Description { get; set; }
        public NoticeBasicIndex NoticeBasicIndex { get; set; }
        public CaseHearing CaseHearing { get; set; }

    }

    public enum DocumentEntityType
    {
        Notice = 0,
        CaseHearing = 1
    }

    public enum DocumentType
    {
        Image = 0,
        Pdf = 1,
        Doc = 2,
        Excel = 3,
        Any = 4
    }
    public enum DocumentStatus
    {
        Added = 0,
        Removed = 1
    }
}
