﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(true,null,nameof(ModifyBy))]
    public class CaseHearing
    {
        public CaseHearing()
        {
            Documents = new HashSet<Document>();
        }
        public Guid CaseHearingId { get; set; }
        public Guid CaseId { get; set; }
        public DateTime? HearingDate { get; set; }
        public DateTime? NextHearingDate { get; set; } 
        public string ModifyBy { get; set; } 

        public Case Case { get; set; }
        public ICollection<Document> Documents { get; set; }
    }
}
