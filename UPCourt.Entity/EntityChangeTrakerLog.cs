﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: false, DisplayProp: null, UserNameProp: null)]
    public class EntityChangeTrakerLog
    {
        public Guid EntityChangeTrakerLogId { get; set; }
        public string TableName { get; set; }
        public string DisplayPropValue { get; set; }
        public Guid EntityId { get; set; }
        public string DeltaChanges { get; set; }
        public string CurrentData { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public EntityState Action { get; set; }
        public Guid? ParentEntityId { get; set; }
        public Guid VirtualSessionId { get; set; }
    }
}
