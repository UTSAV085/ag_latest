﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name), UserNameProp: nameof(ModifyBy))]
    public class UserCredentialRequest
    {
        public Guid UserCredentialRequestId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        /// <summary>
        /// For multiple petition no, this filed should be comma separated
        /// </summary>
        public string PetitionNo { get; set; }
        public string CreateDate { get; set; }
        public Guid? RegionId { get; set; }
        public string ModifyBy { get; set; }
        public UserCredentialRequestStatus Status { get; set; }
        public Guid? UserId { get; set; }
        public User User { get; set; }
        public Region Region { get; set; }
    }

    public enum UserCredentialRequestStatus
    {
        Pending=0,
        Accepted=1,
        Rejected=2
    }
}
