﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPCourt.Entity.Core;

namespace UPCourt.Entity
{
    [EntityChangeTrackerTable(EnableChangeLog: true, DisplayProp: nameof(Name),UserNameProp:nameof(ModifyBy))]
    public class Designation
    {
        public Designation()
        {
            Advocates = new HashSet<Advocate>();
        }
        public Guid DesignationId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DesignationStatus Status { get; set; }
        public bool IsDefault { get; set; }        
        public string ModifyBy { get; set; }
        
        public ICollection<Advocate> Advocates { get; set; }
    }
    public enum DesignationStatus
    {
        Active = 1,
        Inactive = 0
    }
}
